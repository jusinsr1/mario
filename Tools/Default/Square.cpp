#include "stdafx.h"
#include "Square.h"


CSquare::CSquare(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CMapObject(pGraphic_Device)
{
}


CSquare::~CSquare()
{
}

void CSquare::Ready_Square(const _vec3& vecPosition, const _int& i2DType, const _int& iVariable)
{
	m_iVariable = iVariable;
	m_iType = i2DType;
	wstring Path;
	if (m_iType == 0)
		Path = L"../../Resources/Textures/Others/Monster/Pen/Pen_0.png";
	else if (m_iType == 1)
	{
		if (m_iVariable == 0)
			Path = L"../../Resources/Textures/Others/Obejct/On_Platform/On_Platform_0.png";
		else
		{
			m_iVariable = 1;
			Path = L"../../Resources/Textures/Others/Obejct/Switch_Platform/Switch_Platform_0.png";
		}
	}
	else if (m_iType == 2)
		Path = L"../../Resources/Textures/Others/Obejct/Cannon/Cannon_TypeA/Cannon_TypeA_0.png";
	else if (m_iType == 3)
	{
		if (m_iVariable == 0)
			Path = L"../../Resources/Textures/Others/Obejct/Posters/PosterSmall/PosterSmall_0.png";
		else if (m_iVariable == 1)
			Path = L"../../Resources/Textures/Others/Obejct/Posters/PosterBig/PosterBig_0.png";
		else
		{
			m_iVariable = 2;
			Path = L"../../Resources/Textures/Others/Obejct/Graffiti/Graffiti_0.png";
		}
	}
	else if (m_iType == 4)
		Path = L"../../Resources/Textures/Others/Obejct/Jumping/Jumping_0.png";
	else if (m_iType == 5)
		Path = L"../../Resources/Textures/Others/Monster/Alien/Alien_Idle/Alien_Idle_0.png";
	else if (m_iType == 6)
		Path = L"../../Resources/Textures/Others/Monster/Octopus/Oct_Idle/Oct_Idle_0.png";
	else if (m_iType == 7)
		Path = L"../../Resources/Textures/Others/Obejct/Item/Item_0.png";
	else if (m_iType == 8)
		Path = L"../../Resources/Textures/2DMap/ToolBox/CollisionBox.png";
	else if (m_iType == 9)
		Path = L"../../Resources/Textures/2DMap/ToolBox/CameraBox.png";
	D3DXIMAGE_INFO ddsd;
	D3DXCreateTextureFromFileEx(m_pGraphic_Device, Path.c_str(), 0, 0, 0, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, &ddsd, NULL, &m_pTexture);

	if (m_pTexture == nullptr)
		return;
	D3DXMatrixIdentity(&m_matWorld);

	m_iHeight = ddsd.Height;
	m_iWidth = ddsd.Width;
	m_Scale.x = (_float)m_iWidth * 0.02f;
	m_Scale.y = (_float)m_iHeight * 0.02f;
	CObj::Scaling(m_Scale);
	CObj::Set_Position(vecPosition);

	m_iNumVertices = 4;
	m_iStride = sizeof(VTXTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1;

	m_iNumPolygons = 2;
	m_pPosition = new _vec3[m_iNumVertices];
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	CMapObject::Ready_Object();

	VTXTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);
	pVertices[0].vPosition =m_pPosition[0] = _vec3(-0.5f, 0.5f, -0.009f);
	pVertices[0].vTexUV = _vec2(0.f, 0.f);

	pVertices[1].vPosition = m_pPosition[1] = _vec3(0.5f, 0.5f, -0.009f);
	pVertices[1].vTexUV = _vec2(1.f, 0.f);

	pVertices[2].vPosition = m_pPosition[2] =_vec3(0.5f, -0.5f, -0.009f);
	pVertices[2].vTexUV = _vec2(1.f, 1.f);

	pVertices[3].vPosition = m_pPosition[3] =_vec3(-0.5f, -0.5f, -0.009f);
	pVertices[3].vTexUV = _vec2(0.f, 1.f);

	m_pVB->Unlock();

	POLYGON16*	pIndices = nullptr;
	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 3;

	m_pIB->Unlock();
}

void CSquare::Update()
{
}

void CSquare::Render()
{
	m_pGraphic_Device->SetTexture(0, m_pTexture);
	m_pGraphic_Device->SetTransform(D3DTS_WORLD, &m_matWorld);
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CSquare::Release()
{
}

_vec4 CSquare::UpdateRay(const _vec3 * vMouseRay, const _vec3 * vCameraPos, _float * dist)
{
	_vec3	vRayPivot = *vCameraPos;
	_vec3	vRay = *vMouseRay;
	_matrix	matWorldInv = Get_Matrix_Inverse();

	D3DXVec3TransformCoord(&vRayPivot, &vRayPivot, &matWorldInv);
	D3DXVec3TransformNormal(&vRay, &vRay, &matWorldInv);

	_float		fU, fV, fDist, fDistMin = -1;
	if (D3DXIntersectTri(&m_pPosition[0], &m_pPosition[1], &m_pPosition[2], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[0], &m_pPosition[2], &m_pPosition[3], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;

	if (fDistMin != -1)
	{
		if (*dist == 0)
		{
			*dist = fDistMin;
			_vec3 Pos = *vCameraPos + (*vMouseRay * fDistMin);
			return _vec4(Pos.x, Pos.y, Pos.z, (_float)m_iPlaneNum);
		}
		else if (*dist != 0)
		{
			if (*dist > fDistMin)
			{
				*dist = fDistMin;
				_vec3 Pos = *vCameraPos + (*vMouseRay * fDistMin);
				return _vec4(Pos.x, Pos.y, Pos.z, (_float)m_iPlaneNum);
			}
		}
	}
	return _vec4(-1, -1, -1, -1);
}

void CSquare::matWorldChange()
{
	CObj::Set_StateInfo(CObj::STATE_POSITION, &m_Position);
	CObj::Scaling(m_Scale);
}

CSquare * CSquare::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3& vecPosition, const _int& i2DType, const _int& iVariable)
{
	CSquare* pInstance = new CSquare(pGraphic_Device);
	pInstance->Ready_Square(vecPosition, i2DType, iVariable);

	return pInstance;
}
