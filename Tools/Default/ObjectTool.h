#pragma once


// CObjectTool 대화 상자입니다.

class CObjectTool : public CPropertyPage
{
	DECLARE_DYNAMIC(CObjectTool)

public:
	CObjectTool();
	virtual ~CObjectTool();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OBJECTTOOL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
