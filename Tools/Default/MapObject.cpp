#include "stdafx.h"
#include "MapObject.h"


CMapObject::CMapObject(LPDIRECT3DDEVICE9 m_pGraphic_Device)
	:CObj(m_pGraphic_Device)
{
}

CMapObject::~CMapObject()
{
}

void CMapObject::Ready_Object()
{
	if(FAILED(m_pGraphic_Device->CreateVertexBuffer(m_iStride * m_iNumVertices, 0, m_dwFVF, D3DPOOL_MANAGED, &m_pVB, nullptr)))
		return;
	if(FAILED(m_pGraphic_Device->CreateIndexBuffer(m_iPolygonSize * m_iNumPolygons, 0, m_eFormat, D3DPOOL_MANAGED, &m_pIB, nullptr)))
		return;
}

void CMapObject::Update()
{	
}

void CMapObject::Render()
{
}

void CMapObject::Release()
{
	m_pVB->Release();
	m_pIB->Release();
	
	for (size_t i = 0; i < m_iNumVertices; i++)
	{
		SafeDelete(m_pPosition);
	}
}

_vec4 CMapObject::UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist)
{
	return _vec4();
}
