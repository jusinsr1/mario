#pragma once
#include "MapObject.h"
class CSquare :	public CMapObject
{
private:
	CSquare(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CSquare();

public:
	void Ready_Square(const _vec3& vecPosition, const _int& i2DType, const _int& iVariable);
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual _vec4 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);

	void matWorldChange();
public:
	static CSquare* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _vec3& vecPosition, const _int& i2DType, const _int& iVariable);

protected:
	LPDIRECT3DTEXTURE9			m_pTexture = nullptr;
private:
	_uint m_iWidth = 0;
	_uint m_iHeight = 0;

};

