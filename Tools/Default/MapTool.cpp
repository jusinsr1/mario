// MapTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "MapTool.h"
#include "afxdialogex.h"
#include "MainFrm.h"
#include "Cube.h"
#include "Square.h"


// CMapTool 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMapTool, CPropertyPage)

CMapTool::CMapTool()
	: CPropertyPage(IDD_MAPTOOL)
	, m_ToolState(_T(""))
	, m_strVariable(_T(""))
	, m_iVairable(0)
	, m_iPlaneNum(0)
	, m_iRotationNum(0)
{

}

CMapTool::~CMapTool()
{
}

void CMapTool::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO2, m_Tex3DCombo);
	DDX_Text(pDX, IDC_EDIT1, m_Position.x);
	DDX_Text(pDX, IDC_EDIT3, m_Position.y);
	DDX_Text(pDX, IDC_EDIT4, m_Position.z);
	DDX_Text(pDX, IDC_EDIT5, m_Scale.x);
	DDX_Text(pDX, IDC_EDIT6, m_Scale.y);
	DDX_Text(pDX, IDC_EDIT7, m_Scale.z);
	DDX_Control(pDX, IDC_RADIO1, m_Type3DRadio[0]);
	DDX_Control(pDX, IDC_RADIO2, m_Type3DRadio[3]);
	DDX_Control(pDX, IDC_RADIO3, m_Type3DRadio[2]);
	DDX_Control(pDX, IDC_RADIO6, m_Type3DRadio[1]);
	DDX_Control(pDX, IDC_RADIO4, m_2DRadio);
	DDX_Text(pDX, IDC_EDIT8, m_ToolState);
	DDX_Control(pDX, IDC_COMBO4, m_2DObjcetCombo);
	DDX_Text(pDX, IDC_EDIT16, m_strVariable);
	DDX_Text(pDX, IDC_EDIT15, m_iVairable);
	DDX_Text(pDX, IDC_EDIT13, m_iPlaneNum);
	DDX_Text(pDX, IDC_EDIT14, m_iRotationNum);
}


BEGIN_MESSAGE_MAP(CMapTool, CPropertyPage)
	ON_BN_CLICKED(IDC_RADIO1, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO2, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO3, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO6, &CMapTool::OnBnClicked3DTypeChange)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_BUTTON1, &CMapTool::OnBnClickedDraw)
	ON_BN_CLICKED(IDC_BUTTON2, &CMapTool::OnBnClickedChange)
	ON_BN_CLICKED(IDC_BUTTON4, &CMapTool::OnBnClickedSaveData)
	ON_BN_CLICKED(IDC_BUTTON3, &CMapTool::OnBnClickedLoad)
	ON_CBN_SELCHANGE(IDC_COMBO4, &CMapTool::OnCbnSelchange2DType)
END_MESSAGE_MAP()


// CMapTool 메시지 처리기입니다.



BOOL CMapTool::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
	m_pBaseForm = dynamic_cast<CBaseForm*>(pMainFrm->m_SecondSplt.GetPane(0, 0));
	

	if(FAILED(LoadTexture(L"../../Data/Texture/3DMapTexInfo.txt")))
		return false;
	m_Tex3DCombo.SetCurSel(0);
	
	m_pSaveLoad = GET_INSTANCE(CSaveLoad_Manager);
	m_Type3DRadio[0].SetCheck(true);
	m_2DRadio.SetCheck(true);

	m_ToolState.SetString(L"STATE_NORMAL");
	m_2DObjcetCombo.AddString(L"연필");
	m_2DObjcetCombo.AddString(L"발판");
	m_2DObjcetCombo.AddString(L"대포");
	m_2DObjcetCombo.AddString(L"벽그림");
	m_2DObjcetCombo.AddString(L"2D점프대");
	m_2DObjcetCombo.AddString(L"우주인");
	m_2DObjcetCombo.AddString(L"문어");
	m_2DObjcetCombo.AddString(L"코인");
	m_2DObjcetCombo.AddString(L"충돌박스");
	m_2DObjcetCombo.AddString(L"카메라박스");
	m_2DObjcetCombo.SetCurSel(0);
	UpdateData(FALSE);

	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.

}

void CMapTool::OnBnClicked3DTypeChange()
{
	if (m_Type3DRadio[0].GetCheck())
		m_3DType = Building;
	else if (m_Type3DRadio[1].GetCheck())
		m_3DType = Box;
	else if (m_Type3DRadio[2].GetCheck())
		m_3DType = Spring;
	else if (m_Type3DRadio[3].GetCheck())
		m_3DType = Normal;
}




void CMapTool::OnBnClickedDraw()
{
	if (m_iMapToolState == STATE_NORMAL)
	{
		if (m_2DRadio.GetCheck())
			m_iMapToolState = STATE_DRAW2D;
		else
			m_iMapToolState = STATE_DRAW3D;
	}
	StateCheck();
	UpdateData(FALSE);
}

void CMapTool::Update()
{
	if (this == nullptr)
		return;
	UpdateData(TRUE);
		
	KeyCheck();



	UpdateData(FALSE);
}

void CMapTool::KeyCheck()
{
	_bool bClick = GET_INSTANCE(CKeyMgr)->KeyDown(KEY_LBUTTON);
	_bool bOntehView = GET_INSTANCE(CKeyMgr)->GetOnView();
	DrawObject(bClick, bOntehView);

	if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_RBUTTON))
	{
		m_Position = { 0.f,0.f,0.f };
		m_Scale = { 1.f,1.f,1.f };
		m_pSelObj = nullptr;
		m_iMapToolState = STATE_NORMAL;
	}
	if (m_iMapToolState == STATE_NORMAL)
	{
		if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_F1))
			OnBnClickedDraw();

		if (bClick&& bOntehView)
		{
			m_pSelObj = GET_INSTANCE(CSaveLoad_Manager)->GetSelObj();
			_bool Is2D3D = GET_INSTANCE(CSaveLoad_Manager)->IsSel2D3D();;
			if (m_pSelObj != nullptr)
			{
				wstring ImgTag = m_pSelObj->Get_ImgTag();
				_uint Type = m_pSelObj->GetType();
				if (Is2D3D)
				{
					m_iMapToolState = STATE_3DPICKING;
					for (size_t i = 0; i < 4; i++)
						m_Type3DRadio[i].SetCheck(0);
					m_Type3DRadio[Type].SetCheck(1);
					int i = 0;
					for (auto& iter : m_map3D)
					{
						if (iter.first == ImgTag)
							break;
						i++;
					}
					m_Tex3DCombo.SetCurSel(i);
					UpdateData(false);
				}
				else
				{
					m_iMapToolState = STATE_2DPICKING;
					m_2DObjcetCombo.SetCurSel(m_pSelObj->GetType());
				}
				m_Position = *m_pSelObj->Get_StateInfo(CObj::STATE_POSITION);
				m_Scale = m_pSelObj->Get_Scale();
				StateCheck();
			}
		}
	}
	if (m_iMapToolState == STATE_3DPICKING || m_iMapToolState == STATE_2DPICKING)
	{
		if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_F2))
		{
			if (m_pSelObj != nullptr)
			{
				GET_INSTANCE(CSaveLoad_Manager)->DeleteObj(m_pSelObj);
				m_pSelObj = nullptr;
				m_iMapToolState = STATE_NORMAL;
			}
		}
	}
	StateCheck();
}

void CMapTool::DrawObject(_bool bClick,_bool bOntehView)
{
	if (bClick&&bOntehView)
	{
		if (m_iMapToolState == STATE_3DPICKING)
		{
			if (m_pSelObj == nullptr)
				m_iMapToolState = STATE_NORMAL;
			_vec4 MousePos = m_pBaseForm->GetMousePos();
			OnBnClickedChange();
			m_Position = { MousePos.x, MousePos.y, MousePos.z };
			if (((int)MousePos.w % 5) == 0)
				m_Position.z -= (m_Scale.z / 2);
			if (((int)MousePos.w % 5) == 1)
				m_Position.x -= (m_Scale.x / 2);
			if (((int)MousePos.w % 5) == 2)
				m_Position.z += (m_Scale.z / 2);
			if (((int)MousePos.w % 5) == 3)
				m_Position.x += (m_Scale.x / 2);
			if (((int)MousePos.w % 5) == 4)
				m_Position.y += (m_Scale.y / 2);
			m_pSelObj->Set_Position(m_Position);
		}
		if (m_iMapToolState == STATE_2DPICKING)
		{
			if (m_pSelObj == nullptr)
				m_iMapToolState = STATE_NORMAL;
			_vec4 MousePos = m_pBaseForm->GetMousePos();
			m_Position = { MousePos.x, MousePos.y, MousePos.z };
			OnBnClickedChange();
			m_pSelObj->Set_Position(m_Position);
		}

		if (m_iMapToolState == STATE_DRAW3D)
		{
			_vec4 MousePos = m_pBaseForm->GetMousePos();
			wstring ImgTag, Path;
			if (bOntehView && bClick)
			{
				ImgTag = GetComboCurSel();
				auto& iter = m_map3D.find(ImgTag);
				if (iter == m_map3D.end())
					return;
				Path = iter->second;
				m_DrawPosition = { MousePos.x, MousePos.y, MousePos.z };
				if (m_DrawPosition.y == 0)
					m_DrawPosition.y += (m_Scale.y / 2);
				GET_INSTANCE(CSaveLoad_Manager)->Add3DCube(m_3DType, m_pSelObj = CCube::Create(GET_INSTANCE(CDevice)->GetDevice(), m_DrawPosition, m_Scale, Path, ImgTag));
				for (size_t i = 0; i < 4; i++)
				{
					if (0 != m_Type3DRadio[i].GetCheck())
						m_pSelObj->SetType(i);
				}
				m_Position = *m_pSelObj->Get_StateInfo(CObj::STATE_POSITION);
				wstring  ImgTag = m_pSelObj->Get_ImgTag();
				int i = 0;
				for (auto& iter : m_map3D)
				{
					if (iter.second == ImgTag)
						break;
					i++;
				}
				m_Tex3DCombo.SetCurSel(i);
				m_iMapToolState = STATE_3DPICKING;
			}
		}
		if (m_iMapToolState == STATE_DRAW2D)
		{
			_vec4 MousePos = m_pBaseForm->GetMousePos();
			wstring ImgTag, Path;
			if (bOntehView &&bClick)
			{
				m_DrawPosition = { MousePos.x,MousePos.y,MousePos.z };
				_int i2DType = m_2DObjcetCombo.GetCurSel();
				GET_INSTANCE(CSaveLoad_Manager)->AddSquare(m_2DType, m_pSelObj = CSquare::Create(GET_INSTANCE(CDevice)->GetDevice(), m_DrawPosition, i2DType, m_iVairable));
				m_iPlaneNum = (_uint)MousePos.w;
				m_pSelObj->SetPlaneNum(m_iPlaneNum, m_iRotationNum);
				m_Position = m_DrawPosition;
				m_iMapToolState = STATE_2DPICKING;
			}
		}
		StateCheck();
	}
}


void CMapTool::StateCheck()
{
	if (m_iMapToolState == STATE_NORMAL)
		m_ToolState.SetString(L"STATE_NORMAL");
	else if (m_iMapToolState == STATE_DRAW2D)
		m_ToolState.SetString(L"STATE_DRAW2D");
	else if (m_iMapToolState == STATE_DRAW3D)
		m_ToolState.SetString(L"STATE_DRAW3D");
	else if (m_iMapToolState == STATE_2DPICKING)
		m_ToolState.SetString(L"STATE_2DPICKING");
	else if (m_iMapToolState == STATE_3DPICKING)
		m_ToolState.SetString(L"STATE_3DPICKING");
	UpdateData(FALSE);
}

CString CMapTool::GetComboCurSel()
{
	CString StateKey;
	_uint index;
	index = m_Tex3DCombo.GetCurSel();
	m_Tex3DCombo.GetLBText(index, StateKey);
	return StateKey;
}

HRESULT CMapTool::LoadTexture(const wstring & wstrPath)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
		return E_FAIL;

	TCHAR szBuf[MAX_STR] = L"";
	IMGPATHINFO tImgPath;

	while (true)
	{
		fin.getline(szBuf, MAX_STR, '|');
		tImgPath.wstrObjKey = szBuf;

		fin.getline(szBuf, MAX_STR, '|');
		tImgPath.wstrStateKey = szBuf;

		fin.getline(szBuf, MAX_STR, '|');
		tImgPath.iCount = _wtoi(szBuf);

		fin.getline(szBuf, MAX_STR);
		tImgPath.wstrPath = szBuf;

		if (fin.eof())
			break;

		m_map3D.insert({ tImgPath.wstrStateKey, tImgPath.wstrPath });
		m_Tex3DCombo.AddString(tImgPath.wstrStateKey.c_str());
		GET_INSTANCE(CSaveLoad_Manager)->AddTexData(tImgPath.wstrStateKey, tImgPath.wstrPath);
	}

	fin.close();

	return S_OK;
}

void CMapTool::OnBnClickedChange()
{
	if (m_pSelObj == nullptr)
		return;

	if (!GET_INSTANCE(CKeyMgr)->GetOnView()&&m_iMapToolState == STATE_3DPICKING)
	{
		m_pSelObj->Set_Scale(_vec3(m_Scale.x, m_Scale.y, m_Scale.z));
		m_pSelObj->Set_Position(m_Position);
		for (size_t i = 0; i < 4; i++)
		{
			if (0 != m_Type3DRadio[i].GetCheck())
				m_pSelObj->SetType(i);
		}
		wstring ImgTag = GetComboCurSel();
		auto& iter = m_map3D.find(ImgTag);
		if (iter == m_map3D.end())
			return;
		wstring Path = iter->second;
		dynamic_cast<CCube*>(m_pSelObj)->SetTexture(Path,ImgTag);
	}
	else if (m_iMapToolState == STATE_2DPICKING)
	{
		m_pSelObj->Set_Scale(_vec3(m_Scale.x, m_Scale.y, 1));
		m_pSelObj->Set_Position(m_Position);
	}
}


void CMapTool::OnBnClickedSaveData()
{
	UpdateData(TRUE);

	CFileDialog Dlg(FALSE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);

	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);

	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data\\Map");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;
	if (IDOK == Dlg.DoModal())
	{
		wstring  Path = Dlg.GetPathName().GetString();
		GET_INSTANCE(CSaveLoad_Manager)->SaveObject(Path, !(m_2DRadio.GetCheck()));
	}
}





void CMapTool::OnBnClickedLoad()
{
	CFileDialog Dlg(TRUE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);
	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);
	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data\\Map");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;	// 절대경로!
	if (IDOK == Dlg.DoModal())
	{
		wstring  Path = Dlg.GetPathName().GetString();
		GET_INSTANCE(CSaveLoad_Manager)->LoadObject(Path, !(m_2DRadio.GetCheck()));
	}

}


void CMapTool::OnCbnSelchange2DType()
{
	int Select;
	Select = m_2DObjcetCombo.GetCurSel();
	m_2DType = Normal2D;
	if (Select >= 0 && Select <= 3)
	{
		this->GetDlgItem(IDC_EDIT15)->EnableWindow(TRUE);
		if (Select == 0)
			m_strVariable.SetString(L"0 : UpDown, 1 : LeftRight");
		else if (Select == 1)
			m_strVariable.SetString(L"0 : Always On, 1 : SwitchOnOff");
		else if (Select == 2)
			m_strVariable.SetString(L"0 : Down, 1 : Left, 2 : Up, 3 : Right");
		else if (Select == 3)
			m_strVariable.SetString(L"0 : SmallPoster, 1 : BigPoster, 2 :Graffiti");
	}
	else
	{
		this->GetDlgItem(IDC_EDIT15)->EnableWindow(FALSE);
		m_strVariable.SetString(L"");
	}
	if (Select == 8)
		m_2DType = CollisionBox;
	if (Select == 9)
		m_2DType = CameraBox;
	UpdateData(FALSE);
}
