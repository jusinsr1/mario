#include "stdafx.h"
#include "Obj.h"

CObj::CObj(LPDIRECT3DDEVICE9 pGraphic_Device)
	: m_pGraphic_Device(pGraphic_Device)
{
}

CObj::~CObj()
{
}

void CObj::Ready_Object()
{

}

_vec4 CObj::UpdateRay(const _vec3 * vMouseRay, const _vec3 * vCameraPos, _float * dist)
{
	return _vec4();
}



const _vec3 * CObj::Get_StateInfo(STATE eState)
{
	return (_vec3*)&m_matWorld.m[eState][0];
}

void CObj::Set_StateInfo(STATE eState, const _vec3 * pInfo)
{
	memcpy(&m_matWorld.m[eState][0], pInfo, sizeof(_vec3));
}

void CObj::SetUp_RotationX(const _float & fRadian)
{
	_vec3 vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix	matRot;
	D3DXMatrixRotationX(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CObj::STATE_RIGHT, &vRight);
	Set_StateInfo(CObj::STATE_UP, &vUp);
	Set_StateInfo(CObj::STATE_LOOK, &vLook);
}

void CObj::SetUp_RotationY(const _float & fRadian)
{
	_vec3 vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix	matRot;
	D3DXMatrixRotationY(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CObj::STATE_RIGHT, &vRight);
	Set_StateInfo(CObj::STATE_UP, &vUp);
	Set_StateInfo(CObj::STATE_LOOK, &vLook);
}

void CObj::SetUp_RotationZ(const _float & fRadian)
{
	_vec3 vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix	matRot;
	D3DXMatrixRotationZ(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CObj::STATE_RIGHT, &vRight);
	Set_StateInfo(CObj::STATE_UP, &vUp);
	Set_StateInfo(CObj::STATE_LOOK, &vLook);
}

void CObj::Rotation_X(const _float & fTimeDelta)
{
	_vec3	vDir[3];
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationX(&matRot, fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(STATE_LOOK, &vDir[STATE_LOOK]);

}

void CObj::Rotation_Y(const _float & fTimeDelta)
{
	_vec3	vDir[3];
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationY(&matRot, fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(STATE_LOOK, &vDir[STATE_LOOK]);
}

void CObj::Rotation_Z(const _float & fTimeDelta)
{
	_vec3	vDir[3];
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationZ(&matRot, fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(STATE_LOOK, &vDir[STATE_LOOK]);
}

void CObj::Rotation_AxisRight(const _float& fTimeDelta)
{
	_vec3	vDir[3], vAxisRight;
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationAxis(&matRot, &vDir[0], fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(STATE_LOOK, &vDir[STATE_LOOK]);
}


void CObj::Go_Straight()
{
	_vec3 vLook, vPosition;

	vLook = *Get_StateInfo(STATE_LOOK);
	vPosition = *Get_StateInfo(STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vLook, &vLook)*0.1f;

	Set_StateInfo(STATE_POSITION, &vPosition);
}

void CObj::Go_Left()
{
	_vec3 vRight, vPosition;

	vRight = *Get_StateInfo(STATE_RIGHT);
	vPosition = *Get_StateInfo(STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vRight, &vRight)*0.1f;

	Set_StateInfo(STATE_POSITION, &vPosition);
}

void CObj::Go_Right()
{
	_vec3 vRight, vPosition;

	vRight = *Get_StateInfo(STATE_RIGHT);
	vPosition = *Get_StateInfo(STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vRight, &vRight)*0.1f;

	Set_StateInfo(STATE_POSITION, &vPosition);
}

void CObj::Go_Up()
{
	_vec3 vUp, vPosition;

	vUp = *Get_StateInfo(STATE_UP);
	vPosition = *Get_StateInfo(STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vUp, &vUp)*0.1f;

	Set_StateInfo(STATE_POSITION, &vPosition);
}

void CObj::Go_Down()
{
	_vec3 vUp, vPosition;

	vUp = *Get_StateInfo(STATE_UP);
	vPosition = *Get_StateInfo(STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vUp, &vUp)*0.1f;

	Set_StateInfo(STATE_POSITION, &vPosition);
}

void CObj::BackWard()
{
	_vec3 vLook, vPosition;

	vLook = *Get_StateInfo(STATE_LOOK);
	vPosition = *Get_StateInfo(STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vLook, &vLook)*0.1f;

	Set_StateInfo(STATE_POSITION, &vPosition);
}

void CObj::Scaling(const _vec3 & Scale)
{
	_vec3 vDir[3];

	for (size_t i = 0; i < 3; i++)
	{
		vDir[i] = *Get_StateInfo(STATE(i));
		D3DXVec3Normalize(&vDir[i], &vDir[i]);
	}

	vDir[STATE_RIGHT] *= Scale.x;
	vDir[STATE_UP] *= Scale.y;
	vDir[STATE_LOOK] *= Scale.z;

	Set_StateInfo(STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(STATE_LOOK, &vDir[STATE_LOOK]);
}

_matrix CObj::Get_Matrix_Inverse() const
{
	_matrix			matInverse;
	D3DXMatrixInverse(&matInverse, nullptr, &m_matWorld);

	return _matrix(matInverse);
}

_vec3 CObj::Get_Scale()
{
	_float	fScaleX = D3DXVec3Length(Get_StateInfo(CObj::STATE_RIGHT));
	_float	fScaleY = D3DXVec3Length(Get_StateInfo(CObj::STATE_UP));
	_float	fScaleZ = D3DXVec3Length(Get_StateInfo(CObj::STATE_LOOK));

	return _vec3(fScaleX, fScaleY, fScaleZ);
}

wstring CObj::Save2DObj()
{
	wstring SaveData;

	SaveData = to_wstring(m_iType);
	SaveData = SaveData + L"|" + to_wstring(m_iRotationNum);
	SaveData = SaveData + L"|" + to_wstring(m_iVariable);
	SaveData = SaveData + L"|" + to_wstring(m_iPlaneNum);
	SaveData = SaveData + L"|" + to_wstring(m_Position.x);
	SaveData = SaveData + L"|" + to_wstring(m_Position.y);
	SaveData = SaveData + L"|" + to_wstring(m_Position.z);
	if (m_iType == 9)
	{
		SaveData = SaveData + L"|" + to_wstring(m_Scale.x);
		SaveData = SaveData + L"|" + to_wstring(m_Scale.y);
	}

	return SaveData;
}

wstring CObj::Save3DObj()
{
	wstring SaveData;
	
	SaveData = m_wstrImageTag + L"|" + to_wstring(m_Position.x);
	SaveData = SaveData + L"|" + to_wstring(m_Position.y);
	SaveData = SaveData + L"|" + to_wstring(m_Position.z);
	SaveData = SaveData + L"|" + to_wstring(m_Scale.x);
	SaveData = SaveData + L"|" + to_wstring(m_Scale.y);
	SaveData = SaveData + L"|" + to_wstring(m_Scale.z);
	SaveData = SaveData + L"|" + to_wstring(m_iType);
	return SaveData;
}

PLANEINFO CObj::Get_PlaneInfo(const _uint Planenum, const _uint BuildingNum)
{
	PLANEINFO PlaneData;

	PlaneData.iPlaneNum = BuildingNum * 5 + Planenum;

	if (Planenum == 0)
	{
		PlaneData.arrMoveablePlane[0] = -1;
		PlaneData.arrMoveablePlane[1] = BuildingNum * 5 + 1;
		PlaneData.arrMoveablePlane[2] = BuildingNum * 5 + 4;
		PlaneData.arrMoveablePlane[3] = BuildingNum * 5 + 3;


		PlaneData.fWidth = m_Scale.x;
		PlaneData.fHeight = m_Scale.y;
		PlaneData.Pos = { m_Position.x, m_Position.y, m_Position.z - (m_Scale.z*0.5f) };
	}
	else if (Planenum == 1)
	{
		PlaneData.arrMoveablePlane[0] = -1;
		PlaneData.arrMoveablePlane[1] = BuildingNum * 5 + 2;
		PlaneData.arrMoveablePlane[2] = BuildingNum * 5 + 4;
		PlaneData.arrMoveablePlane[3] = BuildingNum * 5;

		PlaneData.fWidth = m_Scale.z;
		PlaneData.fHeight = m_Scale.y;
		PlaneData.Pos = { m_Position.x - (m_Scale.x*0.5f), m_Position.y, m_Position.z};
	}
	else if (Planenum == 2)
	{
		PlaneData.arrMoveablePlane[0] = -1;
		PlaneData.arrMoveablePlane[1] = BuildingNum * 5 + 3;
		PlaneData.arrMoveablePlane[2] = BuildingNum * 5 + 4;
		PlaneData.arrMoveablePlane[3] = BuildingNum * 5 + 1;

		PlaneData.fWidth = m_Scale.x;
		PlaneData.fHeight = m_Scale.y;
		PlaneData.Pos = { m_Position.x, m_Position.y, m_Position.z + (m_Scale.z*0.5f) };
	}
	else if (Planenum == 3)
	{
		PlaneData.arrMoveablePlane[0] = -1;
		PlaneData.arrMoveablePlane[1] = BuildingNum * 5 ;
		PlaneData.arrMoveablePlane[2] = BuildingNum * 5 +4;
		PlaneData.arrMoveablePlane[3] = BuildingNum * 5 +2;

		PlaneData.fWidth = m_Scale.z;
		PlaneData.fHeight = m_Scale.y;
		PlaneData.Pos = { m_Position.x + (m_Scale.x*0.5f), m_Position.y, m_Position.z };
	}
	else if (Planenum == 4)
	{
		PlaneData.arrMoveablePlane[0] = BuildingNum * 5;
		PlaneData.arrMoveablePlane[1] = BuildingNum * 5 +1;
		PlaneData.arrMoveablePlane[2] = BuildingNum * 5 +2;
		PlaneData.arrMoveablePlane[3] = BuildingNum * 5 +3;


		PlaneData.fWidth = m_Scale.x;
		PlaneData.fHeight = m_Scale.z;
		PlaneData.Pos = { m_Position.x, m_Position.y + (m_Scale.y *0.5f), m_Position.z };
	}
	return PlaneData;
}

void CObj::SetPlaneNum(const _int & iPlaneNum, const _int & iRotationNum)
{
	m_iPlaneNum = iPlaneNum;
	_int Num = -1;
	if (iPlaneNum == -1)
		Num = 4;
	else
		Num = iPlaneNum % 5;


	if (Num == 1)
		Rotation_Y(D3DXToRadian(90));
	else if (Num == 2)
		Rotation_Y(D3DXToRadian(180));
	else if (Num == 3)
		Rotation_Y(D3DXToRadian(270));
	else if (Num == 4)
	{
		m_iRotationNum = iRotationNum;
		SetUp_RotationX(D3DXToRadian(90));
		if (m_iRotationNum == 1 && m_iType!=2)
			Rotation_Y(D3DXToRadian(90));
		else if (m_iRotationNum == 2)
			Rotation_Y(D3DXToRadian(180));
		else if (m_iRotationNum == 3)
			Rotation_Y(D3DXToRadian(270));
	}

	if (m_iType == 2)
	{
		m_iRotationNum = iRotationNum;
		if (Num == 0)
		{
			if (m_iRotationNum == 0)
				Rotation_Z(D3DXToRadian(-90));
			else if (m_iRotationNum == 1)
				Rotation_Z(D3DXToRadian(-180));
			else if (m_iRotationNum == 2)
				Rotation_Z(D3DXToRadian(-270));
		}
		else if (Num == 1)
		{
			if (m_iRotationNum == 0)
				Rotation_X(D3DXToRadian(-90));
			else if (m_iRotationNum == 1)
				Rotation_X(D3DXToRadian(-180));
			else if (m_iRotationNum == 2)
				Rotation_X(D3DXToRadian(-270));
		}
		else if (Num == 2)
		{
			if (m_iRotationNum == 0)
				Rotation_Z(D3DXToRadian(90));
			else if (m_iRotationNum == 1)
				Rotation_Z(D3DXToRadian(180));
			else if (m_iRotationNum == 2)
				Rotation_Z(D3DXToRadian(270));
		}
		else if (Num == 3)
		{
			if (m_iRotationNum == 0)
				Rotation_X(D3DXToRadian(90));
			else if (m_iRotationNum == 1)
				Rotation_X(D3DXToRadian(180));
			else if (m_iRotationNum == 2)
				Rotation_X(D3DXToRadian(270));
		}
	}
}
