#pragma once
#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CTransform final: public CComponent
{
public:
	enum STATE		{ STATE_RIGHT, STATE_UP, STATE_LOOK, STATE_POSITION, STATE_END};
private:
	explicit CTransform(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTransform(const CTransform& rhs);
	virtual ~CTransform() = default;
public:
	const _vec3* Get_StateInfo(STATE eState);
	_vec3 Get_Scale();
	_matrix Get_Matrix() const {return m_matWorld;}
public:
	void Set_StateInfo(STATE eState, const _vec3* pInfo);


public:
	HRESULT Ready_Transform();
	HRESULT SetUp_OnGraphicDev();

	void SetUp_Speed(const _float& fMovePerSec, const _float& fRotationPerSec);
	void Go_Straight(const _float& fTimeDelta);
	void Go_Left(const _float& fTimeDelta);
	void Go_Right(const _float& fTimeDelta);
	void Go_Up(const _float& fTimeDelta);
	void Go_Down(const _float& fTimeDelta);
	void BackWard(const _float& fTimeDelta);
	void SetUp_RotationX(const _float& fRadian);
	void SetUp_RotationY(const _float& fRadian);
	void SetUp_RotationZ(const _float& fRadian);
	void SetUp_RotationRight(const _float& fRadian);
	void Rotation_X(const _float& fTimeDelta);
	void Rotation_Y(const _float& fTimeDelta);
	void Rotation_Z(const _float& fTimeDelta);
	void Rotation_AxisRight(const _float& fTimeDelta);
	void Rotation_AxisRight(const _float& fRadian, const _float& fSeta_Degree);
	void Rotation_AxisLook(const _float & fTimeDelta, const _float& fSeta_Degree);
	_matrix Get_Matrix_Inverse();
	void Scaling(const _float& fX, const _float& fY, const _float& fZ);
	void Go_ToTarget(const _vec3* pTargetPos, const _float& fTimeDelta);
	void SetPlaneNum(const _int & iPlaneNum, const _int & iRotationNum);
	void Move(const _float& fTimeDelta,const _vec2& vSpd);
	void MoveV3( const _vec3& vSpd);

	void SetXZ(const _vec3 & vSpd);

	void GetRC2D(RC& rc);
	void GetRC3D(RC& rc);
	_float GetCX();
	_float GetCY();
	_float GetCZ();
private:
	_matrix		m_matWorld;
	_float		m_fSpeed_Move =1;
	_float		m_fSpeed_Rotation =1;
public:
	static CTransform* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free() override;
};

_END