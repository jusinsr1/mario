#pragma once

typedef struct tagCamera_Desc
{
	D3DXVECTOR3		vEye;
	D3DXVECTOR3		vAt;
	D3DXVECTOR3		vAxisY;
}CAMERADESC;

typedef struct tagProjection_Camera_Desc
{
	float			fFovY;
	float			fAspect;
	float			fNear;
	float			fFar;
}PROJDESC;

typedef struct tagVertex_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR2			vTexUV;
}VTXTEX;

typedef struct tagVertex_TextureNormal
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vNormal;
	D3DXVECTOR2			vTexUV;
}VTXNMTEX;

typedef struct tagVertex_ViewPort
{
	D3DXVECTOR4			vPosition;
	D3DXVECTOR2			vTexUV;
}VTXVIEWPORT;

typedef struct tagVertexCube_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vTexUV;
}VTXCUBETEX;

typedef struct tagVertexCube_NormalTexture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vNormal;
	D3DXVECTOR3			vTexUV;
}VTXCUBENMTEX;

typedef struct tagVertexCube_NormalCOlTexture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vNormal;
	DWORD				dwColor;
	D3DXVECTOR3			vTexUV;
}VTXCUBENMCOLTEX;

typedef struct tagParticle
{
	D3DXVECTOR3 vPosition;
	DWORD		dwColor;
}PARTICLE;

typedef struct tagAttibute
{
	D3DXVECTOR3 vPosition;
	D3DXVECTOR3	vVelocity;
	D3DXVECTOR3 vAcceleration;
	float		fTimeLife;
	float		fAge;
	DWORD		dwColor;
	DWORD		dwColorFade;
	bool		bIsLive;
	float		fDist;

}ATTIBUTE;

typedef struct tagVertex_Color
{
	D3DXVECTOR3			vPosition;
	DWORD				dwColor;
}VTXCOL;

typedef struct tagVertex_NormalColor
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR3			vNormal;
	DWORD				dwColor;
}VTXNMCOL;
typedef struct tagPolygon16
{
	unsigned short		_0, _1, _2;
}POLYGON16;

typedef struct tagPolygon32
{
	unsigned long		_0, _1, _2;
}POLYGON32;

typedef struct tagLine16
{
	unsigned short		_0, _1;
}LINE16;

typedef struct rectRC
{
	float  	left, right, top, bot;
	int		plane;
	int		climeDir;
	D3DXVECTOR3	vPos;
}RC;


typedef struct cubeCB
{
	D3DXVECTOR3	vPos;
	float  	w, h, d;
	
}CB;



typedef struct tagObject_Info
{

	bool				IsColne;
	float				fFrame_Speed , fMove_Speed;
	unsigned int		iTexIdx;
}OBJ_INFO;