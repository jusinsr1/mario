#pragma once
#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_Terrain final:	public CVIBuffer
{
public:
	explicit CBuffer_Terrain(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Terrain(const CBuffer_Terrain& rhs);
	virtual ~CBuffer_Terrain() = default;
public:
	HRESULT Ready_VIBuffer(const _float& fInterval);
	void Render_VIBuffer();
private:
	_float		m_fInterval= 0.f;
public:
	static CBuffer_Terrain* Create(LPDIRECT3DDEVICE9 pGraphic_Device,  const _float& fInterval = 300.f);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END
