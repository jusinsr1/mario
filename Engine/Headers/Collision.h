#pragma once
#include "Component.h"

_BEGIN(Engine)
class CTransform;
class CGameObject;
class _ENGINE_DLL CCollision final : public CComponent
{
public:
	enum COLGROUP {COL_PLAYER,COL_NORMAL,COL_TIRE,COL_FLAT,COL_EFF,COL_BULLET,COL_MON,COL_BOX,COL_BUILDING,COL_SPRING,COL_EVENT,
		COL_CONNON,COLL_END};
private:
	explicit CCollision(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCollision() = default;

public:
	HRESULT Ready_Collision();
	HRESULT Add_CollGroup(COLGROUP eGroup, CGameObject* pGameObject);
	HRESULT Coll_CollGroup();
public:
	HRESULT	Erase_Object(CGameObject* pObject);
	HRESULT Release_Coll(CGameObject* pObject);

private:
	list<CGameObject*>	m_CollList[COLL_END];
	typedef list<CGameObject*>	OBJECTLIST;

private:
	void Coll_Player_Spring();
	void Coll_Player_Tire();
	void Coll_Player_Normal();
	void Coll_Player_Flat();
	void Coll_Player_Mon();
	void Coll_Player_Box();

	void Coll_Box_Normal();
	void Coll_Box_Building();


	void Coll_Mon_Flat();
	void Coll_Mon_Bullet();

	void Coll_Event();

private:
	//_bool Rect_Cube(CTransform*& left, CTransform*& right);

private:
	_bool Coll_Rect1(const RC& rc1, const RC& rc2);
	_bool Coll_Rect2(const RC& rc1, const RC& rc2,_vec3* vRap);
	_bool Coll_Rect3(const RC& rc1, const RC& rc2, _vec3* vRap);

	_bool Coll_Cube(const CB& cb1, const CB& cb2, _vec3* vRap);
public:
	static CCollision* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END