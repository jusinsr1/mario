#pragma once
#include "Component.h"

_BEGIN(Engine)

class _ENGINE_DLL CTexture final : public CComponent
{

public:
	enum TYPE { TYPE_GENERAL,TYPE_CUBE , TYPE_END };

public:
	const _uint GetTexSize() { return m_vecTexture.size(); }

private:
	explicit CTexture(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTexture(const CTexture& rhs);
	virtual ~CTexture() = default;
public:
	IDirect3DBaseTexture9*	Get_Tex(const _uint& iIndex) {
		if (m_vecTexture.size() <= iIndex)
			return nullptr;
		return m_vecTexture[iIndex];
	}
	_vec3* Get_TexRect(const _uint& iIndex);

public:
	HRESULT Ready_Texture(const _tchar* pFilePath, const _uint& iNumTexture, 
		const TYPE eTypeID);
	HRESULT SetUp_OnGraphicDev(const _uint& iIndex = 0);
private:
	vector<IDirect3DBaseTexture9*>			m_vecTexture;
	typedef vector<IDirect3DBaseTexture9*>	VECTEXTURE;
	vector<_vec3>			m_vecTextureRect;
	typedef vector<_vec3>	VECTEXTUREINFO;
public:
	static CTexture* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const TYPE eTypeID,
		const _tchar* pFilePath, const _uint& iNumTexture = 1);
		virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END