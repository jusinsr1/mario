#pragma once
#include "Base.h"

_BEGIN(Engine)

class CComponent;
class _ENGINE_DLL CGameObject abstract : public CBase
{
protected:
	explicit CGameObject(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGameObject(const CGameObject& rhs);
	virtual ~CGameObject() = default;
public:
	CComponent* Get_ComponentPointer(const _tchar* pComponentTag);
public:
	virtual HRESULT Ready_GameObject();
	virtual _int	Update_GameObject(const _float& fTImeDelta);
	virtual _int	LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void	Render_GameObject();

	virtual void	Coll_GameObject(_int colType);
	virtual _bool	GetRC(RC& rc);
	virtual void	Coll_Move(_vec3& vMove, _int i);
	virtual void	GetCUBE(CB& cb);
	virtual void	SetBox(_vec3& v);
public:
	virtual	void Get_Event_GameObject(const _uint iType, CComponent* pCom = nullptr);

protected:
	_int Get_RandomNumber(int iMin, int iMax);

protected:
	LPDIRECT3DDEVICE9		m_pGraphic_Device = nullptr;

protected:
	OBJ_INFO	m_tInfo;

	_float		m_fTime = 0.f; 

protected:
	_int		m_iNumPlane = 0;
	_uint		m_iNumTypePlane = 0;
	_bool		m_IsCollsion = false;
protected:
	map<const _tchar*, CComponent*>			m_mapComponent;
	typedef map<const _tchar*, CComponent*>	MAPCOMPONENT;
protected:
	HRESULT Add_Component(const _tchar* pComponentTag, CComponent* pComponent);
	virtual _bool		Time_Permit(const _float& fTimePermit, const _float& fTimeDelta);
	virtual	HRESULT		Move_Frame(const _float& fTimePermit, const _float& fTimeDelta, const _uint& iTexSize);
private:
	CComponent*  Find_Component(const _tchar* pComponentTag);
	

public:
	virtual CGameObject* Clone_GameObject() = 0;
protected:
	virtual void Free();
};

_END