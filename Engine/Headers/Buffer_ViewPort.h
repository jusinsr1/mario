#pragma once
#include "VIBuffer.h"
#include "Transform.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_ViewPort  :public CVIBuffer
{

	private:
		explicit CBuffer_ViewPort(LPDIRECT3DDEVICE9 pGraphic_Device);
		explicit CBuffer_ViewPort(const CBuffer_ViewPort& rhs);
		virtual ~CBuffer_ViewPort() = default;
	public:
		HRESULT Ready_VIBuffer();
		void Render_VIBuffer(const _matrix* pWorldMatrix);
			
	private:
		VTXVIEWPORT*		m_pOriVertices = nullptr;
	private:
		HRESULT Transform_Vertices(const _matrix* pWorldMatrix);
	public:
		static CBuffer_ViewPort* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
		virtual CComponent* Clone_Component();
	protected:
		virtual void Free();

};

_END
