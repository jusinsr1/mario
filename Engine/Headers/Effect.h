#pragma once

#include "GameObject.h"

_BEGIN(Engine)

class _ENGINE_DLL CEffect :
	public CGameObject
{

public:
	explicit CEffect(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CEffect(CEffect& rhs);
	virtual ~CEffect() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int	Update_GameObject(const _float& fTImeDelta);
	virtual _int	LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void	Render_GameObject();
protected:
	_uint			m_iEffectID = 0;
	_vec3			m_vTargetPos;
public:
	virtual CGameObject*	Clone_GameEffect(const _uint& iEffectID , const _int iCurNumPlane) = 0;
private:
	virtual void Free();
};

_END