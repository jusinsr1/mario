#include "..\Headers\Object_Manager.h"
#include "Layer.h"

_IMPLEMENT_SINGLETON(CObject_Manager)
CObject_Manager::CObject_Manager()
{
}

CComponent * CObject_Manager::Get_ComponentPointer(const _uint & iSceneID, const _tchar * pLayerTag, const _tchar * pComponentTag, const _uint & iIndex)
{
	if (nullptr == m_pMapLayers)
		return nullptr;

	if (m_iNumScene <= iSceneID)
		return nullptr;

	CLayer*		pLayer = Find_Layer(iSceneID, pLayerTag);
	if (nullptr == pLayer)
		return nullptr;

	return pLayer->Get_ComponentPointer(pComponentTag, iIndex);
}

HRESULT CObject_Manager::Reserve_Object_Manager(const _uint & iNumScene)
{
	if (m_pMapLayers != nullptr)
		return E_FAIL;

	m_pMapLayers = new MAPLAYERS[iNumScene];

	m_iNumScene = iNumScene;

	return NOERROR;
}

HRESULT CObject_Manager::Add_Prototype_GameObject(const _tchar * pGameObjectTag, CGameObject * pGameObject)
{
	if (pGameObject == nullptr)
		return E_FAIL;

 	if (Find_Prototype(pGameObjectTag))
		return E_FAIL;

	m_mapPrototype.insert({ pGameObjectTag,pGameObject });

	return NOERROR;
}

HRESULT CObject_Manager::Add_GameObjectToLayer(const _tchar * pProtoTag, const _uint & iSceneID, const _tchar * pLayerTag, CGameObject** ppCloneObject)
{
	if (m_pMapLayers == nullptr)
		return E_FAIL;
	if (iSceneID >= m_iNumScene)
		return E_FAIL;

	CGameObject* pPrototype = nullptr;

	pPrototype = Find_Prototype(pProtoTag);

	if (pPrototype == nullptr)
		return E_FAIL;

	CGameObject* pGameObject = pPrototype->Clone_GameObject();
	if (pGameObject == nullptr)
		return E_FAIL;

	if (nullptr != ppCloneObject)
		*ppCloneObject = pGameObject;

	CLayer* pLayer = Find_Layer(iSceneID, pLayerTag);

	if (pLayer == nullptr)
	{
		pLayer = CLayer::Create();
		if (pLayer == nullptr)
			return E_FAIL;

		if (FAILED(pLayer->Add_Object(pGameObject)))
			return E_FAIL;

		m_pMapLayers[iSceneID].insert({ pLayerTag,pLayer });
	}
	else
		if (FAILED(pLayer->Add_Object(pGameObject)))
			return E_FAIL;

	return NOERROR;
}

HRESULT CObject_Manager::Add_GameEffectToLayer(const _tchar * pProtoTag, const _uint & iSceneID, const _tchar * pLayerTag , const _uint& iEffectID, const _int iCurNumPlane, CGameObject** ppCloneObject)
{
	if (m_pMapLayers == nullptr)
		return E_FAIL;
	if (iSceneID >= m_iNumScene)
		return E_FAIL;

	CGameObject* pPrototype = nullptr;

	pPrototype = Find_Prototype(pProtoTag);

	if (pPrototype == nullptr)
		return E_FAIL;

	CGameObject* pGameObject = dynamic_cast<CEffect*>(pPrototype)->Clone_GameEffect(iEffectID,iCurNumPlane);
	if (pGameObject == nullptr)
		return E_FAIL;


	if (nullptr != ppCloneObject)
		*ppCloneObject = pGameObject;

	CLayer* pLayer = Find_Layer(iSceneID, pLayerTag);

	if (pLayer == nullptr)
	{
		pLayer = CLayer::Create();
		if (pLayer == nullptr)
			return E_FAIL;

		if (FAILED(pLayer->Add_Object(pGameObject)))
			return E_FAIL;

		m_pMapLayers[iSceneID].insert({ pLayerTag,pLayer });
	}
	else
		if (FAILED(pLayer->Add_Object(pGameObject)))
			return E_FAIL;

	return NOERROR;
}

_int CObject_Manager::Update_GameObject_Manager(const _float & fTimeDelta)
{
	if (m_pMapLayers == nullptr)
		return -1;

	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapLayers[i])
		{
			if (Pair.second != nullptr)
			{
				if (Pair.second->Update_Object(fTimeDelta) & 0x80000000)
					return -1;
			}

		}

	}

	return _int();
}

_int CObject_Manager::LastUpdate_GameObject_Manager(const _float & fTimeDelta)
{
	if (m_pMapLayers == nullptr)
		return -1;

	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapLayers[i])
		{
			if (Pair.second != nullptr)
			{
				if (Pair.second->LastUpdate_Object(fTimeDelta) & 0x80000000)
					return -1;
			}
			
		}

	}
	return _int();
}

HRESULT CObject_Manager::Clear_Layers(const _uint & iSceneID)
{
	if (m_pMapLayers == nullptr)
		return E_FAIL;
	if (m_iNumScene <= iSceneID)
		return E_FAIL;
	
	for (auto& Pair : m_pMapLayers[iSceneID])
		Safe_Release(Pair.second);

	m_pMapLayers[iSceneID].clear();
	return NOERROR;
}

CGameObject * CObject_Manager::Find_Prototype(const _tchar * pGameObjectTag)
{
	auto& iter = find_if(m_mapPrototype.begin(), m_mapPrototype.end(), CFinder_Tag(pGameObjectTag));

	if (iter == m_mapPrototype.end())
		return nullptr;
	return iter->second;
}

CLayer * CObject_Manager::Find_Layer(const _uint & iSceneID, const _tchar * pLayerTag)
{
	auto& iter = find_if(m_pMapLayers[iSceneID].begin(), m_pMapLayers[iSceneID].end(), CFinder_Tag(pLayerTag));

	if (iter == m_pMapLayers[iSceneID].end())
		return nullptr;
	return iter->second;

}

void CObject_Manager::Free()
{
	for (size_t i = 0; i < m_iNumScene; i++)
	{
		for (auto& Pair : m_pMapLayers[i])
			Safe_Release(Pair.second);

		m_pMapLayers[i].clear();
	}
	Safe_Delete_Array(m_pMapLayers);

	for (auto& Pair : m_mapPrototype)
		Safe_Release(Pair.second);

	m_mapPrototype.clear();
}
