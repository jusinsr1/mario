#include "..\Headers\Buffer_Terrain.h"

CBuffer_Terrain::CBuffer_Terrain(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CVIBuffer(pGraphic_Device)
{
}

CBuffer_Terrain::CBuffer_Terrain(const CBuffer_Terrain & rhs)
	:CVIBuffer(rhs)
{
}

HRESULT CBuffer_Terrain::Ready_VIBuffer(const _float & fInterval)
{
	m_fInterval = fInterval;

	m_iNumVertices = 4;
	m_iStride = sizeof(VTXTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1;
	m_iNumPolygons = 2;

	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec3(0.f,0.f, fInterval);
	pVertices[0].vTexUV = _vec2(0.f,0.f);

	pVertices[1].vPosition = _vec3(fInterval, 0.f, fInterval);
	pVertices[1].vTexUV = _vec2(1.f,0.f);

	pVertices[2].vPosition = _vec3(fInterval, 0.f, 0.f);
	pVertices[2].vTexUV = _vec2(1.f,1.f);

	pVertices[3].vPosition = _vec3(0.f, 0.f, 0.f);
	pVertices[3].vTexUV = _vec2(0.f,1.f);





	m_pVB->Unlock();


	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	_uint			iPolygonIndex = 0;

	// ��.��
	pIndices[iPolygonIndex]._0 = 0;
	pIndices[iPolygonIndex]._1 = 1;
	pIndices[iPolygonIndex]._2 = 2;
			
	// ��.��
	pIndices[iPolygonIndex]._0 = 0;
	pIndices[iPolygonIndex]._1 = 2;
	pIndices[iPolygonIndex]._2 = 3;
			
	m_pIB->Unlock();


	return NOERROR;
}

void CBuffer_Terrain::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

CBuffer_Terrain * CBuffer_Terrain::Create(LPDIRECT3DDEVICE9 pGraphic_Device,const _float & fInterval)
{
	CBuffer_Terrain* pInstance = new CBuffer_Terrain(pGraphic_Device);
	if (FAILED(pInstance->Ready_VIBuffer(fInterval)))
	{
		_MSG_BOX("CBuffer_Terrain Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CBuffer_Terrain::Clone_Component()
{
	return new CBuffer_Terrain(*this);
}

void CBuffer_Terrain::Free()
{
	CVIBuffer::Free();
}