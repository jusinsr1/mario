#include "..\Headers\GameObject.h"
#include"Component.h"

CGameObject::CGameObject(LPDIRECT3DDEVICE9 pGraphic_Device)
	:m_pGraphic_Device(pGraphic_Device)
{
	m_pGraphic_Device->AddRef();
	
	ZeroMemory(&m_tInfo, sizeof(OBJ_INFO));

	m_tInfo.IsColne = false;
}

CGameObject::CGameObject(const CGameObject & rhs)
	:m_pGraphic_Device(rhs.m_pGraphic_Device),
	m_tInfo(rhs.m_tInfo)
{
	m_pGraphic_Device->AddRef();

	ZeroMemory(&m_tInfo, sizeof(OBJ_INFO));

	m_tInfo.IsColne = true;
}

CComponent * CGameObject::Get_ComponentPointer(const _tchar * pComponentTag)
{
	CComponent*	pComponent = Find_Component(pComponentTag);
	if (nullptr == pComponent)
		return nullptr;

	return pComponent;
}

HRESULT CGameObject::Ready_GameObject()
{
	return NOERROR;
}

_int CGameObject::Update_GameObject(const _float & fTImeDelta)
{
	return _int();
}

_int CGameObject::LastUpdate_GameObject(const _float & fTImeDelta)
{
	return _int();
}

void CGameObject::Render_GameObject()
{
}

void CGameObject::Coll_GameObject(_int colType)
{
}

_bool CGameObject::GetRC(RC& rc)
{
	return _bool();
}

void CGameObject::Coll_Move(_vec3& vMove, _int i)
{
}

void CGameObject::GetCUBE(CB & cb)
{
}

void CGameObject::SetBox(_vec3 & v)
{

}

void CGameObject::Get_Event_GameObject(const _uint iType, CComponent * pCom)
{

}

_int CGameObject::Get_RandomNumber(int iMin, int iMax)
{
	random_device random;
	mt19937 engine(random());
	uniform_int_distribution<int> distribution(iMin, iMax);

	return distribution(engine);	
}

HRESULT CGameObject::Add_Component(const _tchar * pComponentTag, CComponent * pComponent)
{
	if (pComponent == nullptr)
		return E_FAIL;
	if (Find_Component(pComponentTag) != nullptr)
		return E_FAIL;
	m_mapComponent.insert({ pComponentTag,pComponent });

	pComponent->AddRef();

	return NOERROR;
}

_bool CGameObject::Time_Permit(const _float & fTimePermit, const _float& fTimeDelta)
{

	_bool	bIsPermit = false;

	m_fTime += fTimeDelta;

	if (m_fTime >= fTimePermit)
	{
		m_fTime = 0.00f;
		bIsPermit = true;

		return bIsPermit;
	}

	return bIsPermit;
}

HRESULT CGameObject::Move_Frame(const _float & fTimePermit, const _float & fTimeDelta, const _uint& iTexSize)
{
	if (iTexSize & 0x80000000)
		return E_FAIL;

	m_fTime += fTimeDelta;

	if (m_fTime >= fTimePermit)
			m_fTime = 0.00f;

	else if (iTexSize <= m_tInfo.iTexIdx)
			m_tInfo.iTexIdx = 0;

	m_tInfo.iTexIdx++;

	return NOERROR;
}

CComponent * CGameObject::Find_Component(const _tchar * pComponentTag)
{
	auto& iter = find_if(m_mapComponent.begin(), m_mapComponent.end(), CFinder_Tag(pComponentTag));

	if (iter == m_mapComponent.end())
		return nullptr;
	return iter->second;
}

void CGameObject::Free()
{
	for (auto& Pair : m_mapComponent)
		Safe_Release(Pair.second);
	m_mapComponent.clear();

	Safe_Release(m_pGraphic_Device);
}
