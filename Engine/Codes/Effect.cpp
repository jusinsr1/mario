#include "Effect.h"

CEffect::CEffect(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CEffect::CEffect(CEffect & rhs)
	:CGameObject(rhs)
{
}

HRESULT CEffect::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CEffect::Ready_GameObject()
{
	return NOERROR;
}

_int CEffect::Update_GameObject(const _float & fTImeDelta)
{
	return _int();
}

_int CEffect::LastUpdate_GameObject(const _float & fTImeDelta)
{
	return _int();
}

void CEffect::Render_GameObject()
{
}


void CEffect::Free()
{
}
