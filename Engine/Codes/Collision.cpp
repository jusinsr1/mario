#include "..\Headers\Collision.h"
#include "GameObject.h"
#include "Management.h"
#include "Transform.h"



CCollision::CCollision(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
{
}

HRESULT CCollision::Add_CollGroup(COLGROUP eGroup, CGameObject * pGameObject)
{
	if (COLL_END <= eGroup)
		return E_FAIL;

	if (pGameObject == nullptr)
		return E_FAIL;

	m_CollList[eGroup].push_back(pGameObject);

	return NOERROR;
}

HRESULT CCollision::Coll_CollGroup()
{
 	Coll_Player_Tire();
	Coll_Player_Normal();
	Coll_Player_Flat();

	Coll_Box_Normal();
	Coll_Player_Box();

	Coll_Box_Building();
	
	Coll_Event();
	
	Coll_Player_Spring();

	return NOERROR;
}

HRESULT CCollision::Erase_Object(CGameObject * pObject)
{
	for (size_t i = 0; i < COLL_END; i++)
	{
		if (m_CollList[i].size() <= 0)
			continue;

		for (auto& iter = m_CollList[i].begin(); iter != m_CollList[i].end();)
		{
			if (*iter != nullptr)
			{
				if (*iter == pObject)
				{
					iter = m_CollList[i].erase(iter);
				}
				else
					iter++;
			}
		}
	}


	return NOERROR;
}

HRESULT CCollision::Release_Coll(CGameObject * pObject)
{
	for (size_t i = 0; i < COLL_END; i++)
	{
		if (m_CollList[i].size() <= 0)
			continue;

		for (auto& iter = m_CollList[i].begin(); iter != m_CollList[i].end();)
		{
			if (*iter != nullptr)
			{
				if (*iter == pObject)
				{
					Safe_Release(pObject);
				}
				else
				{
					iter++;
				}
			}
		}
	}


	return NOERROR;
}

void CCollision::Coll_Player_Spring()
{

	if (m_CollList[COL_PLAYER].size() <= 0)
		return;
	if (m_CollList[COL_SPRING].size() <= 0)
		return;

	CGameObject* player = m_CollList[COL_PLAYER].front();

	RC rcPlayer = {};
	player->GetRC(rcPlayer);


	for (CGameObject*& spring : m_CollList[COL_SPRING])
	{
		RC rcSpring = { 0.f,0.f,0.f,0.f,rcPlayer.plane ,rcPlayer.climeDir,rcPlayer.vPos };//플레이어의면,위치

		if (spring->GetRC(rcSpring))//트루일떄만 충돌처리
		{

			if (Coll_Rect1(rcPlayer, rcSpring))
			{
				player->Coll_GameObject(_int(COL_SPRING));
				spring->Coll_GameObject(_int(COL_PLAYER));
			}

		}


	}

}

void CCollision::Coll_Player_Tire()
{
	if (m_CollList[COL_PLAYER].size() <= 0)
 		return;
	if (m_CollList[COL_TIRE].size() <= 0)
		return;
	
	CGameObject* player = m_CollList[COL_PLAYER].front();

	RC rcPlayer = {};
	player->GetRC(rcPlayer);


	for (CGameObject*& tire : m_CollList[COL_TIRE])
	{
		RC rcTire = {0.f,0.f,0.f,0.f,rcPlayer.plane ,rcPlayer.climeDir,rcPlayer.vPos};//플레이어의면,위치

		if (tire->GetRC(rcTire))//트루일떄만 충돌처리
		{
			
			if (Coll_Rect1(rcPlayer, rcTire))
			{
				player->Coll_GameObject(_int(COL_TIRE));
				tire->Coll_GameObject(_int(COL_PLAYER));
			}

		}

 		
	}                                                                                                                                                                                                                                                                                                                                                                                                  
}

void CCollision::Coll_Player_Normal()
{
	if (m_CollList[COL_PLAYER].size() <= 0)
		return;
	if (m_CollList[COL_NORMAL].size() <= 0)
		return;

	CGameObject* player = m_CollList[COL_PLAYER].front();

	RC rcPlayer = {};
	player->GetRC(rcPlayer);

	_vec3 vRap = {};

	for (CGameObject*& normal : m_CollList[COL_NORMAL])
	{
		RC rcNormal = { 0.f,0.f,0.f,0.f,rcPlayer.plane ,rcPlayer.climeDir,rcPlayer.vPos };//플레이어의면,위치
		

		if (normal->GetRC(rcNormal))//트루일떄만 충돌처리
		{
			

			if (Coll_Rect2(rcPlayer, rcNormal, &vRap))
			{
				player->Coll_GameObject(_int(COL_NORMAL));
				player->Coll_Move(vRap, _int(COL_NORMAL));
			}


		}


	}
}

void CCollision::Coll_Player_Flat()
{

	if (m_CollList[COL_PLAYER].size() <= 0)
		return;
	if (m_CollList[COL_FLAT].size() <= 0)
		return;

	CGameObject* player = m_CollList[COL_PLAYER].front();

	RC rcPlayer = {};
	player->GetRC(rcPlayer);

	_vec3 vRap = {};

	for (CGameObject*& flat : m_CollList[COL_FLAT])
	{
		RC rcFlat = { 0.f,0.f,0.f,0.f,rcPlayer.plane ,rcPlayer.climeDir,rcPlayer.vPos };//플레이어의면,위치

		if (flat->GetRC(rcFlat))//트루일떄만 충돌처리
		{
			if (Coll_Rect3(rcPlayer, rcFlat, &vRap))
			{
				player->Coll_GameObject(_int(COL_FLAT));

				if (rcPlayer.plane % 5 == 4)//옥상
				{
					switch (rcPlayer.climeDir)
					{
					case 0:
						if (vRap.z>0.f)
							player->Coll_Move(vRap, _int(COL_FLAT));
						break;
					case 1:
						if (vRap.x>0.f)
							player->Coll_Move(vRap, _int(COL_FLAT));
						break;
					case 2://여기부터
						if (vRap.z<0.f)
							player->Coll_Move(vRap, _int(COL_FLAT));
						break;
					case 3:
						if (vRap.y>0.f)
							player->Coll_Move(vRap, _int(COL_FLAT));
						break; 
					}
				}
				else//그외
				{
					if (vRap.y>0.f)
					{
						player->Coll_Move(vRap, _int(COL_FLAT));
					}
				}

				
			}

		}
	}
}

void CCollision::Coll_Player_Mon()
{



}

void CCollision::Coll_Player_Box()
{

	if (m_CollList[COL_PLAYER].size() <= 0)
		return;
	if (m_CollList[COL_BOX].size() <= 0)
		return;

	CGameObject* player = m_CollList[COL_PLAYER].front();

	RC rcPlayer = {};
	player->GetRC(rcPlayer);

	_vec3 vRap = {};

	for (CGameObject*& box : m_CollList[COL_BOX])
	{
		RC rcBox = { 0.f,0.f,0.f,0.f,rcPlayer.plane ,rcPlayer.climeDir,rcPlayer.vPos };//플레이어의면,위치


		if (box->GetRC(rcBox))//트루일떄만 충돌처리
		{
			if (Coll_Rect2(rcPlayer, rcBox, &vRap))
			{
				box->Coll_Move(vRap, _int(COL_PLAYER));
				player->Coll_Move(vRap, _int(COL_BOX));
				
			}
		}

	}

}

void CCollision::Coll_Box_Normal()
{

	if (m_CollList[COL_BOX].size() <= 0)
		return;
	if (m_CollList[COL_NORMAL].size() <= 0)
		return;
	

	for (CGameObject*& box : m_CollList[COL_BOX])
	{

		CB cb1;
		box->GetCUBE(cb1);
		

		_vec3 vRap = {};

		for (CGameObject*& normal : m_CollList[COL_NORMAL])
		{
			CB cb2;
			normal->GetCUBE(cb2);

			vRap = {0.f,0.f,0.f};
			box->SetBox(vRap);//부딛혔는지 확인을위해

			if (Coll_Cube(cb1, cb2, &vRap))
			{
				box->Coll_Move(vRap, _int(COL_NORMAL));
			}

		}

	}


}

void CCollision::Coll_Box_Building()
{


	if (m_CollList[COL_BOX].size() <= 0)
		return;
	if (m_CollList[COL_BUILDING].size() <= 0)
		return;


	for (CGameObject*& box : m_CollList[COL_BOX])
	{

		CB cb1;
		box->GetCUBE(cb1);


		_vec3 vRap = {};

		for (CGameObject*& building : m_CollList[COL_BUILDING])
		{
			CB cb2;
			building->GetCUBE(cb2);

			//트루일떄만 충돌처리
			vRap = {};

			if (Coll_Cube(cb1, cb2, &vRap))
			{
				vRap.x = 0.f;
				vRap.z = 0.f;
				box->Coll_Move(vRap, _int(COL_BUILDING));
			}

		}

	}



}

void CCollision::Coll_Event()
{
	if (m_CollList[COL_PLAYER].size() <= 0)
		return;
	if (m_CollList[COL_EVENT].size() <= 0)
		return;

	CGameObject* player = m_CollList[COL_PLAYER].front();

	RC rcPlayer = {};
	player->GetRC(rcPlayer);


	for (CGameObject*& spring : m_CollList[COL_EVENT])
	{
		RC rcSpring = { 0.f,0.f,0.f,0.f,rcPlayer.plane ,rcPlayer.climeDir,rcPlayer.vPos };//플레이어의면,위치

		if (spring->GetRC(rcSpring))//트루일떄만 충돌처리
		{

			if (Coll_Rect1(rcPlayer, rcSpring))
			{
				player->Coll_GameObject(_int(COL_EVENT));
				spring->Coll_GameObject(_int(COL_PLAYER));
			}

		}


	}

}




HRESULT CCollision::Ready_Collision()
{
	return NOERROR;
}



_bool CCollision::Coll_Rect1(const RC & rc1, const RC & rc2)
{
	if ((rc1.right >= rc2.left) && (rc1.left <= rc2.right))
	{
		if ((rc1.bot <= rc2.top) && (rc1.top >= rc2.bot))
		{
			return true;
		}
	}
	return false;
}



_bool CCollision::Coll_Rect2(const RC & rc1, const RC & rc2,_vec3* _vRap)
{	
	_bool bColl = false;
	if ((rc1.right >= rc2.left) && (rc1.left <= rc2.right))
	{
		if ((rc1.bot <= rc2.top) && (rc1.top >= rc2.bot))
		{
			bColl= true;
		}
	}
	
	if (!bColl)
		return false;

	_vec3 vRap = rc2.vPos - rc1.vPos;
	
	// 두 사각형의 가로의 반지름과 세로의 반지름 합을 구한다.
	_float sumRadX = ((rc1.right - rc1.left) + (rc2.right - rc2.left))*0.5f;
	_float sumRadY = ((rc1.top - rc1.bot) + (rc2.top - rc2.bot))*0.5f;

	

	switch (rc1.plane%5)
	{
	case 0:
	case 2:
		if (vRap.x > 0)
			vRap.x -=sumRadX;
		else
			vRap.x +=sumRadX;

		if (vRap.y > 0)
			vRap.y -= sumRadY;
		else
			vRap.y += sumRadY;
		fabsf(vRap.x) > fabsf(vRap.y) ? vRap.x = 0.f : vRap.y = 0.f;
		vRap.z = 0.f;
		break;
	case 1:
	case 3:
 		if (vRap.z > 0)
			vRap.z -= sumRadX;
		else
			vRap.z += sumRadX;

		if (vRap.y > 0)
			vRap.y -= sumRadY;
		else
			vRap.y += sumRadY;
		fabsf(vRap.z) > fabsf(vRap.y) ? vRap.z = 0.f : vRap.y = 0.f;
		vRap.x = 0.f;
		break;
	case 4:
		if (vRap.x > 0)
			vRap.x -= sumRadX;
		else
			vRap.x += sumRadX;

		if (vRap.z > 0)
			vRap.z -= sumRadY;
		else
			vRap.z += sumRadY;
		fabsf(vRap.x) > fabsf(vRap.z) ? vRap.x = 0.f : vRap.z = 0.f;
		vRap.y = 0.f;
		break;
	}




	*_vRap = vRap;


	return bColl;
}

_bool CCollision::Coll_Rect3(const RC & rc1, const RC & rc2, _vec3* _vRap)
{
	/*if (rc1.plane % 5 != 4)
	{*/
		_bool bColl = false;
		if ((rc1.right >= rc2.left) && (rc1.left  <= rc2.right))
		{
			if ((rc1.bot <= rc2.top) && (rc1.top  >= rc2.bot))
			{
				bColl = true;
			}
		}


		if (!bColl)
			return false;

		_vec3 vRap = rc2.vPos - rc1.vPos;

		// 두 사각형의 가로의 반지름과 세로의 반지름 합을 구한다.
		_float sumRadX = ((rc1.right - rc1.left) + (rc2.right - rc2.left))*0.5f;
		_float sumRadY = ((rc1.top - rc1.bot) + (rc2.top - rc2.bot))*0.5f;



		switch (rc1.plane % 5)
		{
		case 0:
		case 2:
			if (vRap.y > 0)
				vRap.y -= sumRadY;
			else
				vRap.y += sumRadY;

			vRap.x = 0.f;
			vRap.z = 0.f;
			break;
		case 1:
		case 3:

			if (vRap.y > 0)
				vRap.y -= sumRadY;
			else
				vRap.y += sumRadY;

			vRap.x = 0.f;
			vRap.z = 0.f;
			break;
		case 4:
			switch (rc1.climeDir)
			{
			case 0:
				if (vRap.z > 0.f)
					vRap.z -= sumRadY;
				else
					vRap.z += sumRadY;
				vRap.x = 0.f;
				vRap.y = 0.f;
				break;
			case 1:
				if (vRap.x > 0.f)
					vRap.x -= sumRadX;
				else
					vRap.x += sumRadX;
				vRap.z = 0.f;
				vRap.y = 0.f;
				//vRap.x *= -1.f;
				break;
			case 2:
				if (vRap.z > 0.f)
					vRap.z -= sumRadX;
				else
					vRap.x += sumRadX;
				vRap.x = 0.f;
				vRap.y = 0.f;
				break;
			case 3:

				break;
			}
			break;
		}


		*_vRap = vRap;


		return bColl;
	//}
	//else//옥상이면
	//{
	//	switch (rc1.climeDir)
	//	{
	//	case 0:
	//		break;
	//	case 1:
	//		break;
	//	case 2:
	//		break;
	//	case 3:
	//		break;
	//	}


	//}
	
}

_bool CCollision::Coll_Cube(const CB & cb1, const CB & cb2, _vec3* _vRap)
{


	_vec3 vDir = cb2.vPos - cb1.vPos;

	_vec3 vRap = 
		_vec3(fabsf(vDir.x) - (cb1.w*0.5f+cb2.w*0.5f),
			fabsf(vDir.y) - (cb1.h*0.5f + cb2.h*0.5f), 
			fabsf(vDir.z) - (cb1.d*0.5f + cb2.d*0.5f));
	
	if (vRap.x >0.f || vRap.y > 0.f || vRap.z > 0.f)
		return false;

	if (fabs(vRap.x) < fabs(vRap.y))
	{
		if (fabs(vRap.x) < fabs(vRap.z))//x가 가장작다
		{
			if (vDir.x<0.f)
				_vRap->x = vRap.x;
			else
				_vRap->x = -vRap.x;
		}
		else//z가 가장작다
		{
			if (vDir.z<0.f)
				_vRap->z = vRap.z;
			else
				_vRap->z = -vRap.z;
		}
	}
	else
	{
		if (fabs(vRap.y) < fabs(vRap.z))//y가 가장작다
		{
			if (vDir.y<0.f)
				_vRap->y = vRap.y;
			else
				_vRap->y = -vRap.y;
		}
		else //z가 가장작다
		{
			if (vDir.z<0.f)
				_vRap->z = vRap.z;
			else
				_vRap->z = -vRap.z;
		}
	}


	return true;
}

CCollision * CCollision::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCollision* pInstance = new CCollision(pGraphic_Device);
	if (FAILED(pInstance->Ready_Collision()))
	{
		_MSG_BOX("CCollision Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CCollision::Clone_Component()
{
	AddRef();
	return this;
}

void CCollision::Free()
{
	for (size_t i = 0; i < COLL_END; i++)
	{
		if (m_CollList[i].size() <= 0)
			break;

		m_CollList[i].clear();
	}

	CComponent::Free();
}
