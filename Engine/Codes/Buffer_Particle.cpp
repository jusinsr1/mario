#include "..\Headers\Buffer_Particle.h"

CBuffer_Particle::CBuffer_Particle(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVtxBuffer(pGraphic_Device)
{

}

CBuffer_Particle::CBuffer_Particle(const CBuffer_Particle & rhs)
	: CVtxBuffer(rhs),
	m_vOrigin(rhs.m_vOrigin),
	m_fEmitRate(rhs.m_fEmitRate),
	m_fSize(rhs.m_fSize),
	m_bIsDead(rhs.m_bIsDead),
	m_dwOffset(rhs.m_dwOffset), 
	m_dwBatchSize(rhs.m_dwBatchSize),
	m_Particle_Lst(rhs.m_Particle_Lst)
{

}

HRESULT CBuffer_Particle::Ready_Particle(const _uint & iNumParticle)
{
	if (FAILED(CVtxBuffer::Ready_CVtxBuffer(iNumParticle)))
		return E_FAIL;

	m_fSize = 1.f;
	m_dwOffset = 0;
	m_dwBatchSize = ULONG(iNumParticle * 0.5);
	for (size_t i = 0; i < iNumParticle; i++)
		CBuffer_Particle::Add_Attibute();

	return NOERROR;
}

HRESULT CBuffer_Particle::SetUp_Attibute(ATTIBUTE* pBute)
{
	pBute->bIsLive = true;
	pBute->dwColor = D3DXCOLOR(1.f,1.f,1.f,1.f);
	pBute->fTimeLife = 1.f;

	////SetUp Bute
	return NOERROR;
}


HRESULT CBuffer_Particle::Reset_Attibute(ATTIBUTE* pBute , const _int iMin, const _int iMax , const _int iNumPlane , const _uint& iNumType)
{
	_int i = 1 , j = 0 , k = 5;

	m_iType = iNumType;

	if (0 == iNumType)
	{
		for (auto& pMyBute : m_Particle_Lst)
		{
			//pMyBute.vPosition = pBute->vPosition;
			//pMyBute.dwColor = pBute->dwColor;

			random_device random;
			mt19937 engine(random());
			uniform_int_distribution<int> distribution(iMin, iMax);

			_int iRand[3] = {};
			for (size_t i = 0; i < 2; i++)
				iRand[i] = distribution(engine);
			_float fSpeed[3] = {};

			for (size_t i = 0; i < 2; i++)
				fSpeed[i] = 0.2f + (iRand[i] * 0.01f);

			i *= -1;

			//if (0 == iNumPlane)
			//	pMyBute.vVelocity = { fSpeed[0] * i, fSpeed[1] * i , -fSpeed[2] };
			//// vlook -z , z != (0 < z)
			//else if (1 == iNumPlane)
			//	pMyBute.vVelocity = { -fSpeed[0], fSpeed[1] * i, fSpeed[2] * i };
			//// vlook -x , x != (0 < x)
			//else if (2 == iNumPlane)
			//	pMyBute.vVelocity = { fSpeed[0] * i, fSpeed[1] * i , fSpeed[2] };
			//// vlook z  ,  z != (0 > z) 
			//else if (3 == iNumPlane)
			//	pMyBute.vVelocity = { fSpeed[0] , fSpeed[1] * i , -fSpeed[2] * i };
			//// vlook x  ,  x != (0 < x)
			//else if (4 == iNumPlane)
			//	pMyBute.vVelocity = { fSpeed[0] * i, fSpeed[1] , -fSpeed[2] * i };
			//// vlook y ,  y != (0 > y)

			fSpeed[1] += 3.f;
			pMyBute.vVelocity = { fSpeed[0] * i, fSpeed[1] * i, 0.f};
			pMyBute.vAcceleration = { 1.04f, 1.04f, 1.04f };
			pMyBute.fTimeLife = pBute->fTimeLife;
		}
	}
	else
	{
		
		for (auto& pMyBute : m_Particle_Lst)
		{
			m_fSize = 0.4f;

			i *= -1;	j++;	k++;

			if (j >= 10)
				j = 0;
			if (k >= 10)
				k = 5;

			random_device random;
			mt19937 engine(random());
			uniform_int_distribution<int> distribution(0, 15);

			_int iRand[10];
			
			for (size_t i = 0; i < 10; i++)
				iRand[i] = distribution(engine);

			_float fSpeedY = iRand[j] * 0.01f + 0.1f;
		
			_float fSpeedX = iRand[k] * 0.001f + 0.008f;

			pMyBute.vVelocity = { fSpeedX * i, fSpeedY , 0.f};
			pMyBute.vAcceleration = { 1.0008f, 1.001f, 1.001f };
			pMyBute.fTimeLife = pBute->fTimeLife;
		}
	}


	return NOERROR;
}

HRESULT CBuffer_Particle::Add_Attibute()
{
	ATTIBUTE tBute;

	ZeroMemory(&tBute, sizeof(ATTIBUTE));

	SetUp_Attibute(&tBute);

	m_Particle_Lst.push_back(tBute);

	return NOERROR;
}



_int CBuffer_Particle::Update_Particle(const _float & fTimeDelta)
{
	_int i = 1, j = 0, k = 5;

	if(0 == m_iType)
		m_fSize += 0.1f;
	

	for (auto& pBute : m_Particle_Lst)
		pBute.fAge += fTimeDelta;

	for (auto& pBute : m_Particle_Lst)
	{
		pBute.vVelocity.x *= pBute.vAcceleration.x;
		pBute.vVelocity.y *= pBute.vAcceleration.y;
		pBute.vVelocity.z *= pBute.vAcceleration.z;

		pBute.vPosition += pBute.vVelocity  * fTimeDelta;
		
		if (-1 == pBute.fTimeLife)
			break;

		if (pBute.fAge >= pBute.fTimeLife)
			pBute.bIsLive = false;
	}

	Release_Dead_Particle();

	return 0;
}

void CBuffer_Particle::PreRender_Particle()
{
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, false);
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, false);
	m_pGraphic_Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	m_pGraphic_Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSPRITEENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALEENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSIZE, FtoDw(m_fSize));
 	m_pGraphic_Device->SetRenderState(D3DRS_POINTSIZE_MIN, FtoDw(0.0f));
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALE_A, FtoDw(0.f));
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALE_B, FtoDw(0.f));
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALE_C, FtoDw(2.f));

}

void CBuffer_Particle::Render_Particle(LPDIRECT3DTEXTURE9 pTexture)
{
	if (!m_Particle_Lst.empty())
	{

		PreRender_Particle();

		if (nullptr != pTexture)
		m_pGraphic_Device->SetTexture(0, pTexture);
		
		m_pGraphic_Device->SetFVF(m_dwFVF);
		m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, sizeof(PARTICLE));

		if (m_dwOffset >= m_iNumVertices)
			m_dwOffset = 0;

		PARTICLE* pParticles = nullptr;

		m_pVB->Lock(m_dwOffset * sizeof(PARTICLE), m_dwBatchSize * sizeof(PARTICLE),
			(void**)&pParticles, m_dwOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);

		// ex ? ex : ex  _ 삼항 연산자,

		_ulong dwNum_InBatch = 0;


		_int iCnt = 0;

		for (auto& rBute : m_Particle_Lst)
		{
			iCnt++;

			if (rBute.bIsLive)
			{
				// 파티클이 생존시, 다음 버텍스 버퍼 세그먼트에 복사

				pParticles->vPosition = rBute.vPosition;
				pParticles->dwColor = rBute.dwColor;

				pParticles++;
				dwNum_InBatch++;

				if (dwNum_InBatch == m_dwBatchSize)
				{
					m_pVB->Unlock();		

					m_pGraphic_Device->DrawPrimitive(D3DPT_POINTLIST, m_dwOffset, m_dwBatchSize);

					m_dwOffset += m_dwBatchSize;

					if (m_dwOffset >= m_iNumVertices)
						m_dwOffset = 0;

					m_pVB->Lock(m_dwOffset * sizeof(PARTICLE), m_iNumVertices * sizeof(PARTICLE),
						(void**)&pParticles, m_dwOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);


					dwNum_InBatch = 0;
				}
			}
		}

		m_pVB->Unlock();

		if (dwNum_InBatch)
			m_pGraphic_Device->DrawPrimitive(D3DPT_POINTLIST, m_dwOffset, m_dwBatchSize);
		
		m_dwOffset += m_dwBatchSize; 

		PostRender_Particle();
	}
		

}

void CBuffer_Particle::PostRender_Particle()
{
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, true);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSPRITEENABLE, false);
	m_pGraphic_Device->SetRenderState(D3DRS_POINTSCALEENABLE, false);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, true);
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, true);
}

_bool CBuffer_Particle::Check_Empty()
{
	return m_Particle_Lst.empty();
}

_bool CBuffer_Particle::Check_Dead()
{
	return m_bIsDead;
}


void CBuffer_Particle::Release_Dead_Particle()
{
	for (auto iter = m_Particle_Lst.begin();
		iter != m_Particle_Lst.end();)
	{
		if (!iter->bIsLive)
			iter = m_Particle_Lst.erase(iter);
		else
			iter++;
	}
}

CBuffer_Particle * CBuffer_Particle::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint iNumParticle)
{

	CBuffer_Particle* pInstance = new CBuffer_Particle(pGraphic_Device);

	if (FAILED(pInstance->Ready_Particle(iNumParticle)))
		Safe_Release(pInstance);

	return pInstance;
}

CComponent * CBuffer_Particle::Clone_Component()
{
	return new CBuffer_Particle(*this);
}

void CBuffer_Particle::Free()
{

	m_Particle_Lst.clear();

	CVtxBuffer::Free();
}

