#include "..\Headers\Buffer_ViewPort.h"



CBuffer_ViewPort::CBuffer_ViewPort(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{

}

CBuffer_ViewPort::CBuffer_ViewPort(const CBuffer_ViewPort & rhs)
	: CVIBuffer(rhs)
	, m_pOriVertices(rhs.m_pOriVertices)
{


}

HRESULT CBuffer_ViewPort::Ready_VIBuffer()
{
	m_iNumVertices = 4;
	m_iStride = sizeof(VTXVIEWPORT);
	m_dwFVF = D3DFVF_XYZRHW | D3DFVF_TEX1/* | D3DFVF_TEXCOORDSIZE2(0)*/;
	m_pPosition = new _vec3[4];

	m_pOriVertices = new VTXVIEWPORT[4];

	m_iNumPolygons = 2;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXVIEWPORT*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec4(-0.5f, -0.5f, 0.f, 1.f);
	pVertices[0].vTexUV = _vec2(0.0f, 0.f);
	m_pOriVertices[0] = pVertices[0];

	pVertices[1].vPosition = _vec4(0.5f, -0.5f, 0.f, 1.f);
	pVertices[1].vTexUV = _vec2(1.f, 0.f);
	m_pOriVertices[1] = pVertices[1];

	pVertices[2].vPosition = _vec4(0.5f, 0.5f, 0.f, 1.f);
	pVertices[2].vTexUV = _vec2(1.f, 1.f);
	m_pOriVertices[2] = pVertices[2];

	pVertices[3].vPosition = _vec4(-0.5f, 0.5f, 0.f, 1.f);
	pVertices[3].vTexUV = _vec2(0.f, 1.f);
	m_pOriVertices[3] = pVertices[3];

	//for (size_t i = 0; i < m_iNumVertices; ++i)
	//	m_pPosition[i] = pVertices[i].vPosition;	

	m_pVB->Unlock();




	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 3;

	m_pIB->Unlock();


	return NOERROR;
}

void CBuffer_ViewPort::Render_VIBuffer(const _matrix* pWorldMatrix)
{
	if (FAILED(Transform_Vertices(pWorldMatrix)))
		return;

	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
	// m_pGraphic_Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_iNumPolygons);
}

HRESULT CBuffer_ViewPort::Transform_Vertices(const _matrix * pWorldMatrix)
{
	VTXVIEWPORT*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		D3DXVec4Transform(&pVertices[i].vPosition, &m_pOriVertices[i].vPosition, pWorldMatrix);
	}

	m_pVB->Unlock();

	return NOERROR;
}


CBuffer_ViewPort * CBuffer_ViewPort::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_ViewPort*	pInstance = new CBuffer_ViewPort(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_ViewPort Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}


CComponent * CBuffer_ViewPort::Clone_Component()
{
	return new CBuffer_ViewPort(*this);
}

void CBuffer_ViewPort::Free()
{
	if (false == m_isClone)
		Safe_Delete_Array(m_pOriVertices);

	CVIBuffer::Free();
}