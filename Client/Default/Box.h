#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CCollision;
class CBuffer_CubeLine;
_END

_BEGIN(Client)

class CBox final : public CGameObject
{
private:
	explicit CBox(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBox(const CBox& rhs);
	virtual ~CBox() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();

	HRESULT SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag);
	virtual void Coll_Move(_vec3& vMove, _int i);
public:
	virtual _bool GetRC(RC& dir);
	virtual void GetCUBE(CB& cb);
	virtual void SetBox(_vec3& v);
private:
	void AsGravity(const _float & fTImeDelta);
public:
	static CBox* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();

private:
	virtual void Free();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_CubeLine*	m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CCollision*			m_pCollCom = nullptr;

	_vec3 vMask;

	_vec2 vSpd = { 0.f,0.f };
	//_vec3 vPre = {0.f,0.f,0.f};
};

_END