#include "stdafx.h"
#include "Shadow.h"
#include "Management.h"
#include "Buffer_Shadow.h"

_USING(Client)

CShadow::CShadow(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CShadow::CShadow(const CShadow & rhs)
	: CGameObject(rhs)
{
}

HRESULT CShadow::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CShadow::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CShadow::Update_GameObject(const _float & fTImeDelta)
{

	return _int();
}

_int CShadow::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return _int();
}

void CShadow::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;


	m_pTransformCom->SetUp_OnGraphicDev();

	m_pGraphic_Device->SetTexture(0, nullptr);
	//m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pGraphic_Device->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_COLOR1);
	m_pBufferCom->Render_VIBuffer();
	//m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pGraphic_Device->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);

}

HRESULT CShadow::SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag)
{
	m_pTransformCom->Scaling(vecScale.x, vecScale.y, vecScale.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vecPos);
	CManagement* pManagement = GET_INSTANCE(CManagement);


	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();



	Safe_Release(pManagement);

	return NOERROR;
}

CShadow * CShadow::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CShadow* pInstance = new CShadow(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CBuilding Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CShadow::Clone_GameObject()
{
	CShadow* pInstance = new CShadow(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CBuilding Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CShadow::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_Shadow*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_Shadow");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CShadow::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);


	CGameObject::Free();
}
