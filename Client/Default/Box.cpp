#include "stdafx.h"
#include "Box.h"
#include "Management.h"
#include "Buffer_CubeLine.h"
#include "Collision.h"

_USING(Client)

CBox::CBox(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CBox::CBox(const CBox & rhs)
	: CGameObject(rhs)
{
}

HRESULT CBox::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CBox::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	///////////////////////////////////
	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_BOX, this)))
		return -1;

	return NOERROR;
}

_int CBox::Update_GameObject(const _float & fTImeDelta)
{
	AsGravity(fTImeDelta);

	return _int();
}

_int CBox::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CBox::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;


	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev()))
		return;

	m_pBufferCom->Render_VIBuffer();


}

HRESULT CBox::SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag)
{
	m_pTransformCom->Scaling(vecScale.x, vecScale.y, vecScale.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vecPos);
	CManagement* pManagement = GET_INSTANCE(CManagement);


	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, ImgTag);

	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;
	Safe_Release(pManagement);

	return NOERROR;
}

void CBox::Coll_Move(_vec3 & vMove, _int i)
{
	if (i == CCollision::COL_NORMAL)
	{
		vSpd.y = 0.f;
		vMask = vMove;
	}
	else if (i == CCollision::COL_PLAYER)
	{
		if (fabsf(vMask.x) > 0.f)
		{
			vMove.x = 0.f;
		}
		
		if (fabsf(vMask.y) > 0.f)
		{
			vMove.y = 0.f;
		}
		
		if(fabsf(vMask.z) > 0.f)
		{
			vMove.z = 0.f;
		}
	}
	else if (i == CCollision::COL_BUILDING)
	{
		vSpd.y = 0.f;
	}

	m_pTransformCom->MoveV3(-vMove*0.5f);
}

_bool CBox::GetRC(RC & rc)
{

	_int dir = rc.plane;

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_float width = m_pTransformCom->GetCX();
	_float height = m_pTransformCom->GetCY();
	_float depth = m_pTransformCom->GetCZ();

	switch (dir % 5)
	{
	case 0:
	case 2:
		if (rc.vPos.z + 0.1f<vPos.z - depth*0.5f || vPos.z + depth*0.5f< rc.vPos.z - 0.1f)
			return false;
		break;
	case 1:
	case 3:
		if (rc.vPos.x < vPos.x - width*0.5f - 0.1f || vPos.x + width*0.5f + 0.1f < rc.vPos.x)
			return false;
		break;
	case 4:
		if (rc.vPos.y + 0.1f<vPos.y - height*0.5f || vPos.y + height*0.5f< rc.vPos.y - 0.1f)
			return false;
		break;
	}

	m_pTransformCom->GetRC3D(rc);
	return true;
}

void CBox::GetCUBE(CB & cb)
{
	cb.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	cb.w = m_pTransformCom->GetCX();
	cb.h = m_pTransformCom->GetCY();
	cb.d = m_pTransformCom->GetCZ();
}

void CBox::SetBox(_vec3 & v)
{
	vMask = v;
}



void CBox::AsGravity(const _float & fTImeDelta)
{
	
	m_pTransformCom->Move(fTImeDelta, vSpd);

	if (vSpd.y >= -13.f)
	{
		vSpd.y -= 0.4f;
	}

	
	

}


CBox * CBox::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBox* pInstance = new CBox(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CBox Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CBox::Clone_GameObject()
{
	CBox* pInstance = new CBox(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CBox Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CBox::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_CubeLine*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeLine");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	Safe_Release(pManagement);

	return NOERROR;
}

void CBox::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pCollCom);

	CGameObject::Free();
}
