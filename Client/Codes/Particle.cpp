#include "stdafx.h"
#include "Particle.h"

_USING(Client)

CParticle::CParticle(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CEffect(pGraphic_Device)
{
}

CParticle::CParticle(CParticle & rhs)
	:CEffect(rhs)
{
}

HRESULT CParticle::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CParticle::Ready_GameObject(PARTICLE eID , const _int iCurNumPlane)
{	ZeroMemory(&m_Bute, sizeof(ATTIBUTE));

	m_eID = eID;

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (iCurNumPlane == -1)
		m_iNumPlane = 4;
	else
		m_iNumPlane = iCurNumPlane % 5;

	m_pTransformCom->Scaling(1.f, 1.f, 1.f);
	m_pTransformCom->SetUp_Speed(1.f, 1.f);

	if (m_eID == PARTICLE_SMOG)
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION , &_vec3(0.f,0.f,0.f));

	return NOERROR;
}

_int CParticle::Update_GameObject(const _float & fTimeDelta)
{
	if (nullptr != m_pTarget_TransCom)
	{
		m_vRight = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_RIGHT);
		m_vUp = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_UP);
		m_vTargetPos = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_POSITION);
	}

	if (!m_bIsLateReady)
	{
		if (0x80000000 & Ready_State())
			return -1;

		m_pParticleCom->Reset_Attibute(&m_Bute, 30 , 40 , m_iNumPlane , (const _uint)m_eID);

		if(m_eID == PARTICLE_ITEM) 
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);

		m_bIsLateReady = true;
	}

	m_pParticleCom->Update_Particle(fTimeDelta);
	

	return _int();
}

_int CParticle::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return 0;
}

void CParticle::Render_GameObject()
{
	if (nullptr == m_pParticleCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	//m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	
	m_pParticleCom->Render_Particle(m_pTextrues[m_eID]);

}

_int CParticle::Ready_State()
{

	switch (m_eID)
	{

	case PARTICLE_ITEM:
		m_Bute.fTimeLife = 0.5f;
		break;
	case PARTICLE_SMOG:
		//m_Bute.vPosition = m_vTargetPos;
		//m_Bute.dwColor = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
		m_Bute.fTimeLife = 8.f;
		break;
	default:
		break;
	}

	return _int();
}

HRESULT CParticle::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Particle
	switch (m_eID)
	{
	case Client::CParticle::PARTICLE_ITEM:
		m_pParticleCom = (CBuffer_Particle*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_Particle_Item");
		if (FAILED(Add_Component(L"Com_Particle", m_pParticleCom)))
			return E_FAIL;
		break;
	case Client::CParticle::PARTICLE_SMOG:
		m_pParticleCom = (CBuffer_Particle*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_Particle_Smoke");
		if (FAILED(Add_Component(L"Com_Particle", m_pParticleCom)))
			return E_FAIL;
		break;
	case Client::CParticle::PARTICLE_END:
		m_pParticleCom = (CBuffer_Particle*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_Particle_Hit");
		if (FAILED(Add_Component(L"Com_Particle", m_pParticleCom)))
			return E_FAIL;
		break;
	default:
		break;
	}

	if (FAILED(Ready_Texture()))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CParticle::Ready_Texture()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	switch (m_eID)
	{
	case PARTICLE_ITEM:
		
		m_pTextrues[PARTICLE_ITEM] = nullptr;
		
		D3DXCreateTextureFromFile(m_pGraphic_Device, 
			L"../../Resources/Textures/Others/Effect/Item_Effect/Item_Effect_0.png", &m_pTextrues[PARTICLE_ITEM]);
		
		if (nullptr == m_pTextrues[PARTICLE_ITEM])
			return E_FAIL;

		break;
	case PARTICLE_SMOG:
		m_pTextrues[PARTICLE_SMOG] = nullptr;

		D3DXCreateTextureFromFile(m_pGraphic_Device, 
			L"../../Resources/Textures/Others/Effect/Smoke_Effect/Smoke_Effect_0.png", &m_pTextrues[PARTICLE_SMOG]);

		if (nullptr == m_pTextrues[PARTICLE_SMOG])
			return E_FAIL;
		break;
	case PARTICLE_END:
		break;
	default:
		break;
	}		

	Safe_Release(pManagement);

	return NOERROR;
}

CParticle * CParticle::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CParticle* pInstance = new CParticle(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
		Safe_Release(pInstance);

	return pInstance;
}

Engine::CGameObject * CParticle::Clone_GameEffect(const Engine::_uint &iPARTICLE_ID , const _int iCurNumPlane)
{
	CParticle* pInstance = new CParticle(*this);

	if(FAILED(pInstance->Ready_GameObject((PARTICLE)iPARTICLE_ID,iCurNumPlane)))
		Safe_Release(pInstance);

	return pInstance;
}

void CParticle::Free()
{	
	if (m_tInfo.IsColne)
		m_pTextrues[m_eID]->Release();

	Safe_Release(m_pParticleCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pTarget_TransCom);

	CGameObject::Free();
}


CGameObject * CParticle::Clone_GameObject()
{
	return nullptr;
}