#include "stdafx.h"
#include "..\Headers\Buffer_Shadow.h"


_USING(Client)

CBuffer_Shadow::CBuffer_Shadow(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{

}

CBuffer_Shadow::CBuffer_Shadow(const CBuffer_Shadow & rhs)
	: CVIBuffer(rhs)
{

}

HRESULT CBuffer_Shadow::Ready_VIBuffer()
{
	m_iNumVertices = 6;
	m_iStride = sizeof(VTXCOL);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_DIFFUSE;

	m_iNumPolygons = 4;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;
	//////////////////////////////////////////////////////////////////////
	
	m_pPosition = new _vec3[m_iNumVertices];
	
	matSha = CreateShadowMatrix();
	
	SetVertex();
	ToShadow();
	




	/////////////////////////////////////////////////////////////////////
	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;


	VTXCOL*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);


	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		pVertices[i].vPosition = m_pPosition[i];
		pVertices[i].dwColor = D3DXCOLOR(0.f, 0.f, 0.f, 0.3f);
	}



	m_pVB->Unlock();



	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);
	/*
	pIndices[0]._0 = 2;
	pIndices[0]._1 = 0;
	pIndices[0]._2 = 1;

	pIndices[1]._0 = 2;
	pIndices[1]._1 = 1;
	pIndices[1]._2 = 3;

	pIndices[2]._0 = 4;
	pIndices[2]._1 = 2;
	pIndices[2]._2 = 3;

	pIndices[3]._0 = 4;
	pIndices[3]._1 = 3;
	pIndices[3]._2 = 5;
	*/
	pIndices[0]._0 = 0;
	pIndices[0]._1 = 2;
	pIndices[0]._2 = 1;

	pIndices[1]._0 = 2;
	pIndices[1]._1 = 3;
	pIndices[1]._2 = 1;

	pIndices[2]._0 = 2;
	pIndices[2]._1 = 4;
	pIndices[2]._2 = 3;

	pIndices[3]._0 = 4;
	pIndices[3]._1 = 5;
	pIndices[3]._2 = 3;

	m_pIB->Unlock();





	return NOERROR;
}

void CBuffer_Shadow::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

_matrix CBuffer_Shadow::CreateShadowMatrix()
{
	_matrix			matShadowed;
	D3DXVECTOR3 dir = { 0.5f, -0.3f, 0.3f };

	D3DXMatrixIdentity(&matShadowed);		// 단위행렬로
	matShadowed._21 = -dir.x / dir.y;
	matShadowed._22 = 0.f;
	matShadowed._23 = -dir.z / dir.y;
	matShadowed._41 = dir.x / dir.y * (-0.5f);
	matShadowed._42 = (-0.5f) + 0.001f;			// 지면에서 뜨도록 미세조정
	matShadowed._43 = dir.z / dir.y * (-0.5f);

	return matShadowed;							// 결과
}

void CBuffer_Shadow::SetVertex()
{
	//m_pPosition[0] = _vec3(-0.5f, 0.5f, -0.5f);
	//m_pPosition[1] = _vec3(-0.5f, -0.5f, -0.5f);
	//m_pPosition[2] = _vec3(-0.5f, 0.5f, 0.5f);
	//m_pPosition[3] = _vec3(-0.5f, -0.5f, 0.5f);
	//m_pPosition[4] = _vec3(0.5f, 0.5f, 0.5f);
	//m_pPosition[5] = _vec3(0.5f, -0.5f, 0.5f);

	
	m_pPosition[0] = _vec3(-0.5f, 0.5f, 0.5f);
	m_pPosition[1] = _vec3(-0.5f, -0.5f, 0.5f);
	m_pPosition[2] = _vec3(0.5f, 0.5f, 0.5f);
	m_pPosition[3] = _vec3(0.5f, -0.5f, 0.5f);
	m_pPosition[4] = _vec3(0.5f, 0.5f, -0.5f);
	m_pPosition[5] = _vec3(0.5f, -0.5f, -0.5f);

}

void CBuffer_Shadow::ToShadow()
{

	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		D3DXVec3TransformCoord(&m_pPosition[i], &m_pPosition[i], &matSha);
	}

	_vec3 a =m_pPosition[0];
	_vec3 b = m_pPosition[1];
	_vec3 c = m_pPosition[2];
	_vec3 d = m_pPosition[3];
	_vec3 e = m_pPosition[4];
	_vec3 f = m_pPosition[5];
	int z = 0;
}



CBuffer_Shadow * CBuffer_Shadow::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_Shadow*	pInstance = new CBuffer_Shadow(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_Shadow Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}


CComponent * CBuffer_Shadow::Clone_Component()
{
	return new CBuffer_Shadow(*this);
}

void CBuffer_Shadow::Free()
{
	Safe_Delete_Array(m_pPosition);

	CVIBuffer::Free();
}
