#include "stdafx.h"
#include "..\Headers\Effect_Player.h"
#include "Collision.h"


_USING(Client)

void CEffect_Player::Coll_GameObject(_int colType)
{

}

_bool CEffect_Player::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}

CEffect_Player::CEffect_Player(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CEffect(pGraphic_Device)
{

}

CEffect_Player::CEffect_Player(CEffect_Player & rhs)
	:CEffect(rhs)
{
}

HRESULT CEffect_Player::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CEffect_Player::Ready_GameObject(EffectID eID , const _int iCurNumPlane)
{
	
	m_eEffectID = eID;

	if (FAILED(Ready_Component()))
		return E_FAIL;


	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_EFF, this)))
		return -1;


	if (iCurNumPlane == -1)
		m_iNumPlane = 4;
	else
		m_iNumPlane = iCurNumPlane % 5;

	m_pTransformCom->Scaling(2.f, 2.f, 2.f);
	m_pTransformCom->SetUp_Speed(1.f, 1.f);

	return NOERROR;
}

_int CEffect_Player::Update_GameObject(const _float & fTimeDelta)
{
	m_vRight = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_RIGHT);
	m_vUp = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_UP);
	
	m_vTargetPos = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_POSITION);

	if (!m_bIsReady)
	{
		if (0 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(0.f));
		else if (1 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(90.f));
		else if (2 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(180.f));
		else if (3 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(270.f));
		
		if (0x80000000 & Update_State(fTimeDelta))
			return -1;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);

		m_bIsReady = true;
	}

		if (m_eEffectID == EFFECT_BSKILL_LEFT)
		{
			m_fFramePerSec = 0.01f;
			m_vTargetPos -= m_vRight * 0.7f;
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);
		}
		else if (m_eEffectID == EFFECT_BSKILL_RIGHT)
		{
			m_fFramePerSec = 0.01f;
			m_vTargetPos -= m_vRight * -0.7f;
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);
		}

		if (m_eEffectID == EFFECT_ASKILL_LEFT)
		{
			m_fFramePerSec = 0.05f;
			m_vTargetPos -= m_vRight * 0.6f;
			m_vTargetPos -= m_vUp * + 0.2f;
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);
		}
		else if (m_eEffectID == EFFECT_ASKILL_RIGHT)
		{
			m_fFramePerSec = 0.05f;
			m_vTargetPos -= m_vRight * -0.6f;
			m_vTargetPos -= m_vUp * + 0.2f;
			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);
		}


	// Move_Frame_Dead	=====================================================================

		
	_bool	bIsPermit = false;

	return _int();
}

_int CEffect_Player::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;



	return 0;
}

void CEffect_Player::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
}

_int CEffect_Player::Update_State(const _float & fTimeDelta)
{


	return _int();
}

HRESULT CEffect_Player::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	if (FAILED(Ready_Texture()))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CEffect_Player::Ready_Texture()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	switch (m_eEffectID)
	{
	case EFFECT_ACOMBO_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"A_Combo_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_BCOMBO_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"B_Combo_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.2f;
		break;
	case EFFECT_CCOMBO_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"C_Combo_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_ACOMBO_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"A_Combo_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_BCOMBO_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"B_Combo_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.2f;
		break;
	case EFFECT_CCOMBO_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"C_Combo_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_ASKILL_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Skill_TypeA_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_ASKILL_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Skill_TypeA_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_BSKILL_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Skill_TypeB_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_BSKILL_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Skill_TypeB_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_CSKILL_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Skill_TypeC_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_CSKILL_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Skill_TypeC_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case EFFECT_MOVE_LEFT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Move_Effect_Left");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		break;
	case EFFECT_MOVE_RIGHT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Move_Effect_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		break;
	case EFFECT_JUMP:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Move_Effect_Right");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		break;

	case EFFECT_END:
		break;
	default:
		break;
	}

	m_iNumTex = m_pTextureCom->GetTexSize();
	

	Safe_Release(pManagement);

	return NOERROR;
}

CEffect_Player * CEffect_Player::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CEffect_Player* pInstance = new CEffect_Player(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
		Safe_Release(pInstance);

	return pInstance;
}

Engine::CGameObject * CEffect_Player::Clone_GameEffect(const Engine::_uint &iEffectID , const _int iCurNumPlane)
{
	CEffect_Player* pInstance = new CEffect_Player(*this);

	if(FAILED(pInstance->Ready_GameObject((EffectID)iEffectID,iCurNumPlane)))
		Safe_Release(pInstance);

	return pInstance;
}

void CEffect_Player::Free()
{	

	if (m_tInfo.IsColne)
		m_pCollCom->Erase_Object(this);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pTarget_TransCom);

	CGameObject::Free();
}


CGameObject * CEffect_Player::Clone_GameObject()
{
	return nullptr;
}