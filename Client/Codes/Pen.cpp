#include "stdafx.h"
#include "Pen.h"
#include "Management.h"
#include "Collision.h"
#include "Effect_Monster.h"

_USING(Client)

CPen::CPen(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CPen::CPen(const CPen & rhs)
	: CGameObject(rhs)
{
	
}

void CPen::Coll_GameObject(_int colType)
{	

	CEffect_Monster* pEffect = nullptr;
	CManagement*		pManagement = CManagement::GetInstance();
	pManagement->AddRef();

	pManagement->Add_GameEffectToLayer(L"Monster_Effect", SCENE_STAGE,
		L"Layer_Effect", CEffect_Monster::MONSTER_EFFECT_OCT, m_iNumPlane % 5, (CGameObject**)&pEffect);

	Safe_Release(pManagement);

	m_eCurState = PEN_DEAD;
}

_bool CPen::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}


// 원형객체 생성될 때 호출.
HRESULT CPen::Ready_Prototype()
{
	// 파일 입출력 등, 초기화에 시간이 걸리는 데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CPen::Ready_GameObject()
{
	// 실제 사용되기위한 객체들 만의 추가적인 데이터를 셋.

	// 현재 객체에게 필요한 컴포넌트들을 복제해서 온다.
		
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_MON, this)))
		return -1;

	return NOERROR;
}

_int CPen::Update_GameObject(const _float & fTimeDelta)
{	
	if (!m_bIsNone)
	{
		_int iTargetPlaneNum = CEvent_Manager::GetInstance()->Get_Player_PlaneNum();

		if (m_iNumPlane == iTargetPlaneNum)
			m_bIsNone = true;
		else
			return 0;
	}
	
	// Reset_State
	if (FAILED(Ready_State()))
		return -1;

	_int iProcess = Update_State(fTimeDelta);

	if (0x80000000 & iProcess)
		return -1;

	return iProcess;
}

_int CPen::LastUpdate_GameObject(const _float & fTimeDelta)
{
	// Move_Frame
	if (FAILED(Move_Frame(0.3f, fTimeDelta, m_pTextureCom[PEN_IDLE]->GetTexSize())))
		return -1;
	
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CPen::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();
	

	if (FAILED(m_pTextureCom[PEN_IDLE]->SetUp_OnGraphicDev(m_iTexIdx)))
		return;


	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	
}

void CPen::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_iNumPlane = iPlaneNum;
	_vec3 TexSize = *m_pTextureCom[PEN_IDLE]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);
	//m_pTransformCom->BackWard(0.001f);
	m_iType = iVariable;
}


HRESULT CPen::Ready_State()
{
	if (PEN_END == m_eCurState)
		return E_FAIL;

	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case PEN_NONE:
			break;
		case PEN_IDLE:
		{
			random_device random;
			mt19937 engine(random());
			uniform_int_distribution<int> distribution(0, 10);
			m_fMyTimeBegin = (_float)distribution(engine);		
		}
			break;
		case PEN_MOVE:
			m_fSpeed = 0.02f;
			break;
		case PEN_DEAD:
			break;
		case PEN_END:
			break;
		default:
			return E_FAIL;
		}

		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int CPen::Update_State(const _float & fTimeDelta)
{
		switch (m_eCurState)
		{
		case PEN_NONE:
			break;
		case PEN_IDLE:
		{
			m_fTimeBegin += fTimeDelta;
		
			if (m_fTimeBegin >= m_fMyTimeBegin)
				m_eCurState = PEN_MOVE;
		}
		break;
		case PEN_MOVE:
		{
			m_fTime += fTimeDelta;

			_vec3 vRight	= *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
			_vec3 vUp		= *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
			_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

			if (1.0f < m_fTime)
			{
				m_fTime = 0.f;
				m_fSpeed *= -1.f;
			}
			
			if (0 == m_iType)
				vPosition -= vRight * m_fSpeed;
			else
				vPosition -= vUp * m_fSpeed;

			m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
		}
		break;
		case PEN_END:
			break;
		case PEN_DEAD:
			return 1;
		default:
			return E_FAIL;
		}



	return _int();
}

HRESULT CPen::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;	

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	 //For.Com_Buffer
 	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
		if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
			return E_FAIL;

	m_pTextureCom[PEN_IDLE] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Pen");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom[PEN_IDLE])))
			return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
		if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
			return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CPen * CPen::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPen*	pInstance = new CPen(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CPen Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CPen::Clone_GameObject()
{

	CPen*	pInstance = new CPen(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CPen Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CPen::Free()
{
	for (size_t i = 0; i < PEN_END; i++)
		Safe_Release(m_pTextureCom[i]);

	if (m_bIsColne)
		m_pCollCom->Erase_Object(this);
	Safe_Release(m_pCollCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
