#include "stdafx.h"
#include "Alien.h"
#include "Management.h"
#include "Collision.h"
#include "Effect_Monster.h"

_USING(Client)

void CAlien::Coll_GameObject(_int colType)
{
	if (m_eCurState != ALIEN_BEATTACK && m_eCurState != ALIEN_BLOCK)
	{
		CEffect_Monster* pEffect = nullptr;
		CManagement*		pManagement = CManagement::GetInstance();
		pManagement->AddRef();

		pManagement->Add_GameEffectToLayer(L"Monster_Effect", SCENE_STAGE,
			L"Layer_Effect", CEffect_Monster::MONSTER_EFFECT_OCT, m_iNumPlane % 5, (CGameObject**)&pEffect);

		Safe_Release(pManagement);

		m_eCurState = ALIEN_BEATTACK;
	}

}

_bool CAlien::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}

CAlien::CAlien(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CAlien::CAlien(const CAlien & rhs)
	: CGameObject(rhs)
{
	
}

HRESULT CAlien::Ready_Prototype()
{
	
	
	return NOERROR;
}

HRESULT CAlien::Ready_GameObject()
{	
	
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_MON, this)))
		return -1;
	

	m_fFrame_Speed = 0.2f;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(10.f ,10.f,10.f));

	return NOERROR;
}

_int CAlien::Update_GameObject(const _float & fTimeDelta)
{	

	if (!m_bIsNone)
	{
		_int iTargetPlaneNum = CEvent_Manager::GetInstance()->Get_Player_PlaneNum();

		if (m_iNumPlane == iTargetPlaneNum)
			m_bIsNone = true;
		else
			return 0;
	}

	// Reset_State
	if (FAILED(Ready_State()))
		return -1;

	// Repeat_State
	
	_int iProcess = Update_State(fTimeDelta);
	
	if (0x80000000 & iProcess)
		return -1;
	
	return iProcess;
}

_int CAlien::LastUpdate_GameObject(const _float & fTimeDelta)
{
	// Move_Frame
	if (FAILED(Move_Frame(m_fFrame_Speed, fTimeDelta, m_pTextureCom[m_eCurState]->GetTexSize())))
		return -1;
	
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return 0;
}

void CAlien::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();
				
	if (FAILED(m_pTextureCom[m_eCurState]->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	
	
}

void CAlien::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation)
{
	_vec3 TexSize = *m_pTextureCom[ALIEN_IDLE]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);
}

HRESULT CAlien::Ready_State()
{

	if (m_eCurState != m_ePreState)
	{
		random_device random;
		mt19937 engine(random());
		uniform_int_distribution<int> distribution(1, 2);

		m_iStateCnt = distribution(engine);

		switch (m_eCurState)
		{
		case CAlien::ALIEN_IDLE:
			m_fFrame_Speed = 0.1f;
			break;
		case CAlien::ALIEN_GAS:
			m_fFrame_Speed = 0.1f;
			break;
		case CAlien::ALIEN_BLOCK:
			m_fFrame_Speed = 0.2f;
			break;
		case CAlien::ALIEN_BEATTACK:
			m_fFrame_Speed = 0.1f;

			m_iHit++;
			if (m_iHit > 3)
				m_eCurState = ALIEN_DEAD;

			break;
		case CAlien::ALIEN_DEAD:
			m_fFrame_Speed = 0.1f;
			break;
		default:
			return E_FAIL;
		}

		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int CAlien::Update_State(const _float & fTimeDelta)
{
	if (ALIEN_END == m_eCurState)
		return -1;

	switch (m_eCurState)
	{
	case CAlien::ALIEN_IDLE:
	{
		random_device random;
		mt19937 engine(random());
		uniform_int_distribution<int> distribution(1, 2);

		m_iStateCnt = distribution(engine);
		m_eCurState = (ALIEN)m_iStateCnt;
	}
		break;
	case CAlien::ALIEN_MOVE:
	{

		m_fTime += fTimeDelta;
		m_fTimeMove += fTimeDelta;

		if (m_fTimeMove > 2.f)
		{
			m_eCurState = ALIEN_IDLE;
			m_fTimeMove = 0.f;
			break;
		}

		_vec3 vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
		_vec3 vUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
		_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

		if (1.0f < m_fTime)
		{
			m_fTime = 0.f;
			m_fSpeed *= -1.f;
		}

		vPosition -= vRight * m_fSpeed;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPosition);

	}
	break;
	case CAlien::ALIEN_GAS:
		if (m_tInfo.iTexIdx > m_pTextureCom[m_eCurState]->GetTexSize()-1)
			m_eCurState = ALIEN_IDLE;			
		break;
	case CAlien::ALIEN_BLOCK:
		if (m_tInfo.iTexIdx > m_pTextureCom[m_eCurState]->GetTexSize() - 1)
			m_eCurState = ALIEN_IDLE;
		break;
	case CAlien::ALIEN_BEATTACK:
		if (m_tInfo.iTexIdx > m_pTextureCom[m_eCurState]->GetTexSize() - 1)
			m_eCurState = ALIEN_IDLE;
		break;
	case CAlien::ALIEN_DEAD:
		return 1;
	default:
		return -1;
	}

	return 0;
}

HRESULT CAlien::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTextureCom[ALIEN_IDLE] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Alien_Idle");
	if (FAILED(Add_Component(L"Com_Texture_Move", m_pTextureCom[ALIEN_IDLE])))
		return E_FAIL;
	m_pTextureCom[ALIEN_MOVE] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Alien_Idle");
	if (FAILED(Add_Component(L"Com_Texture_Idle", m_pTextureCom[ALIEN_MOVE])))
		return E_FAIL;
	m_pTextureCom[ALIEN_GAS] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Alien_Gas");
	if (FAILED(Add_Component(L"Com_Texture_Gas", m_pTextureCom[ALIEN_GAS])))
		return E_FAIL;
	m_pTextureCom[ALIEN_BLOCK] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Alien_Block");
	if (FAILED(Add_Component(L"Com_Texture_Block", m_pTextureCom[ALIEN_BLOCK])))
		return E_FAIL;
	m_pTextureCom[ALIEN_BEATTACK] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Alien_BeAttack");
	if (FAILED(Add_Component(L"Com_Texture_BeAttack", m_pTextureCom[ALIEN_BEATTACK])))
		return E_FAIL;
	m_pTextureCom[ALIEN_DEAD] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Alien_Dead");
	if (FAILED(Add_Component(L"Com_Texture_Dead", m_pTextureCom[ALIEN_DEAD])))
		return E_FAIL;
	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;

	Safe_Release(pManagement);


	return NOERROR;
}
CAlien * CAlien::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CAlien*	pInstance = new CAlien(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CAlien Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CAlien::Clone_GameObject()
{

	CAlien*	pInstance = new CAlien(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CAlien Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CAlien::Free()
{

	if (m_tInfo.IsColne)
		m_pCollCom->Erase_Object(this);
	
	Safe_Release(m_pCollCom);

	for (size_t i = 0; i < ALIEN_END; i++)
		Safe_Release(m_pTextureCom[i]);

	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
