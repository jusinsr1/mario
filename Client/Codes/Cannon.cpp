#include "stdafx.h"
#include "Cannon.h"
#include "Management.h"
#include "Collision.h"


_USING(Client)

CCannon::CCannon(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device),
	m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	m_pInput_Device->AddRef();
}

CCannon::CCannon(const CCannon & rhs)
	: CGameObject(rhs), 
	m_pInput_Device(GET_INSTANCE(CInput_Device))
{
	m_pInput_Device->AddRef();
}


// Prototype
HRESULT CCannon::Ready_Prototype()
{
	return NOERROR;
}

// Clone
HRESULT CCannon::Ready_GameObject()
{
		
	if (FAILED(Ready_Component()))
		return E_FAIL;
	
	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_EVENT, this)))
		return -1;

	_vec3 TexSize = *m_pTextureCom[m_eType]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(10.0f, 10.f, 0.f));
	
	return NOERROR;
}

_int CCannon::Update_GameObject(const _float & fTimeDelta)
{	
	if (FAILED(Late_Ready_GameObject()))
		return -1;

	if (0x80000000 & Update_State(fTimeDelta))
		return -1;

	return _int();
}

_int CCannon::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(
		CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CCannon::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();
				
	if (FAILED(m_pTextureCom[m_eType]->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	
}

void CCannon::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_iNumPlane = iPlaneNum;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);
	_vec3 TexSize = *m_pTextureCom[m_eType]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.01f, TexSize.y*0.01f, TexSize.z);

	m_eType = (CANNON)iVariable;
}

void CCannon::Coll_GameObject(_int colType)
{
	if (STATE_IN == m_eCurState || STATE_OUT == m_eCurState)
		return;

	m_eCurState = STATE_IN;

	CEvent_Manager::GetInstance()->Create_Event(L"Player", 1);
		
}

_bool CCannon::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}

HRESULT CCannon::Late_Ready_GameObject()
{
	if (STATE_END == m_eCurState)
		return E_FAIL;

	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case Client::CCannon::STATE_NONE:
			m_iTexIdx = 0;
			break;
		case Client::CCannon::STATE_IN:
			m_iTexIdx = 1;
			CEvent_Manager::GetInstance()->Create_Event(L"Player",2 , m_pTransformCom);
			break;
		case Client::CCannon::STATE_OUT:
			m_iTexIdx = 2;
			break;
		case Client::CCannon::STATE_END:
			break;
		default:
			break;
		}

		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int CCannon::Update_State(const _float& fTimeDelta)
{
	switch (m_eCurState)
	{
	case Client::CCannon::STATE_NONE:
		break;
	case Client::CCannon::STATE_IN:
		// 플레이어 렌더콜X && 캐논 회전 가능

		m_fTime += fTimeDelta;

		if (m_fTime > 1.f)
			Input_Key();

		m_pTransformCom->Rotation_AxisLook(fTimeDelta , m_fSeta);
		CEvent_Manager::GetInstance()->SetSeta(m_fSeta);
		break;

	case Client::CCannon::STATE_OUT:
		// 플레이어 발사
		CEvent_Manager::GetInstance()->Create_Event(L"Player", 2);
		m_fTime += fTimeDelta;
		
		if (m_fTime > 1.f)
		{
			m_fTime = 0;

			m_eCurState = STATE_NONE;
		}
		break;
	case Client::CCannon::STATE_END:
		break;
	default:
		break;
	}

	return _int();
}

void CCannon::Input_Key()
{
	if (m_fSeta > 360)
		m_fSeta = 360;
	else if (m_fSeta < 0)
		m_fSeta = 0;

	if (m_pInput_Device->IsKeyPressing(DIK_LEFT))
		m_fSeta += 2.f;
	else if (m_pInput_Device->IsKeyPressing(DIK_RIGHT))
		m_fSeta -= 2.f;

	if (m_pInput_Device->IsKeyDown(DIK_UP))
		m_eCurState = STATE_OUT;

}

HRESULT CCannon::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;	

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	 //For.Com_Buffer
 	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
		if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
			return E_FAIL;

	 //For.Com_Texture
	m_pTextureCom[CANNON_TYPEA] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Cannon_TypeA");
		if (FAILED(Add_Component(L"Com_TextureA", m_pTextureCom[CANNON_TYPEA])))
			return E_FAIL;

	m_pTextureCom[CANNON_TYPEB] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Cannon_TypeB");
		if (FAILED(Add_Component(L"Com_TextureB", m_pTextureCom[CANNON_TYPEB])))
			return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
		if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
			return E_FAIL;
	


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CCannon * CCannon::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCannon*	pInstance = new CCannon(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCannon Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CCannon::Clone_GameObject()
{

	CCannon*	pInstance = new CCannon(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCannon Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCannon::Free()
{
	
	if (m_bIsColne)
		m_pCollCom->Erase_Object(this);

	for (size_t i = 0; i < CANNON_END; i++)
		Safe_Release(m_pTextureCom[i]);
	
	Safe_Release(m_pCollCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pInput_Device);
	

	CGameObject::Free();
}
