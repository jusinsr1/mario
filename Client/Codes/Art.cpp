#include "stdafx.h"
#include "Art.h"
#include "Management.h"

_USING(Client)

CArt::CArt(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CArt::CArt(const CArt & rhs)
	: CGameObject(rhs)
{

}


HRESULT CArt::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CArt::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CArt::Update_GameObject(const _float & fTimeDelta)
{
	
	return _int();
}

_int CArt::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CArt::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();


	if (FAILED(m_pTextureCom[m_eType]->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CArt::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_eType = (ART)iVariable;
	m_iNumPlane = iPlaneNum;

	random_device random;
	mt19937 engine(random());
	uniform_int_distribution<int> distribution(0, m_pTextureCom[m_eType]->GetTexSize());

	m_tInfo.iTexIdx = distribution(engine);

	_vec3 TexSize = *m_pTextureCom[m_eType]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

	m_pTransformCom->BackWard(0.001f);
}

HRESULT CArt::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pTextureCom[BIG_POSTER] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"PosterBig");
	if (FAILED(Add_Component(L"Com_Texture_PosterBig", m_pTextureCom[BIG_POSTER])))
		return E_FAIL;
	m_pTextureCom[SMALL_POSTER] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"PosterSmall");
	if (FAILED(Add_Component(L"Com_Texture_PosterSmall", m_pTextureCom[SMALL_POSTER])))
		return E_FAIL;
	m_pTextureCom[GRAFFITI] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Graffiti");
	if (FAILED(Add_Component(L"Com_Texture_Graffiti", m_pTextureCom[GRAFFITI])))
		return E_FAIL;

	Safe_Release(pManagement);


	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CArt * CArt::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CArt*	pInstance = new CArt(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CArt Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CArt::Clone_GameObject()
{
 	CArt*	pInstance = new CArt(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CArt::Free()
{
	for (size_t i = 0; i < ART_END; i++)
		Safe_Release(m_pTextureCom[i]);

	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
