#include "stdafx.h"
#include "Jumping.h"
#include "Management.h"
#include "Collision.h"

_USING(Client)

CJumping::CJumping(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CJumping::CJumping(const CJumping & rhs)
	: CGameObject(rhs)
{

}

void CJumping::Coll_GameObject(_int colType)
{
	m_eCurState = STATE_JUMP;
}

_bool CJumping::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}

void CJumping::Get_Event_GameObject(const _int iType, CComponent * pComponent)
{
}

// Prototype
HRESULT CJumping::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CJumping::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_SPRING, this)))
		return -1;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.01f, TexSize.y*0.01f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(15.0f, 0.f, 0.f));


	m_tInfo.iTexIdx = 0;

	return NOERROR;
}

_int CJumping::Update_GameObject(const _float & fTimeDelta)
{

	if (FAILED(Change_Motion()))
		return -1;

	if (0x80000000 & Update_Motion(fTimeDelta))
		return -1;

	return _int();
}

_int CJumping::LastUpdate_GameObject(const _float & fTimeDelta)
{
	Move_Frame(m_tInfo.fFrame_Speed, fTimeDelta, m_pTextureCom->GetTexSize());

	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CJumping::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CJumping::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation)
{
	m_iNumPlane = iPlaneNum;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);
}

HRESULT CJumping::Change_Motion()
{
	if (m_eCurState != m_ePreState)
	{

		switch (m_eCurState)
		{
		case Client::CJumping::STATE_NONE:
			m_tInfo.iTexIdx = 0;
			break;
		case Client::CJumping::STATE_JUMP:
			break;
		case Client::CJumping::STATE_END:
			break;
		default:
			break;
		}
		
		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int CJumping::Update_Motion(const _float & fTimeDelta)
{
	switch (m_eCurState)
	{
	case Client::CJumping::STATE_NONE:
		break;
	case Client::CJumping::STATE_JUMP:		
		break;
	case Client::CJumping::STATE_END:
		break;
	default:
		break;
	}

	return _int();
}



HRESULT CJumping::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Jumping");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CJumping * CJumping::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CJumping*	pInstance = new CJumping(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CJumping Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CJumping::Clone_GameObject()
{

	CJumping*	pInstance = new CJumping(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CJumping Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CJumping::Free()
{

	if (m_tInfo.IsColne)
		m_pCollCom->Erase_Object(this);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pCollCom);

	CGameObject::Free();
}