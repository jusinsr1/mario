#include "stdafx.h"
#include "..\Headers\Normal.h"
#include "Management.h"
#include "Buffer_CubeLine.h"
#include "Collision.h"

_USING(Client)

CNormal::CNormal(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CNormal::CNormal(const CNormal & rhs)
	: CGameObject(rhs)
{
}

HRESULT CNormal::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CNormal::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_NORMAL, this)))
		return -1;

	return NOERROR;
}

_int CNormal::Update_GameObject(const _float & fTImeDelta)
{

	return _int();
}

void CNormal::GetCUBE(CB & cb)
{
	cb.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	cb.w = m_pTransformCom->GetCX();
	cb.h = m_pTransformCom->GetCY();
	cb.d = m_pTransformCom->GetCZ();
}

_int CNormal::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CNormal::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

		

	m_pTransformCom->SetUp_OnGraphicDev();

	//if (FAILED(m_pTextureCom->SetUp_OnGraphicDev()))
	//	return;

	//m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, true);
	m_pBufferCom->Render_VIBuffer();
	//m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, false);

}

HRESULT CNormal::SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag)
{
	m_pTransformCom->Scaling(vecScale.x, vecScale.y, vecScale.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vecPos);
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, ImgTag);
	
	if (nullptr == m_pTextureCom)
	{
		Safe_Release(pManagement);
		return NOERROR;
	}

 	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;
	Safe_Release(pManagement);

	return NOERROR;
}

_bool CNormal::GetRC(RC & rc)
{
	_int dir = rc.plane;

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_float width = m_pTransformCom->GetCX();
	_float height = m_pTransformCom->GetCY();
	_float depth = m_pTransformCom->GetCZ();


	switch (dir % 5)
	{
	case 0:
	case 2:
		if (rc.vPos.z + 0.001f<vPos.z - depth*0.5f || vPos.z + depth*0.5f< rc.vPos.z - 0.001f)
			return false;
		break;
	case 1:
	case 3:
		if (rc.vPos.x+ 0.001f < vPos.x - width*0.5f  || vPos.x + width*0.5f  < rc.vPos.x- 0.001f)
			return false;
		break;
	case 4:
		if (rc.vPos.y + 0.001f<vPos.y - height*0.5f || vPos.y + height*0.5f< rc.vPos.y - 0.001f)
			return false;
		break;
	}

	m_pTransformCom->GetRC3D(rc);

	//cout << rc.left << " " << rc.right << endl;
	return true;
}

CNormal * CNormal::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CNormal* pInstance = new CNormal(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CNormal Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CNormal::Clone_GameObject()
{
	CNormal* pInstance = new CNormal(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CNormal Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CNormal::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_CubeLine*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeLine");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CNormal::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pCollCom);

	CGameObject::Free();
}