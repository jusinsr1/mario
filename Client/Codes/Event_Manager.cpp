#include "stdafx.h"
#include "Event_Manager.h"
#include "Layer.h"
#include "GameObject.h"
#include "Player.h"

_USING(Client)

_IMPLEMENT_SINGLETON(CEvent_Manager)

CEvent_Manager::CEvent_Manager()
{
}

HRESULT CEvent_Manager::Add_Object_Event_Manager(const _tchar * pGameObjectTag, CGameObject * pGameObject)
{
	if (pGameObject == nullptr)
		return E_FAIL;

 	if (Find_GameObject(pGameObjectTag))
		return E_FAIL;

	pGameObject->AddRef();

	m_mapObject.insert({ pGameObjectTag,pGameObject });


	return NOERROR;
}

const _int CEvent_Manager::Get_Player_PlaneNum()
{
	auto& iter = find_if(m_mapObject.begin(), m_mapObject.end(), CFinder_Tag(L"Player"));

	if (iter == m_mapObject.end())
		return NULL;

	return dynamic_cast<CPlayer*>(iter->second)->Get_PlaneNum();
}



void CEvent_Manager::Create_Event(const _tchar* pGameObjectTag , const _uint iType , CComponent* pComponent)
{
	auto& iter = find_if(m_mapObject.begin(), m_mapObject.end(), CFinder_Tag(pGameObjectTag));

	if (iter == m_mapObject.end())
		return;
	
	return iter->second->Get_Event_GameObject(iType, pComponent);
}

HRESULT CEvent_Manager::Erase_Event_Manager(const _tchar * pGameObjectTag)
{
	auto& iter = find_if(m_mapObject.begin(), m_mapObject.end(), CFinder_Tag(pGameObjectTag));

	if (iter == m_mapObject.end())
		return E_FAIL;

	m_mapObject.erase(iter);

	return NOERROR;
}

CGameObject* CEvent_Manager::Find_GameObject(const _tchar* pGameObjectTag)
{
	auto& iter = find_if(m_mapObject.begin(), m_mapObject.end(), CFinder_Tag(pGameObjectTag));

	if (iter == m_mapObject.end())
		return nullptr;

	return iter->second;
}

void CEvent_Manager::Free()
{


	for (auto& Pair : m_mapObject)
		Safe_Release(Pair.second);

	m_mapObject.clear();
}
