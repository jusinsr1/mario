#include "stdafx.h"
#include "Platform.h"
#include "Management.h"
#include "Collision.h"


_USING(Client)

CPlatform::CPlatform(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CPlatform::CPlatform(const CPlatform & rhs)
	: CGameObject(rhs)
{

}


// Prototype
HRESULT CPlatform::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CPlatform::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_FLAT, this)))
		return -1;

	m_tInfo.fFrame_Speed = 1.f;
	m_tInfo.fMove_Speed	 = 1.f;

	return NOERROR;
}

_int CPlatform::Update_GameObject(const _float & fTimeDelta)
{

	return _int();
}

_int CPlatform::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return 0;
}

void CPlatform::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom[m_eType]->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CPlatform::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_iNumPlane = iPlaneNum;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);
	
	m_eType = (PLATFORM_TYPE)iVariable;
	
	m_tInfo.iTexIdx = Get_RandomNumber(0,m_pTextureCom[m_eType]->GetTexSize());

	_vec3 TexSize = *m_pTextureCom[m_eType]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);

	m_pTransformCom->Scaling(TexSize.x *0.01f, TexSize.y*0.01f, TexSize.z);

	m_iNumPlane = iPlaneNum;

}

void CPlatform::Coll_GameObject(_int colType)
{

	
}

_bool CPlatform::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}
	else
		return false;
	
}

void CPlatform::Get_Event_GameObject(const _int iType, CComponent * pComponent)
{
}

HRESULT CPlatform::Change_Motion()
{
	return NOERROR;
}

_int CPlatform::Update_Motion(const _float & fTimeDelta)
{
	return _int();
}

HRESULT CPlatform::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
		return E_FAIL;

	if (FAILED(Ready_Texture(pManagement)))
		return E_FAIL;

	Safe_Release(pManagement);


	return NOERROR;
}

HRESULT CPlatform::Ready_Texture(CManagement * pManagement)
{
	
	//For.Com_Texture
	m_pTextureCom[PLATFOR_ON] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"On_Platform");
	if (FAILED(Add_Component(L"Com_TextureA", m_pTextureCom[PLATFOR_ON])))
		return E_FAIL;

	m_pTextureCom[PLATFOR_SWITCH] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Switch_Platform");
	if (FAILED(Add_Component(L"Com_TextureB", m_pTextureCom[PLATFOR_SWITCH])))
		return E_FAIL;
	
	return NOERROR;
}

// Create_Prototype
CPlatform * CPlatform::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlatform*	pInstance = new CPlatform(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CPlatform Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CPlatform::Clone_GameObject()
{

	CPlatform*	pInstance = new CPlatform(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CPlatform Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CPlatform::Free()
{

	for (size_t i = 0; i < PLATFOR_END; i++)
		Safe_Release(m_pTextureCom[i]);

	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pCollCom);

	CGameObject::Free();
}