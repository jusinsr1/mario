#include "stdafx.h"
#include "..\Headers\Camera_Game.h"
#include "ObjMove.h"

_USING(Client)

CCamera_Game::CCamera_Game(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CCamera(pGraphic_Device)
{
}

CCamera_Game::CCamera_Game(const CCamera_Game & rhs)
	: CCamera(rhs)
{
}

HRESULT CCamera_Game::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCamera_Game::Ready_GameObject()
{
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	m_pTransform->SetUp_Speed(20.f, D3DXToRadian(90.f));


	return NOERROR;
}

_int CCamera_Game::Update_GameObject(const _float & fTimeDelta)
{
	if (m_pInput_Device->IsKeyPressing(DIK_D))
		m_pTransform->Go_Right(fTimeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_A))
		m_pTransform->Go_Left(fTimeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_W))
		m_pTransform->Go_Straight(fTimeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_S))
		m_pTransform->BackWard(fTimeDelta);

	_long MouseMove = 0;
	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_X))
		m_pTransform->Rotation_Y(0.1f*MouseMove*fTimeDelta);
	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_Y))
		m_pTransform->Rotation_AxisRight(0.1f*MouseMove*fTimeDelta);

	if (m_pPlayerMove == nullptr)
		return -1;

	_vec2 PosInPlayer = m_pPlayerMove->Get_Player();
	PLANEINFO* pPlane= m_pPlayerMove->Get_pPlane();
	_int PlayerDir = pPlane->iPlaneNum % 5;

	Default_Camera();

	return _int();
}

_int CCamera_Game::LastUpdate_GameObject(const _float & fTimeDelta)
{
	Invalidate_ViewProjMatrix();

	return _int();
}

void CCamera_Game::Render_GameObject()
{
}

void CCamera_Game::Default_Camera()
{
	PLANEINFO* pPlane = m_pPlayerMove->Get_pPlane();
	_int PlayerDir = pPlane->iPlaneNum % 5;
	_int NextDir = -1;
	_vec3 vRight, vCameraPos, vLook, vUp, PlaneInPlayer,PlaneInPlayer2, CameraDist ,CameraDist2;
	_vec2 PosInPlayer = m_pPlayerMove->Get_Player();
	if ((pPlane->fWidth / 2)- PosInPlayer.x< 3)
	{
		if (pPlane->arrMoveablePlane[3] != -1)
			NextDir = pPlane->arrMoveablePlane[3] % 5;
	}
	else if (-(pPlane->fWidth / 2) - PosInPlayer.x > -3)
	{
		if (pPlane->arrMoveablePlane[1] != -1)
			NextDir = pPlane->arrMoveablePlane[1] % 5;
	}
	//else if ()
	//{
	//	if (pPlane->arrMoveablePlane[3] != -1)
	//		NextDir = pPlane->arrMoveablePlane[3] % 5;
	//}
	//else if ()
	//{
	//	if (pPlane->arrMoveablePlane[3] != -1)
	//		NextDir = pPlane->arrMoveablePlane[3] % 5;
	//}
	if (PlayerDir == 0)
	{
		PlaneInPlayer = { PosInPlayer.x, PosInPlayer.y, 0.f };
		CameraDist = { 0.f, pPlane->fHeight, -50.f };
	}
	else if (PlayerDir == 1)
	{
		PlaneInPlayer = { 0.f, PosInPlayer.y, -PosInPlayer.x };
		CameraDist = { -50.f, pPlane->fHeight, 0.f };
	}
	else if (PlayerDir == 2)
	{
		PlaneInPlayer = { -PosInPlayer.x, PosInPlayer.y, 0.f };
		CameraDist = { 0.f, pPlane->fHeight, +50.f };
	}
	else if (PlayerDir == 3)
	{
		PlaneInPlayer = { 0.f, PosInPlayer.y, PosInPlayer.x };
		CameraDist = { +50.f, pPlane->fHeight, 0.f };
	}
	if (NextDir == 0)
		CameraDist += { 0.f, pPlane->fHeight / 15, -50.f };
	else if (NextDir == 1)
		CameraDist += { -50.f, pPlane->fHeight / 15, 0.f };
	else if (NextDir == 2)
		CameraDist += { 0.f, pPlane->fHeight / 15, +50.f };
	else if (NextDir == 3)
		CameraDist += { +50.f, pPlane->fHeight / 15, 0.f };





	vCameraPos = (pPlane->Pos + PlaneInPlayer)/2 + CameraDist;
	_vec3 Camera = vCameraPos - m_vCurCameraPos;

	_float CameraMove = D3DXVec3Length(&Camera);
	
	if (CameraMove >= 3.f)
	{
		D3DXVec3Normalize(&Camera, &Camera);
		Camera = Camera * 1.f;
	}
	if (CameraMove >= 0.5f)
	{
		D3DXVec3Normalize(&Camera, &Camera);
		Camera = Camera*0.5f;
	}
	m_vCurCameraPos += Camera;
	
	vLook = pPlane->Pos + PlaneInPlayer/5 - m_vCurCameraPos;


	_vec3 vMoveLook = vLook - m_vCurLook;

	_float LookMove = D3DXVec3Length(&vMoveLook);

	if (LookMove >= 0.5f)
	{
		D3DXVec3Normalize(&vMoveLook, &vMoveLook);
		vMoveLook = vMoveLook*0.5f;
	}
	m_vCurLook += vMoveLook*0.1;



	vUp = { 0.f,1.f,0.f };
	D3DXVec3Normalize(&m_vCurLook, &m_vCurLook);
	D3DXVec3Cross(&vRight, &vUp, &m_vCurLook);
	D3DXVec3Normalize(&vRight, &vRight);

	m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransform->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &m_vCurLook);
	m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &m_vCurCameraPos);
}

CCamera_Game * CCamera_Game::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Game* pInstance = new CCamera_Game(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CCamera_Debug Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCamera_Game::Clone_GameObject()
{
	CCamera_Game* pInstance = new CCamera_Game(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CCamera_Game Clone Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Game::Free()
{
	CCamera::Free();
}
