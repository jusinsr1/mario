#include "stdafx.h"
#include "..\Headers\Effect_Monster.h"

_USING(Client)

CEffect_Monster::CEffect_Monster(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CEffect(pGraphic_Device)
{
}

CEffect_Monster::CEffect_Monster(CEffect_Monster & rhs)
	:CEffect(rhs)
{
}

HRESULT CEffect_Monster::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CEffect_Monster::Ready_GameObject(EffectID eID , const _int iCurNumPlane)
{
	
	m_eEffectID = eID;

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (iCurNumPlane == -1)
		m_iNumPlane = 4;
	else
		m_iNumPlane = iCurNumPlane % 5;

	m_pTransformCom->Scaling(2.f, 2.f, 2.f);
	m_pTransformCom->SetUp_Speed(1.f, 1.f);

	m_fSeta = -90.f;

	return NOERROR;
}

_int CEffect_Monster::Update_GameObject(const _float & fTimeDelta)
{
	if (!m_bIsReady)
	{
		if (0 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(0.f));
		else if (1 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(90.f));
		else if (2 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(180.f));
		else if (3 == m_iNumPlane)
			m_pTransformCom->Rotation_Y(D3DXToRadian(270.f));

		m_bIsReady = true;
	}

	_int iProcess = Update_State(fTimeDelta);
	


	if (m_eEffectID == MONSTER_EFFECT_OCT)
	{
		m_fTime += fTimeDelta;

		if (m_fTime > 2.f)
			return 1;
	}
	else
	{

		// Move_Frame_Dead	=====================================================================
	}
	return iProcess;
}

_int CEffect_Monster::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;
	
	return 0;
}

void CEffect_Monster::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
}

_int CEffect_Monster::Update_State(const _float & fTimeDelta)
{
	switch (m_eEffectID)
	{
	case MONSTER_EFFECT_OCT:		

		m_vRight = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_RIGHT);

		m_vUp = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_UP);

		m_vTargetPos = *m_pTarget_TransCom->Get_StateInfo(CTransform::STATE_POSITION);

		m_fSeta += 1.2f;

		m_fSpeedY += 0.1f * -sinf(D3DXToRadian(m_fSeta));

		m_vTargetPos += m_vUp * m_fSpeedY;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);

		m_fSpeedX += 0.08f * cosf(D3DXToRadian(m_fSeta));

		m_vTargetPos -= m_vRight * m_fSpeedX;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &m_vTargetPos);

		break;

	case MONSTER_EFFECT_HIT:

		m_fTime += fTimeDelta;

		if (m_fTime > 2.f)
			return 1;

		break;

	default:
		break;
	}

	return _int();
}

HRESULT CEffect_Monster::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	if (FAILED(Ready_Texture()))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CEffect_Monster::Ready_Texture()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	switch (m_eEffectID)
	{
	case MONSTER_EFFECT_OCT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Oct_Bullet");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		m_fFramePerSec = 0.1f;
		break;
	case MONSTER_EFFECT_HIT:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Hit_Effect");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		break;
	default:
		break;
	}

	m_iNumTex = m_pTextureCom->GetTexSize();
	

	Safe_Release(pManagement);

	return NOERROR;
}

CEffect_Monster * CEffect_Monster::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CEffect_Monster* pInstance = new CEffect_Monster(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
		Safe_Release(pInstance);

	return pInstance;
}

Engine::CGameObject * CEffect_Monster::Clone_GameEffect(const Engine::_uint &iEffectID , const _int iCurNumPlane)
{
	CEffect_Monster* pInstance = new CEffect_Monster(*this);

	if(FAILED(pInstance->Ready_GameObject((EffectID)iEffectID,iCurNumPlane)))
		Safe_Release(pInstance);

	return pInstance;
}

void CEffect_Monster::Free()
{	

	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pTarget_TransCom);

	CGameObject::Free();
}


CGameObject * CEffect_Monster::Clone_GameObject()
{
	return nullptr;
}