#include "stdafx.h"
#include "Coin.h"
#include "Management.h"
#include "Particle.h"
#include "Collision.h"

_USING(Client)

CCoin::CCoin(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCoin::CCoin(const CCoin & rhs)
	: CGameObject(rhs)
{

}

void CCoin::Coll_GameObject(_int colType)
{
	m_eCurState = STATE_EAT;
}

_bool CCoin::GetRC(RC & rc)
{
	if (rc.plane == m_iNumPlane)
	{
		m_pTransformCom->GetRC2D(rc);
		return true;
	}

	return false;
}


// Prototype
HRESULT CCoin::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CCoin::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_EVENT, this)))
		return -1;


	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(30.f, 10.f, 20.f));
	m_pTransformCom->SetUp_Speed(2.f, 1);

	m_tInfo.fFrame_Speed;
	m_tInfo.fMove_Speed;

	return NOERROR;
}

_int CCoin::Update_GameObject(const _float & fTimeDelta)
{

	if (FAILED(Change_Motion()))
		return -1;

	_int iProcess = Update_Motion(fTimeDelta);

	if (-1 == iProcess)
		return -1;


	return iProcess;

}

_int CCoin::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CCoin::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;


	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	
}

void CCoin::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_iNumPlane = iPlaneNum;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

	m_tInfo.iTexIdx = iVariable;
}

HRESULT CCoin::Change_Motion()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case Client::CCoin::STATE_NONE:
			break;
		case Client::CCoin::STATE_EAT:	
			break;
		case Client::CCoin::STATE_END:
			break;
		default:
			break;
		}
		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int CCoin::Update_Motion(const _float & fTimeDelta)
{
	switch (m_eCurState)
	{
	case Client::CCoin::STATE_NONE:
		break;
	case Client::CCoin::STATE_EAT:
		
		m_fSeta-= 1.5f;

		if (m_fSeta < -80.f && !m_bIsParticle)
			Create_Particle(CParticle::PARTICLE_ITEM);
		if (m_fSeta < -90.f)
			m_eCurState = STATE_END;
		else
		{
			m_pTransformCom->BackWard(fTimeDelta);
			m_pTransformCom->Rotation_AxisRight(fTimeDelta, m_fSeta);
		}
		break;
	case Client::CCoin::STATE_END:
		return 1;
		break;
	default:
		break;
	}

	return _int();
}

HRESULT CCoin::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	if (FAILED(Ready_Texture(pManagement)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}



HRESULT CCoin::Ready_Texture(CManagement * pManagement)
{
	//For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Item");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;
	
	return NO_ERROR;
}


void CCoin::Create_Particle(const _uint& iDrawID)
{
	CParticle* pEffect = nullptr;
	CManagement* pManagement = CManagement::GetInstance();
	pManagement->AddRef();
	pManagement->Add_GameEffectToLayer(L"Particle", SCENE_STAGE, L"Effect_Layer", _uint(CParticle::PARTICLE_ITEM),
		m_iNumPlane, (CGameObject**)&pEffect);
	dynamic_cast<CParticle*>(pEffect)->Set_TargetTransCom(m_pTransformCom);
	m_pTransformCom->AddRef();
	m_bIsParticle = true;

	Safe_Release(pManagement);
}


// Create_Prototype
CCoin * CCoin::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCoin*	pInstance = new CCoin(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCoin Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CCoin::Clone_GameObject()
{

	CCoin*	pInstance = new CCoin(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCoin Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCoin::Free()
{

	if (m_tInfo.IsColne)
		m_pCollCom->Erase_Object(this);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}