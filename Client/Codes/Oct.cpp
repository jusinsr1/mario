#include "stdafx.h"
#include "Oct.h"
#include "Management.h"
#include "Effect_Monster.h"
#include "Collision.h"


_USING(Client)

COct::COct(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

COct::COct(const COct & rhs)
	: CGameObject(rhs)
{
	
}

void COct::Coll_GameObject(_int colType)
{
	CEffect_Monster* pEffect = nullptr;
	CManagement*		pManagement = CManagement::GetInstance();
	pManagement->AddRef();

	pManagement->Add_GameEffectToLayer(L"Monster_Effect", SCENE_STAGE,
		L"Layer_Effect", CEffect_Monster::MONSTER_EFFECT_OCT, m_iNumPlane % 5, (CGameObject**)&pEffect);

	Safe_Release(pManagement);

	m_eCurState = OCT_DEAD;
}

_bool COct::GetRC(RC & rc)
{
	return _bool();
}

void COct::Get_Event_GameObject(const _int iType, CComponent * pComponent)
{
}


// Prototype
HRESULT COct::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT COct::Ready_GameObject()
{	
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_MON, this)))
		return -1;

	m_eCurState	= OCT_IDLE;
	m_tInfo.fMove_Speed	 = 0.1f;
	m_tInfo.fFrame_Speed = 0.15f;


	return NOERROR;
}

_int COct::Update_GameObject(const _float & fTimeDelta)
{	

	_int iTargetPlaneNum = CEvent_Manager::GetInstance()->Get_Player_PlaneNum();

	if (m_iNumPlane == iTargetPlaneNum)
	{
		if (FAILED(Change_Motion()))
		return -1;

		_int iProcess = Update_Motion(fTimeDelta);
	
		if (0x80000000 & iProcess)
			return -1;
		
		return iProcess;
	}

	return 0;
}

_int COct::LastUpdate_GameObject(const _float & fTimeDelta)
{
	Move_Frame(m_tInfo.fFrame_Speed, fTimeDelta, m_pTextureCom[m_eCurState]->GetTexSize());	

	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return 0;
}

void COct::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();
	
	if (FAILED(m_pTextureCom[m_eCurState]->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;
	
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void COct::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation)
{

	m_iNumPlane = iPlaneNum;
	m_iNumTypePlane = iPlaneNum % 5;

	_vec3 vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	_vec3 vTarget = Position;
	
	vTarget -= vLook * 1.f;

	_vec3 TexSize = *m_pTextureCom[OCT_IDLE]->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.02f, TexSize.y*0.02f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vTarget);
	m_pTransformCom->SetPlaneNum(iPlaneNum, iRotation);

}

HRESULT COct::Change_Motion()
{
	if (OCT_END == m_eCurState)
		return E_FAIL;

	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case OCT_IDLE:
				break;
		case OCT_SHOT:
				break;
		case OCT_DEAD:
				break;
		default:
			return E_FAIL;
		}

		m_tInfo.iTexIdx = 0;
		m_ePreState = m_eCurState;
	}

	return NOERROR;
}

_int COct::Update_Motion(const _float& fTimeDelta)
{
	if (OCT_END == m_eCurState)
		return -1;

	switch (m_eCurState)
	{
	case OCT_IDLE:
		break;
	case OCT_SHOT:
		break;
	case OCT_DEAD:
		return 1;
	default:
		return -1;
	}
	return 0;
}


HRESULT COct::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	//For.Com_Coll
	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	Ready_Texture(pManagement);


	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT COct::Ready_Texture(CManagement* pManagement)
{

	m_pTextureCom[OCT_IDLE] = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	return E_NOTIMPL;
}


COct * COct::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	COct*	pInstance = new COct(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"COct Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * COct::Clone_GameObject()
{
	COct*	pInstance = new COct(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"COct Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void COct::Free()
{
	for (size_t i = 0; i < OCT_END; i++)
		Safe_Release(m_pTextureCom[i]);	

	if (m_tInfo.IsColne)
		m_pCollCom->Erase_Object(this);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pPaticleCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}
