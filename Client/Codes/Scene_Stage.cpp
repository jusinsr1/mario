#include "stdafx.h"
#include "..\Headers\Scene_Stage.h"
#include "Back_Logo.h"
#include "Management.h"
#include "Terrain.h"
#include "Camera_Debug.h"
#include "Player.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Building.h"
#include "Buffer_Cube.h"
#include "Tire.h"
#include "Buffer_Tire.h"
#include "Buffer_CubeLine.h"
#include "Plane.h"
#include "Effect_Player.h"
#include "Effect_Monster.h"
#include "Camera_Game.h"
#include "Normal.h"
#include "Jumping.h"
#include "Platform.h"
#include "Coin.h"
#include "Oct.h"
#include "Alien.h"
#include "Art.h"
#include "Buffer_Shadow.h"
#include "Shadow.h"
#include "Box.h"
#include "Particle.h"

_USING(Client)

CScene_Stage::CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CScene(pGraphic_Device)
{

}

HRESULT CScene_Stage::Ready_Scene()
{
	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;
	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;
	

	

	if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
		return E_FAIL;

	if (FAILED(Ready_Layer_3DObject(L"Layer_Building", L"Layer_Tire", L"Layer_Box")))
		return E_FAIL;

	

	if (FAILED(Ready_Layer_2DObject(L"Layer_Monster", L"Layer_Environment", L"Layer_Interaction")))
		return E_FAIL;

	if (FAILED(Ready_Layer_Effect(L"Layer_Effect")))
		return E_FAIL;

	if (FAILED(Ready_Layer_Player(L"Layer_Player")))
		return E_FAIL;
	
	if (FAILED(Ready_Layer_Camera(L"Layer_Camera")))
		return E_FAIL;

	//if (FAILED(Ready_Layer_Monster(L"Layer_Monster")))
	//	return E_FAIL;

	

	//////////////////////////////////////////����
	dir = { 0.3f, -0.3f, 0.5f };
	D3DXCOLOR   color = WHITE;
	InitLight(&dir, &color);
	

	D3DMATERIAL9 mtrl = InitMtrl(WHITE, WHITE, WHITE, BLACK, 5.f);
	m_pGraphic_Device->SetMaterial(&mtrl);

	m_pGraphic_Device->SetLight(0, &light);
	m_pGraphic_Device->LightEnable(0, true);
	m_pGraphic_Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	m_pGraphic_Device->SetRenderState(D3DRS_SPECULARENABLE, true);

	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);

	return NOERROR;
}

_int CScene_Stage::Update_Scene(const _float & fTimeDelta)
{
	
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	m_fTime += fTimeDelta;

	if (m_fTime > 1.f)
	{
		CParticle* pEffect = nullptr;

		pManagement->Add_GameEffectToLayer(L"Particle", SCENE_STAGE, L"Effect_Layer", _uint(CParticle::PARTICLE_SMOG),
				4, (CGameObject**)&pEffect);
			m_fTime = 0.f;
	}

	Safe_Release(pManagement);

	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Stage::LastUpdate_Scene(const _float & fTimeDelta)
{
	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Stage::Render_Scene()
{
}

HRESULT CScene_Stage::Ready_Prototype_GameObject()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();
	
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Terrain", CTerrain::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Player", CPlayer::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Building", CBuilding::Create(m_pGraphic_Device))))
		return E_FAIL; 

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Shadow", CShadow::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Tire", CTire::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Platform", CPlatform::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Normal", CNormal::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_ART", CArt::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Box", CBox::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Jumping", CJumping::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Alien", CAlien::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Oct", COct::Create(m_pGraphic_Device))))
		return E_FAIL;
	
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Coin", CCoin::Create(m_pGraphic_Device))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Prototype_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Terrain", CBuffer_Terrain::Create(m_pGraphic_Device,1000.f))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Player", CBuffer_Player::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_ObjMove_Player", CObjMove::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Cube", CBuffer_Cube::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_CubeLine", CBuffer_CubeLine::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Tire", CBuffer_Tire::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Shadow", CBuffer_Shadow::Create(m_pGraphic_Device))))
		return E_FAIL;

	if(FAILED(GET_INSTANCE(CLoad_Manager)->Load_PlaneData(L"../../Data/Plane/PlaneData.txt")))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Plane", CPlane::Create(m_pGraphic_Device))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camera(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	CCamera_Debug*	pCameraObject = nullptr;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
		return E_FAIL;

	CAMERADESC	CameraDesc;

	ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	CameraDesc.vEye = _vec3(0.f, 20.f, -10.f);
	CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);

	PROJDESC	ProjDesc;
	ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	ProjDesc.fFovY = D3DXToRadian(60.f);
	ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	ProjDesc.fNear = 0.3f;
	ProjDesc.fFar = 500.f;

	if (FAILED(pCameraObject->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
		return E_FAIL;

	Safe_Release(pManagement);

	//CManagement*	pManagement = GET_INSTANCE(CManagement);

	//if (pManagement == nullptr)
	//	return E_FAIL;

	//pManagement->AddRef();

	//CCamera_Game*	pCameraObject = nullptr;

	//if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Game", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
	//	return E_FAIL;

	//CAMERADESC	CameraDesc;

	//ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	////CameraDesc.vEye = _vec3(0.f, 0.f, -10.f);
	////CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	////CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);

	//PROJDESC	ProjDesc;
	//ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	//ProjDesc.fFovY = D3DXToRadian(60.f);
	//ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	//ProjDesc.fNear = 0.3f;
	//ProjDesc.fFar = 500.f;

	//if (FAILED(pCameraObject->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
	//	return E_FAIL;
	//pCameraObject->Set_PlayerMove(m_pPlayer->GetObjMove());

	//Safe_Release(pManagement);


	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();



	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Terrain", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Player(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Player", SCENE_STAGE, pLayerTag ,(CGameObject**)&m_pPlayer)))
		return E_FAIL;

	//if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Alien", SCENE_STAGE, pLayerTag)))
	//	return E_FAIL;

	//if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Oct", SCENE_STAGE, pLayerTag)))
	//	return E_FAIL;

	//if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Coin", SCENE_STAGE, pLayerTag)))
	//	return E_FAIL;

	//dynamic_cast<CParticle*>(pEffect)->Set_TargetTransCom();
	//m_pTransformCom->AddRef();
	//dynamic_cast<CParticle*>(pEffect)->SetNumPlane(m_iTypePlane);
	//m_bIsParticle = true;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Effect(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_Prototype_GameObject(L"Player_Effect", CEffect_Player::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"Monster_Effect", CEffect_Monster::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"Particle", CParticle::Create(m_pGraphic_Device))))
		return E_FAIL;


	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_3DObject(const _tchar* pLayerBuildingTag, const _tchar* pLayerTireTag, const _tchar* pLayerBoxTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(GET_INSTANCE(CLoad_Manager)->Load_3DCube(L"../../Data/Map/3DObj.txt", pLayerBuildingTag, pLayerTireTag, pLayerBoxTag)))
		return E_FAIL;


	
	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_2DObject(const _tchar * pMonsterTag, const _tchar * pEnvironmentTag, const _tchar * pInteractionTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	//if (FAILED(GET_INSTANCE(CLoad_Manager)->Load_2DSquare(L"../../Data/Map/2DObj.txt", pMonsterTag, pEnvironmentTag, pInteractionTag)))
	//	return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}



void CScene_Stage::InitLight(D3DXVECTOR3* direction, D3DXCOLOR* color)
{
	light.Type      = D3DLIGHT_DIRECTIONAL;
	light.Ambient   = *color * 0.6f;
	light.Diffuse   = *color;
	light.Specular  = *color * 0.6f;
	light.Direction = *direction;
}

D3DMATERIAL9 CScene_Stage::InitMtrl(D3DXCOLOR a, D3DXCOLOR d, D3DXCOLOR s, D3DXCOLOR e, float p)
{
	D3DMATERIAL9 mtrl;
	mtrl.Ambient = a;
	mtrl.Diffuse = d;
	mtrl.Specular = s;
	mtrl.Emissive = e;
	mtrl.Power = p;
	return mtrl;
}


CScene_Stage * CScene_Stage::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Stage* pInstance = new CScene_Stage(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		_MSG_BOX("CScene_Stage Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CScene_Stage::Free()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return;

	pManagement->AddRef();
	pManagement->Clear_Layers(SCENE_STAGE);
	Safe_Release(pManagement);
	
	CScene::Free();
}
