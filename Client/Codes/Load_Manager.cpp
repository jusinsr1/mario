#include "stdafx.h"
#include "..\Headers\Load_Manager.h"
#include "Management.h"
#include "Graphic_Device.h"
#include "Building.h"
#include "Tire.h"
#include "Pen.h"
#include "Normal.h"
#include "Platform.h"
#include "Cannon.h"
#include "Coin.h"
#include "Alien.h"
#include "Oct.h"
#include "Jumping.h"
#include "Art.h"
#include "Shadow.h"
#include "Box.h"

#include <fstream>
_USING(Client)
_IMPLEMENT_SINGLETON(CLoad_Manager)


CLoad_Manager::CLoad_Manager()
	:m_pGraphic_Device(GET_INSTANCE(CGraphic_Device)->Get_Graphic_Device())
{
	m_pGraphic_Device->AddRef();
}


HRESULT CLoad_Manager::Load_3DCube(const wstring & wstrPath, const _tchar* pLayerBuildingTag, const _tchar* pLayerTireTag, const _tchar* pLayerBoxTag)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
	{
		_MSG_BOX("Read 3DObj Failed");
		return E_FAIL;
	}

	TCHAR szBuf[256] = L"";
	
	_vec3 vecPosition;
	_vec3 vecScale;
	CubeID eCubeID;
	while (true)
	{
		wstring* CubeTag = new wstring;
		fin.getline(szBuf, 256, '|');
		*CubeTag = szBuf;
		fin.getline(szBuf, 256, '|');
		vecPosition.x = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		vecPosition.y = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		vecPosition.z = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		vecScale.x= (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		vecScale.y = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		vecScale.z = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256);
		eCubeID = (CubeID)_wtoi(szBuf);
		m_vWstring.push_back(CubeTag);
		
		if (fin.eof())
			break;


		if (eCubeID == CUBE_BUILDING)// || eCubeID == CUBE_NORMAL)
		{
			CBuilding*	pBuilding = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Building", SCENE_STAGE, pLayerBuildingTag, (CGameObject**)&pBuilding)))
				return E_FAIL;
			pBuilding->SetData(vecScale, vecPosition, CubeTag->c_str());


			CShadow*	pShadow = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Shadow", SCENE_STAGE, pLayerBuildingTag, (CGameObject**)&pShadow)))
				return E_FAIL;
			pShadow->SetData(vecScale, vecPosition, CubeTag->c_str());
				
		}
		else if (eCubeID == CUBE_JUMP)
		{
			CTire*	pTire = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Tire", SCENE_STAGE, pLayerTireTag, (CGameObject**)&pTire)))
				return E_FAIL;
			pTire->Set_Position(vecPosition);
		}
		else if (eCubeID == CUBE_NORMAL)
		{
			CNormal*	pNormal = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Normal", SCENE_STAGE, pLayerBoxTag, (CGameObject**)&pNormal)))
				return E_FAIL;
			pNormal->SetData(vecScale, vecPosition, CubeTag->c_str());

			//if (vecPosition.y - vecScale.y<FLT_EPSILON&&D3DXVec3Length(&vecScale)>5.f)
			if (vecScale.x>3.f&&vecScale.y>3.f&&vecScale.z>3.f)
			{
				CShadow*	pShadow = nullptr;
				if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Shadow", SCENE_STAGE, pLayerBuildingTag, (CGameObject**)&pShadow)))
					return E_FAIL;
				pShadow->SetData(vecScale, vecPosition, CubeTag->c_str());
			}
		}
		else if (eCubeID == CUBE_BOX)
		{
			CBox*	pBox = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Box", SCENE_STAGE, pLayerBoxTag, (CGameObject**)&pBox)))
				return E_FAIL;
			pBox->SetData(vecScale, vecPosition, CubeTag->c_str());
		}



	}
	fin.close();

	return NOERROR;
}

HRESULT CLoad_Manager::Load_2DSquare(const wstring & wstrPath, const _tchar * pMonsterTag, const _tchar * pEnvironmentTag, const _tchar * pInteraction)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
	{
		_MSG_BOX("Read 3DObj Failed");
		return E_FAIL;
	}

	TCHAR szBuf[256] = L"";



	_int iType = -1;
	_int iRotation = -1;
	_int iVariable = -1;
	_int iPlaneNum = -1;
	_vec3 Position;
	_vec3 Scale = { 1.f,1.f,1.f };

	
	while (true)
	{
		fin.getline(szBuf, 256, '|');
		iType = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		iRotation = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		iVariable = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		iPlaneNum = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		Position.x = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		Position.y = (_float)_wtof(szBuf);
		if (iType == 9)
			fin.getline(szBuf, 256, '|');
		else
			fin.getline(szBuf, 256);
		Position.z = (_float)_wtof(szBuf);

		if (iType == 9)
		{
			fin.getline(szBuf, 256, '|');
			Scale.x = (float)_wtof(szBuf);
			fin.getline(szBuf, 256, '|');
			Scale.y = (float)_wtof(szBuf);
		}

		if (fin.eof())
			break;

		
		//if(iType == 0)
		//{
		//	CPen* pPen = nullptr;
		//	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Pen", SCENE_STAGE, pMonsterTag, (CGameObject**)&pPen)))
		//		return E_FAIL;
		//	pPen->SetData(iPlaneNum, Position, iRotation, iVariable);
		//}
		if (iType == 1)
		{
			CPlatform* pPlatform = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Platform", SCENE_STAGE, pEnvironmentTag, (CGameObject**)&pPlatform)))
				return E_FAIL;
			pPlatform->SetData(iPlaneNum, Position, iRotation, iVariable);
		}
		//if (iType == 2)
		//{
		//	CCannon* pCannon = nullptr;
		//	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Cannon", SCENE_STAGE, pInteraction, (CGameObject**)&pCannon)))
		//		return E_FAIL;
		//	pCannon->SetData(iPlaneNum, Position, iRotation, iVariable);

		//}
		if (iType == 3)
		{
			CArt* pArt = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_ART", SCENE_STAGE, pEnvironmentTag, (CGameObject**)&pArt)))
				return E_FAIL;
			pArt->SetData(iPlaneNum, Position, iRotation, iVariable);
		}
		if (iType == 4)
		{
			CJumping* pJumping = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Jumping", SCENE_STAGE, pInteraction, (CGameObject**)&pJumping)))
				return E_FAIL;
			pJumping->SetData(iPlaneNum, Position, iRotation);
		}
		if (iType == 5)
		{
			CAlien* pAlien = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Alien", SCENE_STAGE, pMonsterTag, (CGameObject**)&pAlien)))
				return E_FAIL;
			pAlien->SetData(iPlaneNum, Position, iRotation);
		}
		if (iType == 6)
		{
			COct* pOct = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Oct", SCENE_STAGE, pMonsterTag, (CGameObject**)&pOct)))
				return E_FAIL;
			pOct->SetData(iPlaneNum, Position, iRotation);
		}
		if (iType == 7)
		{
			CCoin* pCoin = nullptr;
			if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_Coin", SCENE_STAGE, pInteraction, (CGameObject**)&pCoin)))
				return E_FAIL;
			pCoin->SetData(iPlaneNum, Position, iRotation, iVariable);
		}
	
	}
	fin.close();


	return NOERROR;
}

HRESULT CLoad_Manager::Load_PlaneData(const wstring& wstrPath)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
	{
		_MSG_BOX("Read PlaneData Failed");
		return E_FAIL;
	}
	
	TCHAR szBuf[256] = L"";

	while (true)
	{
		PLANEINFO tPlaneInfo;
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.iPlaneNum = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.fWidth = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.fHeight = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.Pos.x = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.Pos.y = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.Pos.z = (_float)_wtof(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.arrMoveablePlane[0] = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.arrMoveablePlane[1] = _wtoi(szBuf);
		fin.getline(szBuf, 256, '|');
		tPlaneInfo.arrMoveablePlane[2] = _wtoi(szBuf);
		fin.getline(szBuf, 256);
		tPlaneInfo.arrMoveablePlane[3] = _wtoi(szBuf);
	
		PLANEINFO* PlaneInfo = new PLANEINFO;
		*PlaneInfo = tPlaneInfo;
		if (fin.eof())
		{
			delete PlaneInfo;
			break;
		}
		m_mapPlaneData.insert({ tPlaneInfo.iPlaneNum ,PlaneInfo });
	}
	fin.close();

	return NOERROR;
}

HRESULT CLoad_Manager::Load_Texture(CTexture::TYPE eTypeID,const wstring & wstrPath)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
	{
		_MSG_BOX("Read TextureData Failed");
		return E_FAIL;
	}

	while (true)
	{
		TCHAR szBuf[256] = L"";
		wstring ImgPath = L"";
		wstring* ImgTag = new wstring;
		_int iCount = 0;
		fin.getline(szBuf, 256, '|');
		fin.getline(szBuf, 256, '|');
		*ImgTag = szBuf;
		fin.getline(szBuf, 256, '|');
		iCount = _wtoi(szBuf);
		fin.getline(szBuf, 256);
		ImgPath = szBuf;
		m_vWstring.push_back(ImgTag);
		if (fin.eof())
			break;
		if (FAILED(GET_INSTANCE(CManagement)->Add_Prototype_Component(SCENE_STATIC, ImgTag->c_str(), CTexture::Create(m_pGraphic_Device, eTypeID, ImgPath.c_str(), iCount))))
			return E_FAIL;
	}
	fin.close();

	return NOERROR;
}


void CLoad_Manager::Free()
{
	Safe_Release(m_pGraphic_Device);
	for (auto& iter : m_mapPlaneData)
		Safe_Delete(iter.second);
	m_mapPlaneData.clear();
	for (auto& iter : m_vWstring)
		Safe_Delete(iter);


	m_vWstring.clear();
}
