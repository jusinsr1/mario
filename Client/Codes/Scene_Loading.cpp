#include "stdafx.h"
#include "Scene_Loading.h"
#include "Management.h"
#include "Loading.h"
#include "Scene_Logo.h"
#include "Scene_Stage.h"
#include "Input_Device.h"

_USING(Client)

CScene_Loading::CScene_Loading(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{
}

HRESULT CScene_Loading::Ready_Scene()
{
	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
		return E_FAIL;



	return NOERROR;
}

_int CScene_Loading::Update_Scene(const _float & fTimeDelta)
{
	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Loading::LastUpdate_Scene(const _float & fTimeDelta)
{
	if (CInput_Device::GetInstance()->IsKeyDown(DIK_SPACE))
	{
		CManagement*		pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return -1;
		pManagement->AddRef();

		CScene_Logo*	pNewScene = CScene_Logo::Create(m_pGraphic_Device);

		if (nullptr == pNewScene)
			return - 1;

		if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
			return -1;

		Safe_Release(pNewScene);
		Safe_Release(pManagement);

		return 0;

	}
	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Loading::Render_Scene()
{
	
}

// 원형객체를 생성해 놓는다.
HRESULT CScene_Loading::Ready_Prototype_GameObject()
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;

	pManagement->AddRef();	
	
	//// For.GameObject_Loading
	//if (FAILED(pManagement->Add_Prototype_GameObject(
	//	L"GameObject_Loading", CLoading::Create(m_pGraphic_Device))))
	//	return E_FAIL;

	Safe_Release(pManagement);


	return NOERROR;
}

HRESULT CScene_Loading::Ready_Prototype_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;


	if (FAILED(GET_INSTANCE(CLoad_Manager)->Load_Texture(CTexture::TYPE_GENERAL, L"../../Data/Texture/2DMapTexInfo.txt")))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoad_Manager)->Load_Texture(CTexture::TYPE_CUBE, L"../../Data/Texture/3DMapTexInfo.txt")))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoad_Manager)->Load_Texture(CTexture::TYPE_GENERAL, L"../../Data/Texture/OthersTexInfo.txt")))
		return E_FAIL;

	// For.Component_Texture_Loading
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOADING,
	//	L"Component_Texture_Loading", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
	//		L"../../Resources/Textures/Logo/Logo_Loading/%d.dds", 10))))
	//	return E_FAIL;

	//// For.Component_Texture_Logo_Background
	//if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOADING,
	//	L"Component_Texture_Loading_Background", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
	//		L"../../Resources/Textures/Logo/Logo_Background/%d.dds", 1))))
	//	return E_FAIL;

	return NOERROR;
}

HRESULT CScene_Loading::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;

	pManagement->AddRef();

	// For.GameObject_Loading

	//for (size_t i = 0; i < 2; i++)
	//	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Loading", SCENE_LOADING, pLayerTag)))
	//		return E_FAIL;

	
	

	Safe_Release(pManagement);

	return NOERROR;
}

CScene_Loading * CScene_Loading::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Loading*	pInstance = new CScene_Loading(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Loading Created Failaed", L"System Error", MB_OK);
		Safe_Release(pInstance);		
	}
	return pInstance;
}

void CScene_Loading::Free()
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	pManagement->Clear_Layers(SCENE_LOADING);
	
	Safe_Release(pManagement);

	CScene::Free();
}
