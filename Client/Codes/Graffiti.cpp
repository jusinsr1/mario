#include "stdafx.h"
#include "Graffiti.h"
#include "Management.h"




_USING(Client)

CGraffiti::CGraffiti(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CGraffiti::CGraffiti(const CGraffiti & rhs)
	: CGameObject(rhs)
{

}


// Prototype
HRESULT CGraffiti::Ready_Prototype()
{

	return NOERROR;
}

// Clone
HRESULT CGraffiti::Ready_GameObject()
{

	if (FAILED(Ready_Component()))
		return E_FAIL;

	_vec3 TexSize = *m_pTextureCom->Get_TexRect(0);
	m_pTransformCom->Scaling(TexSize.x *0.01f, TexSize.y*0.01f, TexSize.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.0f, 0.f, 0.f));


	return NOERROR;
}

_int CGraffiti::Update_GameObject(const _float & fTimeDelta)
{

	return _int();
}

_int CGraffiti::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	return 0;
}

void CGraffiti::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
		return;

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CGraffiti::SetData(const _int & iPlaneNum, const _vec3 & Position, const _int & iRotation, const _int & iVariable)
{
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &Position);
	int Num = -1;

	if (iPlaneNum == -1)
		Num = 4;
	else
		Num = iPlaneNum % 5;

	if (Num == 1)
		m_pTransformCom->Rotation_Y(D3DXToRadian(90));
	else if (Num == 2)
		m_pTransformCom->Rotation_Y(D3DXToRadian(180));
	if (Num == 3)
		m_pTransformCom->Rotation_Y(D3DXToRadian(270));
	//if (Num == 4)


	m_iTexIdx = iVariable;
}

HRESULT CGraffiti::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STATIC, L"Graffiti");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// Create_Prototype
CGraffiti * CGraffiti::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CGraffiti*	pInstance = new CGraffiti(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CGraffiti Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// Prototype_Copy
CGameObject * CGraffiti::Clone_GameObject()
{

	CGraffiti*	pInstance = new CGraffiti(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CGraffiti Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CGraffiti::Free()
{

	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}