#include "stdafx.h"
#include "..\Headers\Player.h"
#include "Management.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Collision.h"
#include "Plane.h"
#include "Effect_Player.h"

_USING(Client)

CPlayer::CPlayer(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
	, m_pInput_Device(GET_INSTANCE(CInput_Device)),
	 m_pManageCom(GET_INSTANCE(CManagement))
{
	m_pInput_Device->AddRef();
}

CPlayer::CPlayer(const CPlayer & rhs)
	:CGameObject(rhs)
	, m_pInput_Device(rhs.m_pInput_Device),
	m_pManageCom(GET_INSTANCE(CManagement))
{
	m_pInput_Device->AddRef();
}

HRESULT CPlayer::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CPlayer::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COL_PLAYER, this)))
		return -1;
	
	curPlane = 1;

	_vec3 pos = m_pPlane->Find_PlaneInfo(curPlane)->Pos;
	

	
	m_pTransformCom->SetUp_Speed(20.f, D3DXToRadian(90.f));
	m_pTransformCom->Scaling(2.f, 2.f, 2.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &pos);


	 
	CEvent_Manager*		pEvent_Manager = CEvent_Manager::GetInstance();
	if (nullptr == pEvent_Manager)
		return E_FAIL;
	pEvent_Manager->AddRef();

	if (FAILED(pEvent_Manager->Add_Object_Event_Manager(L"Player", this)))
		return E_FAIL;

	Safe_Release(pEvent_Manager);


	return NOERROR;
}

_int CPlayer::Update_GameObject(const _float & fTImeDelta)
{
	//m_eCurState = PLAYER_SKILLA_L;
	
	if (m_eCurState == PLAYER_IN)
		return 0;

	 fSpd = 1.f;

	 AsGravity(fTImeDelta);//중력에의한움직임
	 KeyInput(fTImeDelta);//키인풋
	
	if (curPlane == -1)
		curPlane = prePlane;



	m_pObjMove->Setup_Obj(m_pPlane->Find_PlaneInfo(curPlane),m_pTransformCom);//값셋팅

	m_pObjMove->Update();//업데이트
	
	if (m_pObjMove->CheckIn(curPlane,prePlane))//평면이 바뀌진않았는지 체크
	{
		bCheck = true;
		moveCheck();		 //면이바뀜에따른 속도방향전환
	}
	m_pObjMove->Get_ClimbDir(climbDir);//올라온 벽 전해받기
	m_pObjMove->Get_TouchDir(touchDir,offX,offY);//터치방향과 오프셋전해받기


	
	 // Current Time	==========================================

 	if (FAILED(Ready_State()))
		return -1;
	if (0x80000000 & Update_State(fTImeDelta))
		return -1;
	if (FAILED(Move_Frame(m_fFrame_Speed, fTImeDelta, m_pTextureCom[m_eCurState]->GetTexSize())))
		return -1;

	return _int();
}

_int CPlayer::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_eCurState == PLAYER_IN)
		return 0;

	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;

	m_iNumPlane = curPlane;


	return _int();
}

void CPlayer::Render_GameObject()
{
	if (m_eCurState == PLAYER_IN)
		return;

	if (m_pBufferCom == nullptr)
		return;
	
	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom[m_eCurState]->SetUp_OnGraphicDev(m_tInfo.iTexIdx)))
		return;

	if (!bCheck)
		m_pBufferCom->SetUp_BufferPlayer(m_pPlane->Find_PlaneInfo(curPlane), touchDir, climbDir, offX, offY);
	else
		bCheck = false;


	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

}

void CPlayer::Coll_GameObject(_int colType)
{
	switch (colType)
	{
	case CCollision::COL_TIRE:
		TireJump();
		break;
	case CCollision::COL_SPRING:
		TireJump();
		break;
	}

}

void CPlayer::Get_Evente_Object(const _int iType, CComponent * pCom)
{



}

CPlayer * CPlayer::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CPlayer* pInstance = new CPlayer(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CPlayer::Clone_GameObject()
{
	CPlayer* pInstance = new CPlayer(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CPlayer Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

_bool CPlayer::GetRC(RC& rc)
{
	rc.plane = curPlane;
	rc.climeDir = climbDir;
	m_pTransformCom->GetRC2D(rc);
	return true;
}

HRESULT CPlayer::Ready_Component()
{

	m_pTransformCom = (CTransform*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
	return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
	return E_FAIL;

	m_pRendererCom = (CRenderer*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
	return E_FAIL;

	m_pCollCom = (CCollision*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Coll", m_pCollCom)))
	return E_FAIL;

	m_pBufferCom = (CBuffer_Player*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_Buffer_Player");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
	return E_FAIL;

	m_pObjMove = (CObjMove*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_ObjMove_Player");
	if (FAILED(Add_Component(L"Com_ObjMove", m_pObjMove)))
	return E_FAIL;

	m_pPlane = (CPlane*)m_pManageCom->Clone_Component(SCENE_STAGE, L"Component_Plane");
	if (FAILED(Add_Component(L"Com_Plane", m_pPlane)))
	return E_FAIL;

	if (FAILED(Ready_TextrueCom()))
	return E_FAIL;

return NOERROR;
}

HRESULT CPlayer::Ready_State()
{


	return NOERROR;
}

_int CPlayer::Update_State(const _float & fTimeDelta)
{

	return 0;
}

void CPlayer::Check_Combo()
{
	
	EffectID eEffectID = EFFECT_END;
	CGameObject* pEffect = nullptr;

	if (m_iComboCnt == 1 && !m_bIsCombo[0])
	{
		if (m_eCurState == PLAYER_ATTACK_L)
			eEffectID = EFFECT_ACOMBO_LEFT;
		else
			eEffectID = EFFECT_ACOMBO_RIGHT;

		m_iNumTex = 4;
		m_bIsCombo[0] = true;

		Create_Effect(L"Player_Effect", m_eCurState);
	}
	else if (m_iComboCnt <= 2 && !m_bIsCombo[1])
	{
		if (m_eCurState == PLAYER_ATTACK_L)
			eEffectID = EFFECT_BCOMBO_LEFT;
		else
			eEffectID = EFFECT_BCOMBO_RIGHT;

		m_iNumTex = 9;
		m_bIsCombo[1] = true;

		Create_Effect(L"Player_Effect", m_eCurState);
	}
	else if (m_iComboCnt <= 3&& !m_bIsCombo[2])
	{
		
		if (m_eCurState == PLAYER_ATTACK_L)
			eEffectID = EFFECT_CCOMBO_LEFT;
		else
			eEffectID = EFFECT_CCOMBO_RIGHT;

		m_iNumTex = 12;
		m_bIsCombo[2] = true;

		Create_Effect(L"Player_Effect", m_eCurState);
	}
}

void CPlayer::Create_Effect(const _tchar * Effect_Tag, PLAYER eEFffect_ID)
{
	CEffect_Player* pEffect;
	m_pManageCom->Add_GameEffectToLayer(Effect_Tag, SCENE_STAGE, L"Effect_Layer", _uint(eEFffect_ID), m_iNumPlane, (CGameObject**)&pEffect);
	dynamic_cast<CEffect_Player*>(pEffect)->Set_TargetTransCom(m_pTransformCom);
	m_pTransformCom->AddRef();
}

void CPlayer::KeyInput(const _float & fTImeDelta)
{

	if (m_pInput_Device->IsKeyDown(DIK_UP))
		Jump();		

	if (m_pInput_Device->IsKeyPressing(DIK_LEFT))
			
		vSpd.x = -6.f*fSpd;

	if (m_pInput_Device->IsKeyPressing(DIK_RIGHT))

		vSpd.x = 6.f*fSpd;
}

void CPlayer::AsGravity(const _float & fTImeDelta)
{
	m_pTransformCom->Move(fTImeDelta, vSpd);


	if (vSpd.y >= -15.f)
	{
		vSpd.y -= 0.5f;
	}

	vSpd.x *= 0.9f;
}

void CPlayer::moveCheck()
{
	if (curPlane % 5 == 4)
		return;


	if (prePlane % 5 != 4)
		return;


	float fCur = 0.f;

	switch ((curPlane % 5)- climbDir)
	{
	case 1:
	case 3:
		fCur = vSpd.y;
		vSpd.y = vSpd.x;
		vSpd.x = -fCur;
		break;
	case -2:
	case 2:
		vSpd.y *= -1.f;
		break;
	case -1:
	case -3:
		fCur = vSpd.y;
		vSpd.y = -vSpd.x;
		vSpd.x = fCur;
		break;
	}

	m_iNumPlane = curPlane % 5;

}

void CPlayer::Coll_Move(_vec3& vMove, _int i)
{
	_float vTick = 0.f;

	if (curPlane % 5 == 4)//옥상일때
	{
		switch (climbDir)
		{
		case 0:
			vTick = vMove.z;
			break;
		case 1:
			vTick = vMove.x;
			break;
		case 2:
			vTick = -vMove.z;
			break;
		case 3:
			vTick = -vMove.x;
			break;
		}
	}
	else
	{
		vTick = vMove.y;
	}

	


	if (i == CCollision::COL_FLAT)//플렛폼일때만 아래로 내려갈때만 
	{


		if (fabs(vTick) > 0.f)
		{
			if (vSpd.y < 0.f)
			{
				m_pTransformCom->MoveV3(vMove);//속도가 아래를향할때만 위치보정
				vSpd.y = 0.f;
				cntJump = 0;

				if (PLAYER_JUMP_L == m_eCurState)
					m_eCurState = PLAYER_IDLE_L;
				else if (PLAYER_JUMP_R == m_eCurState)
					m_eCurState = PLAYER_IDLE_R;
			}
		}

	}
	else if (i == CCollision::COL_BOX)
	{
		if (vSpd.y < 0.f)
		{
			vSpd.y = -3.f;
			cntJump = 0;
		}
			
		m_pTransformCom->MoveV3(vMove);
	}
	else
	{
		m_pTransformCom->MoveV3(vMove);

		if (vTick > 0.f&&vSpd.y <= 0.f || vTick < 0.f&&vSpd.y>0.f)
		{
			vSpd.y = 0.f;
			cntJump = 0;
			if (PLAYER_JUMP_L == m_eCurState)
				m_eCurState = PLAYER_IDLE_L;
			else if (PLAYER_JUMP_R == m_eCurState)
				m_eCurState = PLAYER_IDLE_R;
		}

	}



}

void CPlayer::Get_Event_GameObject(const _uint iType,CComponent* pCom)
{
	if (iType == 1 && m_eCurState != PLAYER_OUT)
		m_eCurState = PLAYER_IN;
	else if (iType == 2)
		m_eCurState = PLAYER_OUT;

	return;
}

void CPlayer::Jump()
{
	if (cntJump == 0)
	{
		m_pTransformCom->MoveV3({0.f,0.03f,0.f});
		vSpd.y += 15.f;
		//vSpd.y += 20.f;
		cntJump++;
	}
	else if (cntJump == 1)
	{
		vSpd.y += 10.f;
		cntJump++;
	}
}

void CPlayer::TireJump()
{
	

	if (vSpd.y <= 0.f)
		vSpd.y = 20.f;
	

}

void CPlayer::Free()
{

	//if (m_bIsColne)
	//	m_pCollCom->Release_Coll(this);


	for (size_t i = 0; i < PLAYER_END ; i++)
		Safe_Release(m_pTextureCom[i]);
	

	//Safe_Release(m_pManageCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pInput_Device);
	Safe_Release(m_pObjMove);

	Safe_Release(m_pCollCom);
	Safe_Release(m_pPlane);


	CGameObject::Free();
}


//==============================================================================================================
// 텍스쳐 클론
//==============================================================================================================

HRESULT CPlayer::Ready_TextrueCom()
{
	m_pTextureCom[PLAYER_IDLE_L] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Idle_Left");
	if (FAILED(Add_Component(L"Com_Texture_Idle_Left", m_pTextureCom[PLAYER_IDLE_L])))
		return E_FAIL;
	m_pTextureCom[PLAYER_IDLE_R] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Idle_Right");
	if (FAILED(Add_Component(L"Com_Texture_Idle_Right", m_pTextureCom[PLAYER_IDLE_R])))
		return E_FAIL;

	m_pTextureCom[PLAYER_RUN_L] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Run_Left");
	if (FAILED(Add_Component(L"Com_Texture_Run_Left", m_pTextureCom[PLAYER_RUN_L])))
		return E_FAIL;
	m_pTextureCom[PLAYER_RUN_R] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Run_Right");
	if (FAILED(Add_Component(L"Com_Texture_Run_Right", m_pTextureCom[PLAYER_RUN_R])))
		return E_FAIL;

	m_pTextureCom[PLAYER_DOWN_L] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Down_Left");
	if (FAILED(Add_Component(L"Com_Texture_Down_Left", m_pTextureCom[PLAYER_DOWN_L])))
		return E_FAIL;
	m_pTextureCom[PLAYER_DOWN_R] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Down_Right");
	if (FAILED(Add_Component(L"Com_Texture_Down_Right", m_pTextureCom[PLAYER_DOWN_R])))
		return E_FAIL;


	m_pTextureCom[PLAYER_ATTACK_L] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Attack_Left");
	if (FAILED(Add_Component(L"Com_Texture_Attack_Left", m_pTextureCom[PLAYER_ATTACK_L])))
		return E_FAIL;
	m_pTextureCom[PLAYER_ATTACK_R] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Attack_Right");
	if (FAILED(Add_Component(L"Com_Texture_Attack_Right", m_pTextureCom[PLAYER_ATTACK_R])))
		return E_FAIL;

	m_pTextureCom[PLAYER_JUMP_L] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Jump_Left");
	if (FAILED(Add_Component(L"Com_Texture_Jump_Left", m_pTextureCom[PLAYER_JUMP_L])))
		return E_FAIL;
	m_pTextureCom[PLAYER_JUMP_R] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Jump_Right");
	if (FAILED(Add_Component(L"Com_Texture_Jump_Right", m_pTextureCom[PLAYER_JUMP_R])))
		return E_FAIL;
	m_pTextureCom[PLAYER_OUT] = (CTexture*)m_pManageCom->Clone_Component(SCENE_STATIC, L"Player_Jump_Right");
	if (FAILED(Add_Component(L"Com_Texture_Out", m_pTextureCom[PLAYER_JUMP_R])))
		return E_FAIL;

	return NOERROR;
}
