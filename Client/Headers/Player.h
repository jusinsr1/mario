#pragma once
#include "GameObject.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;

_END

_BEGIN(Client)
class CBuffer_Player;
class CObjMove;
class CPlane;
class CPlayer final : public CGameObject
{
public:
	enum PLAYER {	PLAYER_IDLE_L,   PLAYER_IDLE_R, 
					PLAYER_RUN_L,	 PLAYER_RUN_R,
					PLAYER_DOWN_L,   PLAYER_DOWN_R,
					PLAYER_ATTACK_L, PLAYER_ATTACK_R,
					PLAYER_SKILL_L, PLAYER_SKILL_R,
					PLAYER_JUMP_L,   PLAYER_JUMP_R,
					PLAYER_IN ,		 PLAYER_OUT,
					PLAYER_END };

public:
	const _int				Get_PlaneNum()						{ return m_iNumPlane; }
	virtual void			Coll_GameObject(_int colType);
	virtual _bool			GetRC(RC& rc);
	CObjMove*				GetObjMove()						{ return m_pObjMove; }
	virtual void			Get_Evente_Object(const _int iType, CComponent* pCom);
private:
	explicit CPlayer(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlayer(const CPlayer& rhs);
	virtual ~CPlayer() = default;
public:
	virtual HRESULT			Ready_Prototype();
	virtual HRESULT			Ready_GameObject();
	virtual _int			Update_GameObject(const _float& fTImeDelta);
	virtual _int			LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void			Render_GameObject();


public:
	static CPlayer*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject();

public:
	
private:
	HRESULT				Ready_Component();
	HRESULT				Ready_TextrueCom();
private:
	HRESULT				Ready_State();
	_int				Update_State(const _float& fTimeDelta);
	void				Check_Combo();
	void				Create_Effect(const _tchar* Effect_Tag, PLAYER eEFffect_ID);
private:
	void				KeyInput(const _float & fTImeDelta);
	void				AsGravity(const _float & fTImeDelta);
	void				moveCheck();
public:
	virtual void Coll_Move(_vec3& vMove, _int i);
	virtual void Get_Event_GameObject(const _uint iTypem,CComponent* pCom);
private:
	void Jump();
	void TireJump();
private:
	virtual void Free();
private:
	PLAYER  m_eCurState = PLAYER_IDLE_L;
	PLAYER  m_ePreState = PLAYER_END;
	_uint	m_iComboCnt = 0;
	_bool	m_bIsCombo[3] = { false , false, false };
	_uint	m_iNumTex = 0;
	_int	m_iNumPlane = 0;
	_float	m_fFrame_Speed = 0.2f;
	_float  m_fOutTime = 0.f;
	_bool	m_bIs_InConnon  = false;
	_bool	m_bIs_OutConnon = false;
	_float  m_fCannonSeta = 0.f;
	_float  m_fSeta = 0.f;

private:
	CInput_Device*  m_pInput_Device = nullptr;
	CRenderer*		m_pRendererCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CTransform*		m_pTargetTrans = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_Player*	m_pBufferCom = nullptr;
	CManagement*	m_pManageCom = nullptr;
	CPlane*			m_pPlane = nullptr;
private:
	CTexture*		m_pTextureCom[PLAYER_END] = {};

private:
	_int curPlane = 0;
	_int prePlane = 0;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;


	_vec2 vSpd = {0.f,0.f};
	_float fSpd = 0.f;

	_uint cntJump = 0;
};

_END