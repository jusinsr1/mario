#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CBuffer_Particle;
class CCollision;
_END

_BEGIN(Client)

class COct final : public CGameObject
{
public:
	enum OCT { OCT_IDLE, OCT_SHOT, OCT_DEAD, OCT_EFFECT, OCT_END };

private:
	explicit COct(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit COct(const COct& rhs);
	virtual ~COct() = default;
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
	virtual void		Get_Event_GameObject(const _int iType, CComponent* pComponent);
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject();
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();
	virtual void		SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CBuffer_Particle*	m_pPaticleCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:	
	CTexture*			m_pTextureCom[OCT_END] = {};
private:
	OCT					m_eCurState = OCT_IDLE;	
	OCT					m_ePreState = OCT_END;
private:
	HRESULT				Ready_Component(); 
	HRESULT				Ready_Texture(CManagement* pManagement);

private:
	HRESULT				Change_Motion();
	_int				Update_Motion(const _float& fTimeDelta);
public:
	static COct* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
private:
	virtual void Free();

};

_END