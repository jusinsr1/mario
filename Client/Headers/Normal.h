#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_CubeLine;
class CCollision;
_END

_BEGIN(Client)
class CNormal final : public CGameObject
{
private:
	explicit CNormal(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CNormal(const CNormal& rhs);
	virtual ~CNormal() = default;


public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();

	HRESULT SetData(_vec3 vecScale, _vec3 vecPos, const _tchar* ImgTag);
public:
	virtual _bool GetRC(RC& dir);
	virtual void GetCUBE(CB& cb);
public:
	static CNormal* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();

private:
	virtual void Free();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_CubeLine*	m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
};

_END
