#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CBuffer_Particle;
_END

_BEGIN(Client)

class CSmoke final :
	public CGameObject
{
public:
	enum SMOKE_STATE { STATE_NONE, STATE_IDLE, STATE_END };
private:
	explicit CSmoke(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CSmoke(const CSmoke& rhs);
	virtual ~CSmoke() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
private:
	SMOKE_STATE		m_eCurState = STATE_NONE;
	SMOKE_STATE		m_ePreState = STATE_END;
private:
	_float				m_fSeta = 0.f;
private:
	HRESULT		Late_Ready_GameObject();
	_int		Update_State(const _float& fTimeDelta);


private:
	_bool	m_bIsParticle = false;

private:
	_int		m_iNumPlane = -1;
	_int		m_iTypePlane = 0;

private:
	HRESULT Ready_Component();
public:
	static CSmoke*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END