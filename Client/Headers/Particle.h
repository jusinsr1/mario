#pragma once
#include "Effect.h"
#include "Management.h"

_BEGIN(Client)

class CParticle :
	public CEffect
{	
public:
	enum PARTICLE {	PARTICLE_ITEM,PARTICLE_SMOG,PARTICLE_END };
public:
	void	Set_TargetTransCom(CTransform* pTarget_TransCom)	 { m_pTarget_TransCom = pTarget_TransCom; }
	void	SetNumPlane(_int& iNumPlane)						 { m_iNumPlane = iNumPlane; }
public:
	explicit CParticle(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CParticle(CParticle& rhs);
	virtual	 ~CParticle() = default;
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject(PARTICLE eID, const _int iCurNumPlane);
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();
private:
	PARTICLE			m_eID = PARTICLE_END;
private:
	CTransform*			m_pTransformCom = nullptr;
	CTransform*			m_pTarget_TransCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_Particle*	m_pParticleCom = nullptr;
	LPDIRECT3DTEXTURE9	m_pTextrues[PARTICLE_END];
	ATTIBUTE			m_Bute;
private:		
	_vec3				m_vTargetPos, m_vRight , m_vUp;
	_int				m_iNumPlane = 0;
	_bool				m_bIsLateReady = false;
private:
	_int				Ready_State();
private:
	HRESULT Ready_Component();
	HRESULT	Ready_Texture();
public:
	static CParticle* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
protected:
	virtual CGameObject * Clone_GameObject() override;
	virtual Engine::CGameObject * Clone_GameEffect(const Engine::_uint &iEffectID, const _int iCurNumPlane);
private:
	virtual void Free();

};


_END