#pragma once

#include "VIBuffer.h"

_BEGIN(Client)

class CBuffer_Shadow final : public CVIBuffer
{
private:
	explicit CBuffer_Shadow(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Shadow(const CBuffer_Shadow& rhs);
	virtual ~CBuffer_Shadow() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();

private:
	_matrix CreateShadowMatrix();
	void SetVertex();
	void ToShadow();

public:
	//virtual _bool Picking_ToBuffer(_vec3* pOut, const CTransform* pTransformCom, const CPicking* pPickingCom);
public:
	static CBuffer_Shadow* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();


private:
	_matrix matSha;

};

_END