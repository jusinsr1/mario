#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CJumping final :
	public CGameObject
{
public: enum JUMPING_STATE { STATE_NONE, STATE_JUMP , STATE_END };

private:
	explicit CJumping(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CJumping(const CJumping& rhs);
	virtual ~CJumping() = default;
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
	virtual void		Get_Event_GameObject(const _int iType, CComponent* pComponent);
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int	Update_GameObject(const _float& fTimeDelta);
	virtual _int	LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void	Render_GameObject();
	virtual void	SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:	
	CTexture*			m_pTextureCom = nullptr;
private:
	JUMPING_STATE		m_eCurState = STATE_NONE;
	JUMPING_STATE		m_ePreState = STATE_END;

private:
	HRESULT Ready_Component();

private:
	HRESULT				Change_Motion();
	_int				Update_Motion(const _float& fTimeDelta);
public:
	static CJumping*		Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END