#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CPlatform final :
	public CGameObject
{
public:
	enum PLATFORM_TYPE { PLATFOR_ON , PLATFOR_SWITCH, PLATFOR_END};


private:
	explicit CPlatform(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlatform(const CPlatform& rhs);
	virtual ~CPlatform() = default;
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
	virtual void		Get_Event_GameObject(const _int iType, CComponent* pComponent);

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);

private:
	CRenderer*				m_pRendererCom = nullptr;
	CBuffer_RcTex*			m_pBufferCom = nullptr;
	CTransform*				m_pTransformCom = nullptr;	
	CCollision*				m_pCollCom = nullptr;
private:
	CTexture*				m_pTextureCom[PLATFOR_END] = {};
private:
	PLATFORM_TYPE			m_eType = PLATFOR_ON;

private:
	HRESULT					Change_Motion();
	_int					Update_Motion(const _float& fTimeDelta);

private:
	HRESULT					Ready_Component();
	HRESULT					Ready_Texture(CManagement* pManagement);
public:
	static CPlatform*		Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END