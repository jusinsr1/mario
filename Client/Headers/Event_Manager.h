#pragma once
#include "Base.h"

_BEGIN(Engine)
class CGameObject;
_END
_BEGIN(Client)

class CEvent_Manager final: public CBase
{
	_DECLARE_SINGLETON(CEvent_Manager)
public:
	explicit CEvent_Manager();
	virtual ~CEvent_Manager() = default;
public:
	HRESULT Add_Object_Event_Manager(const _tchar * pGameObjectTag , CGameObject* pObject);
	
public:
	_float&	GetSeta()	{ return m_fSeta; }
	void	SetSeta(_float& fSeta) { m_fSeta = fSeta; }
public:
	const _int	Get_Player_PlaneNum();
public:
	void	Create_Event(const _tchar* pGameObjectTag, const _uint iType , CComponent* pComponent = nullptr);
public:
	HRESULT	Erase_Event_Manager(const _tchar* pGameObjectTag);
private:
	map<const _tchar*, CGameObject*>			m_mapObject;
	typedef map<const _tchar*, CGameObject*>	MAPObject;	
private:
	CGameObject* Find_GameObject(const _tchar* pGameObjectTag);
protected:
	virtual void Free();

private:
	_float m_fSeta = 0.f;

};

_END
