#pragma once
#include "Scene.h"

_BEGIN(Client)

class CPlayer;
class CScene_Stage final : public CScene
{
private:
	explicit CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CScene_Stage() = default;
public:
	virtual HRESULT Ready_Scene();
	virtual _int Update_Scene(const _float& fTimeDelta);
	virtual _int LastUpdate_Scene(const _float& fTimeDelta);
	virtual void Render_Scene();
private:
	HRESULT Ready_Prototype_GameObject();
	HRESULT Ready_Prototype_Component();
	HRESULT Ready_Layer_Camera(const _tchar* pLayerTag);
	HRESULT Ready_Layer_BackGround(const _tchar* pLayerTag);
	HRESULT Ready_Layer_Player(const _tchar * pLayerTag);
	HRESULT Ready_Layer_Effect(const _tchar * pLayerTag);
	HRESULT Ready_Layer_3DObject(const _tchar* pLayerBuildingTag, const _tchar* pLayerTireTag, const _tchar* pLayerBoxTag);
	HRESULT Ready_Layer_2DObject(const _tchar* pMonsterTag, const _tchar* pEnvironmentTag, const _tchar* pInteractionTag);


	void InitLight(D3DXVECTOR3* direction, D3DXCOLOR* color);
	D3DMATERIAL9 InitMtrl(D3DXCOLOR a, D3DXCOLOR d, D3DXCOLOR s, D3DXCOLOR e, float p);
public:
	static CScene_Stage* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
protected:
	virtual void Free();
private:
	D3DLIGHT9	light;
	CPlayer*	m_pPlayer = nullptr;
	D3DXVECTOR3 dir = {};
	_float		m_fTime = 0.f;

};

_END