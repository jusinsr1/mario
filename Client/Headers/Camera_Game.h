#pragma once
#include "Camera.h"



_BEGIN(Client)
class CObjMove;
class CCamera_Game final: public CCamera
{
private:
	explicit CCamera_Game(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera_Game(const CCamera_Game& rhs);
	virtual ~CCamera_Game() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	void Set_PlayerMove(CObjMove* PlayerMove) { m_pPlayerMove = PlayerMove; }



private:
	void Default_Camera();

public:
	static CCamera_Game* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();

private:
	CObjMove* m_pPlayerMove = nullptr;
	_vec3 m_vCurCameraPos = { 0.f,0.f,0.f };
	_vec3 m_vCurLook = { 0.f,0.f,0.f };
};

_END