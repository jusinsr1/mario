#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
_END

_BEGIN(Client)

class CArt final
	: public CGameObject
{
public:
	enum ART { SMALL_POSTER, BIG_POSTER, GRAFFITI, ART_END };
private:
	explicit CArt(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CArt(const CArt& rhs);
	virtual ~CArt() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
private:
	CTexture*			m_pTextureCom[ART_END] = {};
private:
	ART					m_eType = SMALL_POSTER;
private:
	_int				m_iNumPlane = -1;

private:
	HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
public:
	static CArt* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END