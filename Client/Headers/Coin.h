#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CBuffer_Particle;
class CCollision;
_END

_BEGIN(Client)

class CCoin final :
	public CGameObject
{
public:
	enum COIN_STATE { STATE_NONE, STATE_EAT, STATE_END };
private:
	explicit CCoin(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCoin(const CCoin& rhs);
	virtual ~CCoin() = default;

public:
	virtual	void	Coll_GameObject(_int colType);
	virtual _bool	GetRC(RC& rc);

public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject();
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();
	virtual void		SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:
	COIN_STATE			m_eCurState = STATE_NONE;
	COIN_STATE			m_ePreState = STATE_END;
private:
	_bool				m_bIsParticle = false;
	_float				m_fSeta = 0.f;
private:
	HRESULT				Ready_Component();
	HRESULT				Ready_Texture(CManagement* pManagement);

private:
	HRESULT				Change_Motion();
	_int				Update_Motion(const _float& fTimeDelta);

private:
	void				Create_Particle(const _uint& iDrawID);

public:
	static CCoin*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END