#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CAlien final : public CGameObject
{
public:
	enum ALIEN { ALIEN_MOVE, ALIEN_IDLE, ALIEN_GAS, ALIEN_BLOCK, ALIEN_BEATTACK, ALIEN_DEAD , ALIEN_END};
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);

private:
	explicit CAlien(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CAlien(const CAlien& rhs);
	virtual ~CAlien() = default;
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject();
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();
	virtual void		SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation);
	
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CCollision*			m_pCollCom = nullptr;
private:
	CTexture*			m_pTextureCom[ALIEN_END] = {};
private:
	_float				m_fFrame_Speed = 0.f;
	_float				m_fSpeed = 0.f;
	_float				m_fTimeMove = 0.f;
private:
	_bool				m_bIsNone = false;
private:
	_uint				m_iNumPlane = -1;
private:
	_uint				m_iStateCnt = 0;
	_int				m_iHit = 0;
private:
	ALIEN				m_eCurState = ALIEN_IDLE;
	ALIEN				m_ePreState = ALIEN_END;
private:
	HRESULT				Ready_State();
	_int				Update_State(const _float& fTimeDelta);

private:
	HRESULT Ready_Component();	// 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
public:
	static CAlien* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END