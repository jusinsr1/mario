#pragma once
#include "Effect.h"
#include "Management.h"

_BEGIN(Client)

class CEffect_Monster final :
	public CEffect
{	
public:
	enum MONSTER_EFFECT { MONSTER_EFFECT_OCT , MONSTER_EFFECT_ALIEN , MONSTER_EFFECT_HIT , MONSTER_EFFECT_END};

public:
	void	Set_TargetTransCom(CTransform* pTarget_TransCom)
	{ m_pTarget_TransCom = pTarget_TransCom; }

	void	SetNumPlane(_uint& iNumPlane)						
	{ m_iNumPlane = iNumPlane; }

public:
	explicit CEffect_Monster(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CEffect_Monster(CEffect_Monster& rhs);
	virtual	 ~CEffect_Monster() = default;
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject(EffectID eID, const _int iCurNumPlane);
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();

private:
	EffectID			m_eEffectID;
	EffectID			m_ePreEffectID = EffectID::EFFECT_END;
	
private:
	CTransform*			m_pTransformCom = nullptr;
	CTransform*			m_pTarget_TransCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
private:
	_float				m_fFramePerSec = 0.f;
	_uint				m_iNumTex = 0;
	_uint				m_iNumPlane = 0;
	_float				m_fSpeedX = 0.f;
	_float				m_fSpeedY = 0.f;
	_float				m_fSeta = 0.f;
	_vec3				m_vTargetPos;

	_vec3				m_vRight = {};
	_vec3				m_vUp = {};
	_bool				m_bIsReady = false;
	_bool				m_bIsOkStart = false;
	_bool				m_bIsSetPos = false;
private:
	_int	Update_State(const _float& fTimeDelta);
private:
	HRESULT Ready_Component();
	HRESULT	Ready_Texture();
public:
	static CEffect_Monster* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
protected:
	virtual CGameObject * Clone_GameObject() override;
	virtual Engine::CGameObject * Clone_GameEffect(const Engine::_uint &iEffectID, const _int iCurNumPlane);
private:
	virtual void Free();

};

_END