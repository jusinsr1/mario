#pragma once

#include "GameObject.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
class CCollision;
_END

_BEGIN(Client)

class CCannon final : 
	public CGameObject
{
public:
	enum CANNON		  { CANNON_TYPEA ,CANNON_TYPEB ,CANNON_END};
	enum CANNON_STATE { STATE_NONE, STATE_IN, STATE_OUT , STATE_END};
	enum CANNON_DIR   { DIR_MZ, DIR_PX, DIR_PZ, DIR_MX, DIR_END};
private:
	explicit CCannon(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCannon(const CCannon& rhs);
	virtual ~CCannon() = default;
public:
	virtual HRESULT		Ready_Prototype();
	virtual HRESULT		Ready_GameObject();
	virtual _int		Update_GameObject(const _float& fTimeDelta);
	virtual _int		LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void		Render_GameObject();
	virtual void		SetData(const _int& iPlaneNum, const _vec3& Position, const _int& iRotation, const _int& iVariable);
public:
	virtual void		Coll_GameObject(_int colType);
	virtual _bool		GetRC(RC& rc);
private:
	CInput_Device*		m_pInput_Device = nullptr;
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom[CANNON_END] = {};
	CCollision*			m_pCollCom = nullptr;
private:
	CANNON				m_eType			= CANNON_TYPEA;
	CANNON_STATE		m_eCurState		= STATE_NONE;
	CANNON_STATE		m_ePreState		= STATE_END;
	CANNON_DIR			m_eDir			= CANNON_DIR::DIR_END;
private:
	_bool				m_bIsReady = false;

private:
	_float				m_fSeta = 0.f;
	_int				m_iNumPlane = 0;
private:
	HRESULT	Late_Ready_GameObject();
	_int	Update_State(const _float& fTimeDelta);
	void	Input_Key();
private:
	HRESULT Ready_Component();

public:
	static CCannon*			Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject*	Clone_GameObject() override;
protected:
	virtual void Free();

};

_END